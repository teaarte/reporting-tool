
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})


mygrid = new dhtmlXGridObject('grid');
//mygrid.setHeader("Start date,End date,Text");

//mygrid.enableAutoHeight(true, 500, true);
mygrid.enableAutoWidth(true);
//mygrid.enableMarkedCells(true);
mygrid.enableExcelKeyMap();
mygrid.enableCellIds(true);
mygrid.enableEditEvents(true,false,true);
mygrid.init();



$( ".column-multiselect" ).each(function( index ) {
    console.log( index + ": " + $( this ).val() );


    //var myobj = JSON.parse('{'+$( this ).val()+'}');
    var myobj = JSON.parse($( this ).val());

    console.log('Data', $( this ).data('column-index'));

    mygrid.registerCList($( this ).data('column-index'), myobj);
});




//mygrid.enableMultiline(true);
let db_id = $('input[name=db_id]').val();

mygrid.load("/report/database/getData/"+db_id, "xml");

mygrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
    /*
    console.log('rId', rId);
    console.log('cInd', cInd);
    console.log('getColumnId', mygrid.getColumnId(cInd));*/

    if (stage == 2 && nValue == oValue && mygrid.getColType(cInd) == 'upload') {
        //console.log('!!!!!!!!!!!!!!!!!');
        return false;
    }

    let db_id = $('input[name=db_id]').val();

    if (stage == 2 && nValue != oValue) {


        if (mygrid.getColType(cInd) == 'upload') {
            console.log('onEditCell for upload!!!!!!!!!');
            return false;
        }

        /* Сохраняем на сервер */
        let data = {};
        data['rId'] = rId;
        data['cId'] = mygrid.getColumnId(cInd);

        data['oldValue'] = oValue;
        data['value'] = nValue;

        $.ajax({
            url: '/report/database/insert-data',
            type: 'post',
            data:{db_id:db_id, data:data},
            success:function(data) {
                console.log('success');
                // todo: почему-это этот true не дает нужного результата, поэтому снизу возвращаем..
                return true;
            }
        });

    }

    return true;


});




mygrid.attachEvent("onRowAdded",function(rId){
    //console.log('onRowAdded', rId);

    /* Отправляем запрос на сревер, чтобы вместо фейкового айди, получить настоящий айди новой строки из базы */

    let db_id = $('input[name=db_id]').val();
    let data = {};
    data['rId'] = rId;

    $.ajax({
        url: '/report/database/generateNewRow',
        type: 'post',
        data:{db_id:db_id, data:data},
        success:function(data) {
            console.log('success');

            if (data.s === 1) {
                mygrid.changeRowId(rId, data.id);
            }

        }
    });


})



function addRow() {
    var newId = (new Date()).valueOf();
    mygrid.addRow(newId,"");


}

function deleteRow() {
    let db_id = $('input[name=db_id]').val();

    var rId = mygrid.getSelectedRowId();
    mygrid.deleteRow(rId);

    let data = {};
    data['rId'] = rId;

    $.ajax({
        url: '/report/database/remove-row',
        type: 'post',
        data:{db_id:db_id, data:data},
        success:function(data) {
            console.log('success');
            // todo: почему-это этот true не дает нужного результата, поэтому снизу возвращаем..
            return true;
        }
    });
}


