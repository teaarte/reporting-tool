$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function loadData() {
    let reportSchedule_id = $('input[name=reportSchedule_id]').val();
    $.ajax({
        url: '/report-schedule/get-data',
        type: 'post',
        data:{reportSchedule_id:reportSchedule_id},
        success: function (response) {
            if(response){
                $('#grid_loader').remove();
                if(response === 'false'){
                    alert(trans.global.columns_not_exists);
                }
                else{
                    //gridInit($.parseJSON(response))
                }
            }
        }
    });
}

/*
function gridInit(response) {
    let reportSchedule_id = $('input[name=reportSchedule_id]').val();

    let editable = true;

    let tbar = [{
        text: 'Add',
        action: 'add',
        handler: function(){
            let id = 1;
            if(grid.getTotal() > 0)
                id = parseInt(grid.getData().reverse()[0].id) + 1;
            $.ajax({
                url: '/report-schedule/add-row',
                type: 'post',
                data:{reportSchedule_id:reportSchedule_id, id:id},
                success: function (data) {
                    if(data){
                        grid.add({id: id});
                    }
                }
            });
        }
    }, {
        text: 'Remove',
        action: 'remove'
    }];

    if(response.status === 'close'){
        editable = false;
        tbar = null;
    }

    let defaults = {
        type: 'string',
        flex: 1,
        editable: editable,
        sortable: true,
        resizable: true,
        ellipsis: true
    };

    let events = [
        {
            set: function(grid, params)
            {
                let data = {};
                data['id'] = params.id;
                data['key'] = params.key;
                data['oldValue'] = params.oldValue;
                data['value'] = params.value;
                data['rowIndex'] = params.rowIndex;


                console.dir(params);

                $.ajax({
                    url: '/report-schedule/insert-data',
                    type: 'post',
                    data:{reportSchedule_id:reportSchedule_id, data:data},
                });
            }
        },
        {
            remove: function(grid, id, item){
                $.ajax({
                    url: '/report-schedule/remove-row',
                    type: 'post',
                    data:{reportSchedule_id:reportSchedule_id, id:id}
                });
            },
        }
    ];


    $.each(response.columns, function( index, value ) {

        console.log(value);
        if (value.cellTip) {
            //response.columns.index.cellTip = Function(value.cellTip);
            console.log(value.cellTip);
            //response.columns[index].cellTip = value.cellTip.parseFunction();

            response.columns[index].cellTip = new Function("o", value.cellTip);


        }

    });


    let grid = new FancyGrid({
        renderTo: 'report-fancy',
        data: response.data,
        height: 500,
        defaults: defaults,
        tbar: tbar,
        clicksToEdit: 1,
        selModel: 'rows',
        columns: response.columns,
        events: events
    });
}*/

$(function() {
    loadData();
});