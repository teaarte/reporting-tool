$(function() {

    let table =$('#report-columns-table');
    let action = 'column/get-list';

    const columns = [
        {data: 'index', name: 'index'},
        {data: 'title', name: 'title'},
        {data: 'type', name: 'type'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ];

    table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: action,
        columns: columns
    });

    $('#type').on('change', function () {
        if (this.value === 'combo' || this.value === 'multiselect')
            $('.report-column-data').show();
        else
            $('.report-column-data').hide();
    });

    if ($('#type').val() === 'combo' || $('#type').val() === 'multiselect')
        $('.report-column-data').show();
    else
        $('.report-column-data').hide();

});