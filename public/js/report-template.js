$(function() {

    let table =$('#report-templates-table');
    let action = 'template/get-list';

    const columns = [
        {data: 'name', name: 'name'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ];

    table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: action,
        columns: columns
    });
});