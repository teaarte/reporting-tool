$(function() {

    let table =$('#report-final-table');
    let action = '/reportFinal/list';

    const columns = [
        {data: 'name', name: 'name'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ];

    table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: action,
        columns: columns
    });

});