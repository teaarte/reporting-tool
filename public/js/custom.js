$(function() {
    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy',
    });

    $(".select2").select2();
});


function getMiniStat(reportSchedule_id) {

    $.ajax({
        url: '/report-schedule/getMiniStat',
        type: 'get',
        data: {reportSchedule_id: reportSchedule_id},
        success: function (response) {

            //console.dir(response.result);

            $('#articles_published').html(response.result.count);
            $('#audience_reach').html(response.result.reach);
            $('#ave_generated').html(response.result.ave);
            $('#positive_statements').html(response.result.positive_statemts_percent);


        }
    });

}