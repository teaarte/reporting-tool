
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})


mygrid = new dhtmlXGridObject('grid');
//mygrid.setHeader("Start date,End date,Text");

//mygrid.enableAutoHeight(true, 500, true);
mygrid.enableAutoWidth(true);
//mygrid.enableMarkedCells(true);
mygrid.enableExcelKeyMap();
mygrid.enableCellIds(true);
mygrid.enableEditEvents(true,false,true);
//mygrid.enableAutoSaving();

//mygrid.enableValidation(true,true);

mygrid.init();




$( ".column-multiselect" ).each(function( index ) {
    console.log( index + ": " + $( this ).val() );


    //var myobj = JSON.parse('{'+$( this ).val()+'}');
    var myobj = JSON.parse($( this ).val());

    console.log('Data', $( this ).data('column-index'));

    mygrid.registerCList($( this ).data('column-index'), myobj);
});


//mygrid.enableMultiline(true);
let reportSchedule_id = $('input[name=reportSchedule_id]').val();

mygrid.load("/report-schedule/getData/"+reportSchedule_id, "xml");

mygrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){


    /*
    console.log('rId', rId);
    console.log('cInd', cInd);
    console.log('getColumnId', mygrid.getColumnId(cInd));*/

    let reportSchedule_id = $('input[name=reportSchedule_id]').val();

    /*
    console.log('---------------------------------------- onEditCell !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    console.log('stage', stage);
    console.log('oValue', oValue);
    console.log('nValue', nValue);
    */

    if (stage == 2 && nValue == oValue && mygrid.getColType(cInd) == 'upload') {
        //console.log('!!!!!!!!!!!!!!!!!');
        return false;
    }

    if (stage == 2 && nValue != oValue) {


        if (mygrid.getColumnId(cInd) == 'media_name') {
            if (!nValue.match(/^[0-9a-fA-F]{24}$/)) {
                /* Not mongoId!!!*/
                alert('Please select from the list.');
                return false;
            }

        }


        console.log('getColType', mygrid.getColType(cInd));


        if (mygrid.getColType(cInd) == 'upload') {
            console.log('onEditCell for upload!!!!!!!!!');
            return false;
        }


        // Сохраняем на сервер
        let data = {};
        data['rId'] = rId;
        data['cId'] = mygrid.getColumnId(cInd);

        data['oldValue'] = oValue;
        data['value'] = nValue;

        $.ajax({
            url: '/report-schedule/insert-data',
            type: 'post',
            data:{reportSchedule_id:reportSchedule_id, data:data},
            success:function(data) {
                console.log('success');
                // todo: почему-это этот true не дает нужного результата, поэтому снизу возвращаем..
                return true;
            }
        });

    }

    return true;


});

/*
mygrid.attachEvent("onCellChanged", function(rId,cInd,nValue){


    console.log('cell changed! New value: ', nValue);


    var cellObj = mygrid.cells(rId, cInd);
    //console.log('cell title: ', cellObj.getTitle());
    //console.dir(cellObj);

    var colType = mygrid.getColType(cInd);
    if (colType == 'upload' && nValue != "") {

        //mygrid.editStop();

        // Сохраняем на сервер
        console.log('cell changed! New value: ', nValue);
        console.log('UPLOAD FILE!!!');


        let reportSchedule_id = $('input[name=reportSchedule_id]').val();

        // Сохраняем на сервер
        let data = {};
        data['rId'] = rId;
        data['cId'] = mygrid.getColumnId(cInd);


        data['value'] = nValue;

        $.ajax({
            url: '/report-schedule/insert-data',
            type: 'post',
            data:{reportSchedule_id:reportSchedule_id, data:data},
            success:function(data) {
                console.log('success instert after file upload');
                // todo: почему-это этот true не дает нужного результата, поэтому снизу возвращаем..
                return true;
            }
        });


    }

});
*/

mygrid.attachEvent("onRowAdded",function(rId){
    //console.log('onRowAdded', rId);

    /* Отправляем запрос на сревер, чтобы вместо фейкового айди, получить настоящий айди новой строки из базы */

    let reportSchedule_id = $('input[name=reportSchedule_id]').val();
    let data = {};
    data['rId'] = rId;

    $.ajax({
        url: '/report-schedule/generateNewRow',
        type: 'post',
        data:{reportSchedule_id:reportSchedule_id, data:data},
        success:function(data) {
            console.log('success');

            if (data.s === 1) {
                mygrid.changeRowId(rId, data.id);
            }

        }
    });


})



function addRow() {
    var newId = (new Date()).valueOf();
    mygrid.addRow(newId,"");


}

function deleteRow() {
    let reportSchedule_id = $('input[name=reportSchedule_id]').val();

    var rId = mygrid.getSelectedRowId();
    mygrid.deleteRow(rId);

    let data = {};
    data['rId'] = rId;

    $.ajax({
        url: '/report-schedule/remove-row',
        type: 'post',
        data:{reportSchedule_id:reportSchedule_id, data:data},
        success:function(data) {
            console.log('success');
            // todo: почему-это этот true не дает нужного результата, поэтому снизу возвращаем..
            return true;
        }
    });
}


