$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


function loadData() {
    let list_id = $('input[name=list_id]').val();
    if(list_id){
        $.ajax({
            url: '/report/database/get-data',
            type: 'post',
            data:{list_id:list_id},
            success: function (response) {
                if(response){
                    $('#grid_loader').remove();
                    if(response === 'false'){
                        alert(trans.global.columns_not_exists);
                    }
                    else{
                        //gridInit($.parseJSON(response))
                    }
                }
            }
        });
    }
}


function gridInit(response) {


    let list_id = $('input[name=list_id]').val();

    let editable = true;

    let tbar = [{
        text: 'Add',
        action: 'add',
        handler: function(){
            let id = 1;
            if(grid.getTotal() > 0)
                id = parseInt(grid.getData().reverse()[0].id) + 1;
            grid.add({id:id});
        }
    }, {
        text: 'Remove',
        action: 'remove'
    }];

    let defaults = {
        type: 'string',
        flex: 1,
        editable: editable,
        sortable: true,
        resizable: true,
        ellipsis: true
    };

    let events = [
        {
            set: function(grid, params)
            {
                console.dir(params);
                console.log('item row');
                console.dir(params.item);
                console.log('data._id', params.item.data._id);


                let data = {};

                if (params.item.data._id) {
                    data['_id'] = params.item.data._id;
                }


                data['id'] = params.id;
                data['key'] = params.key;
                data['oldValue'] = params.oldValue;
                data['value'] = params.value;
                data['rowIndex'] = params.rowIndex;

                $.ajax({
                    url: '/report/database/insert-data',
                    type: 'post',
                    data:{list_id:list_id, data:data},
                });
            }
        },
        {
            remove: function(grid, id, item){
                console.dir(item);

                let data = {};

                if (item.data._id) {
                    data['_id'] = item.data._id;
                }
                data['list_id'] = list_id;
                data['id'] = id;



                $.ajax({
                    url: '/report/database/remove-row',
                    type: 'post',
                    data: data
                });
            },
        }
    ];
/*
    let grid = new FancyGrid({
        renderTo: 'report-fancy',
        data: response.items,
        height: 'fit',
        defaults: defaults,
        tbar: tbar,
        clicksToEdit: 2,
        selModel: 'rows',
        columns: response.columns,
        events: events
    });
*/


/*
    let db_id = $('input[name=list_id]').val();


    mygrid = new dhtmlXGridObject('grid');
    mygrid.setHeader("Start date,End date,Text");
    mygrid.init();
    mygrid.load("/report/database/getData/"+db_id, "json");
    */

    //var dp = new dataProcessor("./grid_data");
    //dp.init(mygrid);
}

$(function() {

    let table =$('#my-database-table');
    let action = 'database/get-list';

    const columns = [
        {data: 'name', name: 'name'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ];

    table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: action,
        columns: columns
    });

    $(document).on("click", ".add-column-data-row", function () {
        let column_data_group = $('.column-data-group');
        let clone = column_data_group.last().clone();
        $('.add-column-data-row').html('-').removeClass('add-column-data-row btn-info').addClass('remove-column-data-row btn-danger');
        clone.find('input').val('');
        column_data_group.last().after(clone);
    });

    $(document).on("click", ".remove-column-data-row", function () {
        this.closest('.column-data-group').remove();
    });


    loadData();

});