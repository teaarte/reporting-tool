$(function() {

    let table =$('#users-table');
    let action = 'user/get-list';

    const columns = [
        {data: 'email', name: 'email'},
        {data: 'generated_password', name: 'generated_password'},
        {data: 'groups', name: 'groups'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ];

    table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: action,
        columns: columns
    });

});