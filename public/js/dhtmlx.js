function eXcell_upload(cell){ //the eXcell name is defined here
    if (cell){                // the default pattern, just copy it
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;

    }

    /* read-only cell doesn't have edit method */
    //this.edit = function(){}
    /* the cell is read-only, so it's always disabled */
    //this.isDisabled = function(){ return true; }

    /*
    this.edit = function(){

        console.log('cell edit');
        //console.dir('cell cellIndex: ', this.cell.cellIndex );


        this.mydialog(this, this.getValue());


        //console.log('edit innerHTML: ' + this.cell.innerHTML);
        this.val = this.getValue(); // save current value
        //console.log('this.getValue: ' + this.getValue());




        //console.log('cell: ', this.cell.val);


        // blocks onclick event
        this.cell.firstChild.onclick=function(e){ (e||event).cancelBubble=true;}
        //this.cell.firstChild.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;}
    }
    */

    this.edit = function(){

        console.log('cell edit');
        //console.dir('cell cellIndex: ', this.cell.cellIndex );


        this.mydialog(this, this.getValue());


        //console.log('edit innerHTML: ' + this.cell.innerHTML);
        //this.val = this.getValue(); // save current value
        //console.log('this.getValue: ' + this.getValue());




        //console.log('cell: ', this.cell.val);


        // blocks onclick event
        //this.cell.firstChild.onclick=function(e){ (e||event).cancelBubble=true;}
        //this.cell.firstChild.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;}
    }


    this.getValue=function(){

        //return this.cell.getValue();
        console.log('getValue innerHTML: ' + this.cell.innerHTML);
        if (this.cell.firstChild)  {
            console.log('getValue innerHTML 1.1: ' + this.cell.innerHTML);
            //console.log('getValue innerHTML2: ' + this.cell.firstChild.childNodes[0].value);

            if (this.cell.firstChild.firstChild) {
                var newVal = this.cell.firstChild.firstChild.value;
            } else {
                var newVal = this.cell.firstChild.value;
            }

            console.log('getValue newVal: ' + newVal);
            return newVal;
        }


        //return this.cell.innerHTML;
    }


    this.setValue=function(val){
/* Начальная отрисовка */
        console.log('setValue 2: ' + val);

        //this.setCValue(val);

        if (typeof val == "undefined") {
            val = '';
        }

        if (val.length > 0) {
            console.log('upload 0');
            this.setCValue("<input type='hidden' value='"+val+"'><img src='"+val+"-/resize/25x/' style='cursor:pointer;'>", val);
        } else {
            console.log('upload 1');
            this.setCValue("<input type='hidden' value='"+val+"'><button style='line-height: 1;'>Upload</button>", val);
        }

        //$(this.cell).trigger( "onEditCell" );







        //this.setCValue('upload: ' + val);
    }
    this.detach=function(){
        // sets the new value
        //this.cell.wasChanged=false;
/*
        var newVal = this.cell.firstChild.firstChild.value;
        console.log('detach newVal: ' + newVal);


        //this.setValue(newVal);

        if (newVal == '') {
            console.log('detach false ');
            return false;
        } else {
            console.log('detach true');

            return true;
        }*/

        return this.val!=this.getValue(); // compares the new and the old values
    }


    this.mydialog=function(grid, id){

        console.log('file id', id);

        if (typeof id == "undefined") {
            id = '';
        }

        var file;
        if (id != '') {
            file = uploadcare.fileFrom('uploaded', id);
        } else {
            file = null;
        }

        var cellIndex = this.cell.cellIndex;
        var rowId = this.cell.parentNode.idd;



        var dialog = uploadcare.openDialog(file, 'file', {imagesOnly: true, crop: true}).done(function(file) {
            // this is a promise
            if (file)   { // promise was completed
                file.done(function(fileInfo) {
                    // this is an object with all the relevant file information
                    console.log(fileInfo.uuid);

                    grid.setValue(fileInfo.cdnUrl);




                    let reportSchedule_id = $('input[name=reportSchedule_id]').val();

                    // Сохраняем на сервер
                    let data = {};
                    data['rId'] = rowId;
                    data['cId'] = mygrid.getColumnId(cellIndex);


                    data['value'] = fileInfo.cdnUrl;

                    $.ajax({
                        url: '/report-schedule/insert-data',
                        type: 'post',
                        data:{reportSchedule_id:reportSchedule_id, data:data},
                        success:function(data) {
                            console.log('success instert after file upload');
                        }
                    });
                });
            }
        });




    }


}
eXcell_upload.prototype = new eXcell;

// ---------------------------------------------------------------------------------------------------------------------


function eXcell_ave(cell){
    if (cell){                // the default pattern, just copy it
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;

    }

    this.setValue=function(val){

        /*
        console.log('setValue: '+val);
        console.dir(this.cell);
        console.log('currency: ', this.cell.getAttribute('type'));
*/

        var varSplit = val.split(" ");

        this.setCValue(varSplit[0]+' '+varSplit[1]);
    }

    this.getValue=function(){
        //console.log('getValue ave');
        //return this.cell.innerHTML+' test'; // get value

        if (this.cell.childNodes[0].value)  {

            //console.dir('dir childNodes[0]', this.cell.childNodes[0]);
            //console.log('childNodes[0]', this.cell.childNodes[0]);
            //console.log(11111111111111111111);
            return this.cell.childNodes[0].value + ' ' + this.cell.childNodes[1].value; // get value
        } else {
            //console.log(22222222222222222);
            return this.cell.innerHTML;
        }

    }
    this.edit=function(){
        this.val = this.getValue(); // save current value

        console.log('this.val', this.val);

        this.cell.innerHTML = "<input type='text' style='width:70px;line-height: 1;' value=''>"
            + "<select style='width:50px;'><option value='EUR'>EUR"
            + "<option value='GBP'>GBP</select>"; // editor's html

        if (this.cell.childNodes[0].value)  {
            //this.cell.firstChild.value=parseInt(val); // set the first part of data
            console.log('empty ave field');
        } else {
            var varSplit;
            if (!this.val || this.val == '&nbsp;') {
                //this.val = '0 '+default_currency;
                varSplit = ['', default_currency];
                this.cell.firstChild.value = '';
            } else {
                varSplit = this.val.split(" ");
                this.cell.firstChild.value=parseInt(varSplit[0]); // set the first part of data
            }

            this.cell.firstChild.focus();
            this.cell.childNodes[1].value=varSplit[1];

        }

        // blocks onclick event
        this.cell.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;}
        this.cell.childNodes[1].onclick=function(e){ (e||event).cancelBubble=true;}
    }

    this.detach=function(){
        // sets the new value
        //this.setValue(this.cell.childNodes[0].value+" "+this.cell.childNodes[1].value);
        //return this.val!=this.getValue(); // compares the new and the old values

        //console.log('detach getValue - new: ' + this.cell.childNodes[0].value+"|"+this.cell.childNodes[1].value);
        //console.log('detach getValue - old: ' + this.val);
        //console.log('detach getValue - new: ' + this.getValue());

        this.setValue(this.cell.childNodes[0].value+' '+this.cell.childNodes[1].value);

        return true;

        return this.val!=this.getValue();
    }

}
eXcell_ave.prototype = new eXcell;

