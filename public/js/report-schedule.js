$(function() {

    let table =$('#report-schedules-table');
    let action = '/report-schedule/list';

    const columns = [
        {data: 'name', name: 'name'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ];


    table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            "url": action,
            "data": {
                "reportId": $('#report-schedules-table').data('report-id')
            },
        },
        columns: columns
    });

});