После создания контейнера выполнить эту команду, чтобы создался юзер reporting, иначе к базе будет не подключиться!

sudo docker exec `sudo docker ps -q --filter "ancestor=mysql/mysql-server:5.7"` /bin/sh -c 'mysql -u root -proot < /tmp/mysql/create_user.sql'
