<?php

namespace App\Listeners;

use App\Events\ScreenshotSaved;
use Illuminate\Support\Facades\Storage;



use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Filestack\FilestackClient;
use Filestack\FilestackSecurity;
use Filestack\Filelink;
use Filestack\FilestackException;

use App\Models\Screenshot;

class MakeScreenshot
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(Screenshot $screenshot)
    {




        $api_key = env('FILESTACK_API_KEY');
        $secret = env('YOUR_FILESTACK_SECURITY_SECRET');
        $security = new FilestackSecurity($secret);
        $client = new FilestackClient($api_key, $security);
// take a screenshot of a url
        $url = $screenshot['url'];
        $screenshot_filelink = $client->screenshot($url, [], 'desktop', 'all', 1920, 1080, 3000 );
        //$destination = __DIR__ . '/../tests/testfiles/screenshot-test.png';
        $destination = storage_path('app/public/screenshots')."/{$screenshot['_id']}.png";
        $result = $screenshot_filelink->download($destination);
# or get contents then save
        $contents = $screenshot_filelink->getContent();
        file_put_contents($destination, $contents);
// delete remote file
        $screenshot_filelink->delete();





        //Storage::put('loginactivity5.txt', 'test');
        //$message = $event->a;

        //Storage::put('loginactivity.txt', $message);

    }


}