<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @param  Store  $store
     * @return void
     */
    public function handle(Login $event)
    {

        $group = Auth::user()->active_group;
        if (!$group) {
            $group = $this->_setActiveContributorGroup();
        }

        Session::put('contributorGroupId', $group);


        $project = Auth::user()->active_project;


        if(!empty($project)) {
            Session::put('project', $project);
        }



        /*

        if(!$project) {
            $project = $this->_setActiveProject();
        }

        Session::put('project', $project);
*/

    }

    private function _setActiveContributorGroup()
    {
        if (Auth::user()->inRole('user')){
            $group = Auth::user()->contributorGroups->first()->group->getKey();
            $user = User::findOrFail(Auth::id());
            $user->contributorGroupId = $group;
            if($user->save())
                return $group;
            return false;
        } elseif (Auth::user()->inRole('admin') || Auth::user()->inRole('padmin')){
            return 0;
        }

    }

    private function _setActiveProject()
    {

        //return true;


            //print_r($project = Auth::user()->projects->first());exit;
            //print_r($project = Auth::user());exit;
            $project = Auth::user()->projects->first()->project->getKey();


            $user = User::findOrFail(Auth::id());
            $user->project = $project;
            if($user->save())
                return $project;

            return false;

    }
}
