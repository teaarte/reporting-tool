<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Facades\Datatables;

class Group extends \Moloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    private $rules = [
        'name' => 'required|max:255',
    ];

    public function users()
    {
        return $this->hasMany(GroupUsers::class);
    }

    public static function getList()
    {
        //$query = $this->select('*')->get();
        $query = self::select('*')->get();
        return $query;
    }

    public function datatables($data)
    {
        $editable = true;
        $deletable = true;

        return Datatables::of($data)
            ->addColumn('action', function ($item) use($editable, $deletable) {
                return $this->_generateActions($item, $editable, $deletable);
            })
            ->filterColumn('name', function($query, $keyword) {
                $query->where('name', 'like', '%'.$keyword.'%');
            })
            ->make(true);
    }

    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }

    private function _generateActions($item, $editable, $deletable)
    {
        $view = view('partials.table_actions', ['item' => $item, 'route' => 'group', 'editable' => $editable, 'deletable' => $deletable]);
        return $view->render();

    }
}
