<?php

namespace App\Models\Report;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Report\ReportSchedule;
use App\Models\Report\ReportFinal;


class Report extends \Moloquent
{
//    protected $fillable = ['index', 'user_id'];

    protected $fillable = ['name', 'from_date', 'to_date', 'template_id'];

    private $rules = [
        'name' => 'required|max:255',
        'from_date' => 'required|date|',
        'to_date' => 'nullable|date|after_or_equal:from_date',
        'template_id' => 'required',
        'period' => 'required',
    ];

    const OPEN_STATUS = 'open';

    const CLOSE_STATUS = 'close';

    public function data()
    {
        return $this->hasMany(ReportData::class)->orderBy('id');
    }

    public function template()
    {
        return $this->belongsTo(ReportTemplate::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function reportSchedule()
    {
        return $this->hasMany(ReportSchedule::class)->orderBy('date', 'desc');
    }

    public function reportFinal()
    {
        return $this->hasMany(ReportFinal::class);
    }



    public function hasGroupAccess()
    {
        if($this->group_id !== Auth::user()->contributorGroupId)
            return false;
        return true;
    }

    public function changeStatus($status)
    {
        if($status == 'open')
            $this->status = $this::OPEN_STATUS;
        elseif($status == 'close')
            $this->status = $this::CLOSE_STATUS;

        if($this->save())
            return true;
        return false;
    }


    // ------------------------
    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }

    public function createRow($data)
    {
        $this->name = $data['name'];
        $this->from_date = Carbon::parse($data['from_date'])->format('Y-m-d');
        $this->to_date =  Carbon::parse($data['to_date'])->format('Y-m-d');
        $this->template_id = $data['template_id'];
        $this->period = $data['period'];

        if($this->save()){

            ReportSchedule::createMultiple($this->id);
            return true;
        }
        return false;
    }
}
