<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Models\Report\Report;
use App\Models\Report\ReportSchedule;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Validator;

class ReportFinal extends \Moloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    private $rules = [
        'name' => 'required|max:255',
    ];

    public function report()
    {
        return $this->belongsTo(Report::class);
    }

    public function reportSchedule()
    {
        return $this->belongsTo(ReportSchedule::class);
    }



    public static function getList()
    {

        //$query = $this->select('*')->get();
        //$query = self::select('*')->where('type', '!=', 'main')->get();
        $query = self::select('*')->get();

        //print_r($query);exit;
        return $query;
    }





    public function datatables($data)
    {
        $editable = true;
        $deletable = true;

        return Datatables::of($data)
            ->addColumn('action', function ($item) use($editable, $deletable) {
                return $this->_generateActions($item, $editable, $deletable);
            })
            ->filterColumn('name', function($query, $keyword) {
                $query->where('name', 'like', '%'.$keyword.'%');
            })
            ->make(true);
    }

    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }

    private function _generateActions($item, $editable, $deletable)
    {
        $view = view('partials.table_actions', ['item' => $item, 'route' => 'project', 'editable' => $editable, 'deletable' => $deletable]);
        return $view->render();

    }


    public function addNew($name, $from_ts, $to_ts, $contributorGroupId = 0, $link = '') {


        $row = new $this;
        $row->name = $name;
        $row->from_ts = $from_ts;
        $row->to_ts = $to_ts;
        $row->contributorGroupId = $contributorGroupId;
        $row->link = $link;


        if ($row->save()) {
            return true;
        } else {
            return false;
        }

    }
}
