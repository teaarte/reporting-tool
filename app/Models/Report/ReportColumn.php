<?php

namespace App\Models\Report;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Validator;

class ReportColumn extends \Moloquent
{
    protected $table = 'fields';

    protected $fillable = ['index', 'title', 'type', 'data', 'order'];

    public $types = ['string', 'checkbox', 'combo', 'number', 'image', 'text', 'date', 'multiselect', 'upload', 'ave'];

    private $rules = [
        'index' => 'required|max:255',
        'title' => 'required|max:255',
        'type' => 'required',
    ];

    public function getList()
    {
        $query = $this->select('*')->get();
        return $query;
    }

    public function datatables($data)
    {
        $editable = true;
        $deletable = true;

        return Datatables::of($data)
            ->addColumn('action', function ($item) use($editable, $deletable) {
                return $this->_generateActions($item, $editable, $deletable);
            })
            ->make(true);
    }

    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        if(isset($data['type']) && ($data['type'] == 'combo' || $data['type'] == 'multiselect')) {
            $this->rules['data'] = 'required';
        }


        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }

    private function _generateActions($item, $editable, $deletable)
    {
        $view = view('partials.table_actions', ['item' => $item, 'route' => 'report.column', 'editable' => $editable, 'deletable' => $deletable]);
        return $view->render();
    }
}
