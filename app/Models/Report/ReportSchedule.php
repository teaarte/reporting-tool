<?php

namespace App\Models\Report;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Validator;
use App\Models\Report\ReportFinal;
use Illuminate\Support\Facades\Auth;


class ReportSchedule extends \Moloquent
{

    protected $fillable = [
        'name',
        'report_id'
    ];

    private $rules = [
        'name' => 'required|max:255',
        'report_id' => 'required|string',
    ];

    protected $casts = [
        'report_id' => 'string',
    ];


    public function report()
    {
        //return 'bla';
        //return $this->belongsTo('App\Models\Report\Report');
        return $this->belongsTo(Report::class);
    }

    public function reportFinal()
    {
        return $this->hasMany(ReportFinal::class);
    }



    public function getList($reportId)
    {

        if (empty($reportId)) {
            $query = $this->select('*')->get();
        } else {
            $query = $this->select('*')->where('report_id', '=', new \MongoDB\BSON\ObjectID($reportId))->get();
        }

        //$query = self::select('*')->get();
        return $query;
    }


    public function datatables($data)
    {
        $editable = true;
        $deletable = true;

        return Datatables::of($data)
            ->addColumn('action', function ($item) use($editable, $deletable) {
                return $this->_generateActions($item, $editable, $deletable);
            })
            ->filterColumn('name', function($query, $keyword) {
                $query->where('name', 'like', '%'.$keyword.'%');
            })
            ->make(true);
    }


    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }

    private function _generateActions($item, $editable, $deletable)
    {
        $view = view('partials.table_actions', ['item' => $item, 'route' => 'report-schedule', 'editable' => $editable, 'deletable' => $deletable]);
        return $view->render();

    }


    public static function createMultiple($report_id)
    {

        //$report_id = $request['id'];
        $report = Report::findOrFail($report_id);
        //$report['period'] = 'monthly';
        //$report['period'] = 'daily';

        /*
        $startDate = date('Y-m-d');
        if (strtotime($report['from_date']) > strtotime($startDate)) {
            $startDate = $report['from_date'];
        }
        */

        $startDate = $report['from_date'];
        $dates = [$startDate];

        switch ($report['period']) {

            case 'quarterly':
                $max_i = 4;
                $type = 'day';
                break;


            case 'monthly':
                $max_i = 12;
                $type = 'month';
                break;

            case 'daily':
                $max_i = 30;
                $type = 'day';
                break;


            default:
                exit;

        }


        // Получаем максимальную созданную дату на объекте
        $maxDateByReport = ReportSchedule::where('report_id', $report_id)->max('date');


        if (strtotime("now") < strtotime($report['to_date'])) {


            $time = strtotime($dates[0]);
            for ($i = 1; $i <= $max_i; $i++) {
                $final = date("Y-m-d", strtotime("+{$i} {$type}", $time));
                if ( $final < strtotime($maxDateByReport)) {
                    continue;
                } elseif (strtotime($final) > strtotime($report['to_date'])) {
                    break;
                } else {
                    $dates[] = $final;
                }
            }
        }



        //print_r($dates); exit;

        foreach ($dates as $date) {
            $ReportSchedule = new ReportSchedule();

            $ReportSchedule->name = $date;
            $ReportSchedule->date = $date;
            $ReportSchedule->report_id = "$report_id";
            $ReportSchedule->save();

        }



    }



    public function getCountOfDataByContributorGroup($contributorGroupId) {

        $count = \App\Models\Report\ReportData::where('contributorGroupId', (string)$contributorGroupId)->count();

        return $count;
    }


}

