<?php

namespace App\Models\Report;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class MyDatabase extends \Moloquent
{
    protected $table = 'databases';

    protected $fillable = ['name', 'columns'];

    private $rules = [
        'name' => 'required|max:255',
        'columns' => 'required|max:255',
    ];

    public function items()
    {
        return $this->hasMany(DatabaseItem::class, 'list_id')->orderBy('id');
/*
        return $this->hasMany(DatabaseItem::class, 'list_id')->whereHas('contributorGroups', function ($query) {
                $query->where('contributorGroupId', Session::get('contributorGroupId'));
            })->orderBy('id');
*/

        //return $this->hasMany(DatabaseItem::class, 'list_id')->where('contributorGroupId', Session::get('contributorGroupId'))->orderBy('id');

    }


    public function items2()
    {

        if (Session::get('contributorGroupId') == 0) {
            return $this->hasMany(DatabaseItem::class, 'list_id')->orderBy('id');
        } else {
            return $this->hasMany(DatabaseItem::class, 'list_id')->where('contributorGroupId', Session::get('contributorGroupId'))->orderBy('id');
        }



    }



    public function scopeOfContributorGroup($query, $contributorGroupId)
    {
        return $query->where('contributorGroupId', $contributorGroupId);
    }

    public function scopeOfProject($query, $project_id)
    {
        return $query->where('projectId', $project_id);
    }


    public function getList($contributorGroupId = null)
    {
        if (empty($contributorGroupId)) {
            $query = $this->select('*')->where('contributorGroupId', $contributorGroupId)->get();
        } else {
            $query = $this->select('*')->where('contributorGroupId', $contributorGroupId)->get();
        }

        return $query;
    }

    public function datatables($data)
    {
        $editable = true;
        $deletable = true;
        $fillable = true;

        return Datatables::of($data)
            ->addColumn('action', function ($item) use($editable, $deletable, $fillable) {
                return $this->_generateActions($item, $editable, $deletable, $fillable);
            })
            ->make(true);
    }

    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }



    private function _generateActions($item, $editable = false, $deletable = false, $fillable = false)
    {
        $view = view('partials.table_actions', ['item' => $item, 'route' => 'report.database', 'editable' => $editable, 'deletable' => $deletable, 'fillable' => $fillable]);
        return $view->render();
    }


    public function topics() {

        $topics = \App\Models\Report\DatabaseItem::where('list_id', '599c91ba4e801f00076dd217')->get();

        return $topics;
    }

}
