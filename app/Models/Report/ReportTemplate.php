<?php

namespace App\Models\Report;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
//use Jenssegers\Mongodb\Eloquent\Model as Model;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Validator;

class ReportTemplate extends \Moloquent
{
    use Sluggable, SluggableScopeHelpers;

    const MONTHLY_REPORT = 'monthly-report';

    protected $table = 'reports_templates';

    protected $fillable = ['name', 'fields', 'fields1'];


    private $rules = [
        'name' => 'required|max:255',
        'fields' => 'required',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getList()
    {
        $query = $this->select('*')->get();
        return $query;
    }

    public function datatables($data)
    {
        $editable = true;
        $deletable = true;

        return Datatables::of($data)
            ->addColumn('action', function ($item) use($editable, $deletable) {
                if($item->slug == $this::MONTHLY_REPORT)
                    $deletable = false;
                return $this->_generateActions($item, $editable, $deletable);
            })
            ->make(true);
    }

    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }

    private function _generateActions($item, $editable, $deletable)
    {
        $view = view('partials.table_actions', ['item' => $item, 'route' => 'report.template', 'editable' => $editable, 'deletable' => $deletable]);
        return $view->render();

    }
}
