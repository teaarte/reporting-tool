<?php

namespace App\Models\Report;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Report\ReportSchedule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ReportData extends \Moloquent
{

    const OPEN_STATUS = 'open';

    const CLOSE_STATUS = 'close';


    protected $table = 'reports_data';
/*
    protected $fillable = ['name', 'from_date', 'to_date', 'tpl_id'];

    private $rules = [
        'name' => 'required|max:255',
        'from_date' => 'required|date|',
        'to_date' => 'nullable|date|after_or_equal:from_date',
        'tpl_id' => 'required',
        'period' => 'required',
    ];


    public function reportSchedule()
    {
        return $this->hasMany(ReportSchedule::class);
    }


    public function validate($data)
    {
        if(!$data) {
            return false;
        }

        $v = Validator::make($data, $this->rules);
        $v->validate();
        if ($v->fails()) {
            return false;
        }
        return true;
    }

    public function createRow($data)
    {
        $this->name = $data['name'];
        $this->from_date = Carbon::parse($data['from_date'])->format('Y-m-d');
        $this->to_date =  Carbon::parse($data['to_date'])->format('Y-m-d');
        $this->tpl_id = $data['tpl_id'];
        $this->period = $data['period'];

        if($this->save()){
            return true;
        }
        return false;
    }

*/

    public function add($data, $report_id)
    {
        if ($data['value'] == 'true') {
            $data['value'] = true;
        }  elseif($data['value'] == 'false') {
            $data['value'] = false;
        }



        //print_r($data);exit;
/*
        if (!empty($data['key_id'])) {
            // Пишем айди
            $result = $this->where('id', intval($data['id']))->where('reportSchedule_id', $report_id)->update([$data['key_id'] => $data['value_id'] ]);
        } else {
            // Пишем значение
            $result = $this->where('id', intval($data['id']))->where('reportSchedule_id', $report_id)->update([$data['key'] => $data['value'] ]);
        }*/

        //$result = $this->where('id', intval($data['id']))->where('reportSchedule_id', $report_id)->where('contributorGroupId', Session::get('contributorGroupId'))->update([$data['key'] => $data['value'] ]);

        /*
        echo "reportSchedule_id: $report_id \n";
        echo "contributorGroupId: ".Session::get('contributorGroupId')." \n";
        print_r($data);
        */

        // s = 1 означает, что строка наполнена хоть какой-то инфой и не полностью пустая.


        $result = $this->where('_id', new \MongoDB\BSON\ObjectID($data['rId']))
            ->where('reportSchedule_id', $report_id)
            ->where('contributorGroupId', Session::get('contributorGroupId'))
            ->update([$data['cId'] => $data['value'], 's' => 1 ]);

        //echo "result: ".$result;


        if ($result) {
            return true;
        } else {
            return false;
        }


    }

    public function addRow($id, $template_id, $ReportSchedule)
    {


        //$ReportTemplate = ReportTemplate::where('template_id', $template_id)->get();
        $ReportTemplate = ReportTemplate::find(new \MongoDB\BSON\ObjectID($template_id));
        //print_r($ReportTemplate);exit;

        //echo $ReportTemplate->name; exit;

        $fields = array();

        foreach ($ReportTemplate->fields as $field){
            $fields[] = ReportColumn::findOrFail($field);
        }


        //print_r($columns);exit;


        $row = new ReportData;

        $row->id = intval($id);
        $row->report_id = $ReportSchedule->report_id;
        $row->reportSchedule_id = $ReportSchedule->getKey();
        $row->contributorGroupId = Auth::user()->contributorGroupId ?? 0;
        $row->user_id = Auth::user()->id;

        $date = new \DateTime($ReportSchedule->date);
        $date->setTime(0,0,0);

        $row->date_ts = $date->getTimestamp();

        foreach ($fields as $field){
            $row->attributes[$field->index] = "";
        }

        if($row->save())
            return true;


        return false;
    }


    public function scopeByScheduleAndContributorGroup($query, $reportSchedule_id)
    {

        $contributor_group_id = Session::get('contributorGroupId') ?? 0;

        if (Session::get('contributorGroupId') == 0) {
            return $query->where('reportSchedule_id', $reportSchedule_id);
        } else {
            return $query->where('reportSchedule_id', $reportSchedule_id)->where('contributorGroupId', $contributor_group_id);
        }
        

    }



    public function scopeFindReportByIndex($query, $index)
    {
        return $query->where('index', $index)->where('group_id', Auth::user()->contributorGroupId);
    }

    public function addWithData($ReportSchedule)
    {


        //$k = $ReportSchedule->report();
        //$k = $ReportSchedule->id();


        //$report = ReportSchedule::find($ReportSchedule->id)->report();
        //print_r($report);

        $report_id = ReportSchedule::find($ReportSchedule->id)->report_id;
        //echo $report_id;

        $template_id = Report::find($report_id)->template_id;
        //echo $template_id;

        //exit;


        $reportData = new ReportData();
        /*
        $this->index = $index;
        $this->group_id = Auth::user()->contributorGroupId;
        $this->type_id = $type;
        $this->status = $this::OPEN_STATUS;
        */
        //if($this->save()) {
            for($i = 1; $i <= 5; $i++){
                //$reportData->addRow($i, $template_id, $ReportSchedule);
            }
        //}
        return $this;
    }

    public function removeRow($id, $reportSchedule_id)
    {
        //$data = $this->where('id', intval($id))->where('reportSchedule_id', $reportSchedule_id)->where('contributorGroupId', Session::get('contributorGroupId'));
        $row = $this->where('_id', new \MongoDB\BSON\ObjectID($id))->where('reportSchedule_id', $reportSchedule_id);

        if($row->delete())
            return true;
        return false;
    }

    public function generateNewRow($reportSchedule_id) {
        $row = new $this;
        $row->reportSchedule_id = $reportSchedule_id;
        $row->contributorGroupId = Session::get('contributorGroupId') ?? 0;
        $row->user_id = Auth::user()->id;

        // Индикатор о том, что строка создана, но не наполнена информацией
        $row->s = 0;

        $ReportSchedule = ReportSchedule::find($reportSchedule_id);

        $report_id = $ReportSchedule->report_id;
        $row->report_id = $report_id;

        $date = new \DateTime($ReportSchedule->date);
        $date->setTime(0,0,0);
        $row->date_ts = $date->getTimestamp();




        if ($row->save()) {
            return $row->getKey();
        }


    }


}
