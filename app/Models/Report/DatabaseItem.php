<?php

namespace App\Models\Report;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DatabaseItem extends \Moloquent
{
    protected $table = 'databaseItems';

    public function contributor()
    {
        //return $this->belongsTo('App\Models\Group');
        return $this->hasOne('App\Models\Group', 'contributorGroupId');


    }

    public function add($data, $list_id)
    {

        /*
        if (Session::get('contributorGroupId') == 0) {
            $row = $this->where('id', intval($data['id']))->where('list_id', $list_id)->first();
        } else {
            $row = $this->where('id', intval($data['id']))->where('list_id', $list_id)->where('contributorGroupId', Session::get('contributorGroupId'))->first();
        }*/

        //print_r($data);
        //$row = $this->where('_id', new \MongoDB\BSON\ObjectID($data['rId']))->where('list_id', $list_id)->where('contributorGroupId', Session::get('contributorGroupId'))->first();

        $row = $this->where('_id', new \MongoDB\BSON\ObjectID($data['rId']))->where('list_id', $list_id)->first();



        if ($row) {

            //echo "exists!";exit;
            $row->attributes[$data['cId']] = $data['value'];

            if($row->save())
                return true;
        }
        else {


            $row = new $this;
            $row->list_id = $list_id;
//            $row->_id = new \MongoDB\BSON\ObjectID($data['rId']);
            $row->contributorGroupId = Session::get('contributorGroupId') ?? 0;
            $row->user_id = Auth::user()->id;
            $row->attributes[$data['cId']] = $data['value'];

            //print_r($row); exit;


            if ($row->save()) {
                return true;
            }


        }
        return false;
    }

    public function removeRow($id, $list_id)
    {
        //$row = $this->where('_id', new \MongoDB\BSON\ObjectID($id))->where('list_id', $list_id)->where('contributorGroupId', Session::get('contributorGroupId'));
        $row = $this->where('_id', new \MongoDB\BSON\ObjectID($id))->where('list_id', $list_id);

        if ($row->delete()) {
            return true;
        }

        return false;
    }


    public function generateNewRow($db_id) {
        $row = new $this;
        $row->list_id = $db_id;
        $row->contributorGroupId = Session::get('contributorGroupId') ?? 0;
        $row->user_id = Auth::user()->id;

        if ($row->save()) {
            return $row->getKey();
        }


    }

    public function generateNewExcelImportRow($list_id, $contributorGroupId, $media_name, $sector, $unique_visitor, $circulation, $media_type) {
        $row = new $this;
        $row->list_id = $list_id;
        $row->contributorGroupId = $contributorGroupId;
        $row->user_id = Auth::user()->id;
        $row->name = $media_name;
        $row->sector = $sector;
        $row->unique_visitors = $unique_visitor;
        $row->circulation = $circulation;
        $row->media_type = $media_type;

        if ($row->save()) {
            return $row->getKey();
        }

    }









}
