<?php

namespace App\Models;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Models\Sentinel\EloquentUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Facades\Datatables;

class User extends EloquentUser implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'generated_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    private $rules = [
        'email' => 'required|email|max:255|unique:users,email',
        'group' => 'required'
    ];

    public function contributorGroups()
    {
        return $this->hasMany(GroupUsers::class);
    }

    public function projects()
    {
        return $this->hasMany(\App\Models\Project::class);
    }

    public function getList()
    {
        $userRole = Sentinel::findRoleBySlug('user');
        $query = $this->select('*')->whereIn('role_id', [$userRole->getKey()])->get();
        return $query;
    }

    public function datatables($data)
    {
        $editable = true;
        $deletable = true;
        $groupList = null;

        return Datatables::of($data)
            ->addColumn('action', function ($item) use($editable, $deletable) {
                return $this->_generateActions($item, $editable, $deletable);
            })
            ->editColumn('groups', function ($item) use($groupList) {
                foreach ($item->contributorGroups as $group){
                    $groupList = $groupList.','.$group->group->name;
                }
                return trim($groupList,',');
            })
            ->filterColumn('email', function($query, $keyword) {
                $query->where('email', 'like', '%'.$keyword.'%');
            })
            ->make(true);
    }

    public function createRow($data)
    {
        $password = str_random(10);

        $this->email = $data['email'];
        $this->password = bcrypt($password);
        $this->generated_password = $password;

        if($this->save()){
            $userRole = Sentinel::findRoleBySlug('user');
            $userRole->users()->attach($this);

            foreach ($data['group'] as $group){
                $groupUsers = new GroupUsers();
                $groupUsers->user_id = $this->id;
                $groupUsers->group_id = $group;
                $groupUsers->save();
            }

            return true;
        }
    }

    public function updateRow($data)
    {
        $this->email = $data['email'];
        if($this->update()){
            GroupUsers::where('user_id', $this->getKey())->delete();
            foreach ($data['group'] as $group){
                $groupUsers = new GroupUsers();
                $groupUsers->user_id = $this->id;
                $groupUsers->group_id = $group;
                $groupUsers->save();
            }
            return true;
        }
    }

    public function validate($data, $id = null)
    {
        if(!$data) {
            return false;
        }

        $this->rules['email'] =  $this->rules['email'] . ',' . $id . ',_id';

        $v = Validator::make($data, $this->rules);
        $v->validate();

        if ($v->fails()) {
            return false;
        }

        return true;
    }

    public function inGroup($id)
    {
        if($id === null)
            return false;
        foreach ($this->groups as $group){
            if($group->group_id == $id)
                return true;
        }
        return false;
    }

    public function setActiveContributorGroup($group)
    {
        $this->contributorGroupId = $group;
        if($this->save()){
            //Session::put(Auth::id().'_contributor-group', $group);
            Session::put('contributorGroupId', $group);
            return true;
        }
        return false;
    }

    public function setActiveProject($project)
    {
        $this->active_project = $project;
        if($this->save()){
            //Session::put(Auth::id().'_user_project', $project);
            Session::put('project', $project);
            return true;
        }
        return false;
    }

    private function _generateActions($item,  $editable, $deletable)
    {
        $view = view('partials.table_actions', ['item' => $item, 'route' => 'user', 'editable' => $editable, 'deletable' => $deletable]);
        return $view->render();

    }
}
