<?php

namespace App\Models;


class Charts
{
    //

    public static function getStackedBar($data) {
        /*
        $data = [
            'infile' => [
                'chart' => [
                    'type' => 'bar'
                ],
                'title' => [
                    'text' => "Steep Chart"
                ],
                'xAxis' => [
                    'categories' => ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                        'text' => 'Total fruit consumption'
                    ]
                ],
                'legend' => [
                    'reversed' => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'stacking' => 'normal'
                    ],
                ],
                'series' => [
                    [
                        'name' => 'John',
                        'data' => [5, 3, 4, 7, 2]
                    ],
                    [
                        'name' => 'Jane',
                        'data' => [2, 2, 3, 2, 1]
                    ],
                    [
                        'name' => 'Joe',
                        'data' => [3, 4, 4, 2, 5]
                    ],

                ]

            ],
            //'async' => true,
        ];
*/


        $result = self::sendRequest($data);
        return $result;
    }

    public static function getPie($data) {

/*
        $data = [
            'infile' => [
                'title' => [
                    'text' => "Steep Chart"
                ],
                'xAxis' => [
                    "categories" => ["Jan", "Feb", "Mar"]
                ],
                'series' => [
                    'data' => [29.9, 71.5, 106.4]
                ]

            ],
            'async' => true,
        ];
*/


        $result = self::sendRequest($data);
        return $result;
    }




    protected static function sendRequest($data)
    {



        $data_string = json_encode($data);

        //$data_string = http_build_query($data);

        //echo $data_string; exit;


        //$data_string = str_replace("\n", '', $data_string);
        //echo $data_string; exit;

        //$data_string = '{"infile":{"title": {"text": "Steep Chart"}, "xAxis": {"categories": ["Jan", "Feb", "Mar"]}, "series": [{"data": [29.9, 71.5, 106.4]}]}}';

        //$curl_log = fopen(storage_path()."/curl.txt", 'rw'); // open file for READ and write

        //$data_string = '{"infile":{"title": {"text": "Steep Chart"}, "xAxis": {"categories": ["Jan", "Feb", "Mar"]}, "series": [{"data": [29.9, 71.5, 106.4]}]}}';


        //$ch = curl_init('http://charts.alcatel-mobile-reporting.com:8889/stocks/add');
        $ch = curl_init('http://charts.alcatel-mobile-reporting.com:8889');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            )
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        //curl_setopt($ch, CURLOPT_STDERR, $curl_log);
        curl_setopt($ch, CURLOPT_VERBOSE, true);



        $result = curl_exec($ch);
        //$information = curl_getinfo($ch);
        //print_r($information);
        //echo $debug;
        /*
                rewind($curl_log);
                $output= fread($curl_log, 2048);
                echo "<pre>". print_r($output, 1). "</pre>";
                fclose($curl_log);*/

//close connection
        curl_close($ch);

        return $result;
    }


}
