<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class GroupUsers extends \Moloquent
{
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
