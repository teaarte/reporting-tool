<?php

namespace App\Models\Presentation;

use App\Models\Report\DatabaseItem;
use App\Models\Report\ReportData;
use Illuminate\Database\Eloquent\Model;
use App\Libs\SynthesioApi;
use App\Models\Charts;
use App\Models\Report\MyDatabase;

class IFA_Berlin_2017_Model extends \Moloquent
{

    protected
        $allowedTopics = [
            'IDOL 5',
            'IDOL 5S',
            'Alcatel A7',
            'Alcatel A7 XL',
            'Alcatel Mobile',
            'Palm'
        ],

        $competitors = [
            'Alcatel Mobile',
            'Nokia',
            'Asus',
            'Motorola',
            'HTC',
            'Honor',
            'LG',
            'Sony',
            'Huawei',
            'Wiko',
        ],

        $topCountries = [
            ['_id' => '599a893c5dbf5f00062d0604', 'name' => 'Germany', 'code' => 'DE'],
            ['_id' => '599a8cad5dbf5f00075dcd9a', 'name' => 'France', 'code' => 'FR'],
            ['_id' => '599a8ca25dbf5f00062d060b', 'name' => 'Russia', 'code' => 'RU'],
            ['_id' => '599a89455dbf5f00075dcd94', 'name' => 'UK',   'code' => 'UK'],
            ['_id' => '599a8cfe5dbf5f00075dcd9c', 'name' => 'IT',   'code' => 'IT'],
            ['_id' => '599a8cec5dbf5f00062d060c', 'name' => 'SP',   'code' => 'SP'],
            ['_id' => '599a8ce25dbf5f00075dcd9b', 'name' => 'Poland', 'code' => 'PL'],
        ],

        $schedule_ids,
        $contributorGroup,
        $from_ts,
        $from_date,
        $to_ts,
        $to_date;



    public function setDate($from_ts, $to_ts) {
        $this->from_ts = $from_ts;
        $this->from_date = date('Y-m-d H:i:s', $from_ts);

        $this->to_ts = $to_ts;
        $this->to_date = date('Y-m-d H:i:s', $to_ts);
    }

    public function setContributorGroup($contributorGroup) {
        $this->contributorGroup = $contributorGroup;
    }

    public function setSchedules($schedule_ids = []) {
        $this->schedule_ids = $schedule_ids;
    }


    public function getData() {

        $reportData = new ReportData();

        $dataBaseItem = DatabaseItem::all();

        $ave_generated = 0;

        $audience_reach = 0;

        $articles_published = $reportData->count();

        $positive_statements = $reportData->where('sentiment', '599c90f74e801f0006148023')->where('reportSchedule_id', ['$in' => $this->schedule_ids])->count();
        $negative_statements = $reportData->where('sentiment', '599c91034e801f0006148025')->where('reportSchedule_id', ['$in' => $this->schedule_ids])->count();
        $neutral_statements = $reportData->where('sentiment', '599c90fe4e801f0006148024')->where('reportSchedule_id', ['$in' => $this->schedule_ids])->count();

        $positive_statements = $positive_statements + $neutral_statements;


        $data = $reportData->where('reportSchedule_id', ['$in' => $this->schedule_ids])->get();


        //print_r($data);exit;

        //print_r($this->schedule_ids);exit;

/*
        echo 'positive_statements '.$positive_statements."<br>\n";
        echo 'negative_statements '.$negative_statements."<br>\n";
        echo 'neutral_statements '.$neutral_statements;
        exit;
        */

        foreach ($data as $element) {
            if (isset($element['ave']['def'])) {
                $ave = $element['ave']['EUR'] ?? $element['ave']['GBP'] ?? 0;
            } else {
                $ave = $element['ave'];
            }


            $a = preg_replace("/[^0-9]/", '', $ave);
            $ave_generated += intval($a);
        }

        foreach ($data as $element) {
            $id = $element['media_name'];

            if ($id == 'Ammassi.it') {
                //print_r($element);exit;
            }

            if ($id == 'sim.bg' || $id == 'Belrynok.ru' || trim($id) == 'Belrynok.ru') {
                continue;
            }


            try {
                if ($id) {
                    $media = $dataBaseItem->where('_id', new \MongoDB\BSON\ObjectID($id))->first();
                    $audience_reach += intval($media['unique_visitors']);
                    $audience_reach += intval($media['circulation']);

                }
            } catch (\Exception $e) {
                continue;
            }

        }

        $audience_reach = round($audience_reach / 1000000, 1).' MN';

        $data = [
            'ave_generated' => $ave_generated,
            'articles_published' => $articles_published,
            'positive_statements' => $positive_statements,
            'audience_reach' => $audience_reach
        ];

        return $data;
    }



    public function getPieTopicsPR() {

        $dayStart = date("Y-m-d");


        // Берем топики
        $topics = \App\Models\Report\DatabaseItem::where('list_id', '599c91ba4e801f00076dd217')->get();

        $items = [];
        $total = 0;

        $reportData = new ReportData;
        $topicsData = [];
        foreach ($topics as $topic) {


            $count = $reportData->where('topic', (string)$topic['_id'])->where('reportSchedule_id', ['$in' => $this->schedule_ids])->count();


            // Other
            /*
            if ((string)$topic['_id'] == '599c9f3f4e801f0006148033') {
                $topicsData['59ad78d9549eaa06513122f2'] = $count;
            } else {
                $topicsData[(string)$topic['_id']] = $count;
            }
*/

            $topicsData[(string)$topic['_id']] = $count;







            $total += $count;

        }


        foreach ($topics as $topic) {

            if ($total > 0) {
                $percent = round($topicsData[(string)$topic['_id']]*100/$total, 2);
            } else {
                $percent = 0;
            }




            if ($topic['name'] == 'OTHER') {
                $topic['name'] = 'Family Watch MT30';

            } elseif ($topic['name'] == 'Family Watch MT30') {
                $topic['name'] = 'OTHER';
            }



            $items[] = [
                'name' => "<b>{$topic['name']}</b>: {$percent}%",
                'y' => $percent,
            ];

        }



        //print_r($items); exit;

        $pieData = [
            'infile' => [
                'credits' => [
                    'enabled' => false
                ],
                'legend' => [
                    'itemStyle' => [
                        'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                        'fontSize' => '17px',
                    ],
                ],
                'chart' => [
                    'spacingTop' => 0,
                    'spacingBottom' => 0,
                    'spacingLeft' => 0,
                    'spacingRight' => 0,
                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie'
                ],
                'title' => [
                    'text' => null
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' =>  'pointer',
                        'showInLegend' => true,

                        'dataLabels' => [
                            'enabled' => false,
                            /*'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            'style' => [
                                'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                                'fontSize' => '22px'
                            ]*/
                        ]
                    ]
                ],
                'series' => [[
                    'name' => 'Test',
                    'colorByPoint' => true,
                    'data' => $items,
                ]]

            ],
            //'async' => true,
        ];

        $imageData = Charts::getPie($pieData);

        return $imageData;
    }


    public function getPieCountriesSocial() {

        $api = new SynthesioApi();
        $data = $api->getMentionCounts($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);
        //$data = true;

        $items = [];

        $total = 0;


    }

    public function getPieTopicsSocial() {

        return '';

        //$dayStart = date("Y-m-d");

        $api = new SynthesioApi();
        $data = $api->getMentionCounts($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);
        //$data = true;


        $items = [];

        $total = 0;

        // Считаем общее кол-во пунктов, чтобы правильно затем высчитать проценты
        foreach ($data as $name => $v) {
            if (!in_array($name, $this->allowedTopics)) {
                continue;
            }
            $total += (float)$v;
        }


        foreach ($data as $name => $v) {
            if (!in_array($name, $this->allowedTopics)) {
                continue;
            }

            if ($total > 0) {
                $percent = round($v*100/$total, 2);
            } else {
                $percent = 0;
            }



            $items[] = [
                'name' => "<b>{$name}</b>: {$percent}%",
                'y' => $percent,
            ];
        }





        //print_r($items); exit;

        $pieData = [
            'infile' => [
                'credits' => [
                    'enabled' => false
                ],
                'legend' => [
                    'itemStyle' => [
                        'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                        'fontSize' => '17px',
                    ],
                ],
                'chart' => [
                    'spacingTop' => 0,
                    'spacingBottom' => 0,
                    'spacingLeft' => 0,
                    'spacingRight' => 0,
                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie'
                ],
                'title' => [
                    'text' => null
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' =>  'pointer',
                        'dataLabels' => [
                            'enabled' => false,
                            /*'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            'style' => [
                                'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                                'fontSize' => '22px'
                            ]*/
                        ]
                    ]
                ],
                'series' => [[
                    'name' => 'Test',
                    'colorByPoint' => true,
                    'data' => $items,
                ]]

            ],
            //'async' => true,
        ];

        $imageData = Charts::getPie($pieData);

        return $imageData;
    }

    public function getPieTopicsPRSocial() {


        $allowedTopics = [
            'IDOL 5',
            'IDOL 5S',
            'Alcatel A7',
            'Alcatel A7 XL',
            'Alcatel Mobile',
            'A7',
            'A7 XL',
        ];

        //$dayStart = date("Y-m-d");

        $api = new SynthesioApi();
        $data = $api->getMentionCounts($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);


        //print_r($data);exit;

        $items = [];

        $total = 0;

        // Считаем общее кол-во пунктов, чтобы правильно затем высчитать проценты
        foreach ($data as $name => $v) {
            if (!in_array($name, $allowedTopics)) {
                continue;
            }
            $total += (float)$v;
        }


        foreach ($data as $name => $v) {
            if (!in_array($name, $allowedTopics)) {
                continue;
            }

            if ($total > 0) {
                $percent = round($v*100/$total, 2);
            } else {
                $percent = 0;
            }



            $items[] = [
                'name' => "<b>{$name}</b>: {$percent}%",
                'y' => $percent,
            ];
        }





        //print_r($items); exit;

        $pieData = [
            'infile' => [
                'credits' => [
                    'enabled' => false
                ],
                'legend' => [
                    'itemStyle' => [
                        'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                        'fontSize' => '17px',
                    ],
                ],
                'chart' => [
                    'spacingTop' => 0,
                    'spacingBottom' => 0,
                    'spacingLeft' => 0,
                    'spacingRight' => 0,

                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie'
                ],
                'title' => [
                    'text' => null
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' =>  'pointer',
                        'showInLegend' => true,
                        'dataLabels' => [
                            'enabled' => false,
                            /*'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            'style' => [
                                'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                                'fontSize' => '22px'
                            ]*/
                        ]
                    ]
                ],
                'series' => [[
                    'name' => 'Test',
                    'colorByPoint' => true,
                    'data' => $items,
                ]]

            ],
            //'async' => true,
        ];

        $imageData = Charts::getPie($pieData);

        return $imageData;
    }



    public function getPiePerCountryPR() {



        $reportSchedule = new \App\Models\Report\ReportSchedule();
        $items = [];
        $total = 0;
        $data = [];

        foreach ($this->topCountries as $country) {
            // Получаем кол-во статей для каждой страны

            $count = $reportSchedule->getCountOfDataByContributorGroup((string)$country['_id']);
            $data[(string)$country['_id']] = $count;

            // Считаем общее кол-во пунктов, чтобы правильно затем высчитать проценты
            $total += (float)$count;
        }


        foreach ($this->topCountries as $country) {

            $percent = round($data[(string)$country['_id']]*100/$total, 2);


            $items[] = [
                'name' => "<b>{$country['name']}</b>: {$percent}%",
                'y' => $percent,
            ];
        }





        //print_r($items); exit;

        $pieData = [
            'infile' => [
                'credits' => [
                    'enabled' => false
                ],
                'legend' => [
                    'itemStyle' => [
                        'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                        'fontSize' => '17px',
                    ],
                ],
                'chart' => [
                    'spacingTop' => 0,
                    'spacingBottom' => 0,
                    'spacingLeft' => 0,
                    'spacingRight' => 0,

                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie'
                ],
                'title' => [
                    'text' => null
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' =>  'pointer',
                        'showInLegend' => true,
                        'dataLabels' => [
                            'enabled' => false,
                            /*'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            'style' => [
                                'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                                'fontSize' => '22px'
                            ]*/
                        ]
                    ]
                ],
                'series' => [[
                    'name' => 'Test',
                    'colorByPoint' => true,
                    'data' => $items,
                ]]

            ],
            //'async' => true,
        ];

        $imageData = Charts::getPie($pieData);

        return $imageData;
    }


    public function getPieCompetitors() {



        //$dayStart = date("Y-m-d");

        $api = new SynthesioApi();
        $data = $api->getMentionCounts($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);


        $items = [];

        $total = 0;



        // Считаем общее кол-во пунктов, чтобы правильно затем высчитать проценты
        foreach ($data as $name => $v) {
            if (!in_array($name, $this->competitors)) {
                continue;
            }
            $total += (float)$v;
        }


        foreach ($data as $name => $v) {
            if (!in_array($name, $this->competitors)) {
                continue;
            }

            if ($total > 0) {
                $percent = round($v*100/$total, 2);
            } else {
                $percent = 0;
            }



            $items[] = [
                'name' => "<b>{$name}</b>: {$percent}%",
                'y' => $percent,
            ];
        }





        //print_r($items); exit;

        $pieData = [
            'infile' => [
                'credits' => [
                    'enabled' => false
                ],
                'chart' => [
                    'spacingTop' => 0,
                    'spacingBottom' => 0,
                    'spacingLeft' => 0,
                    'spacingRight' => 0,
                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie'
                ],
                'title' => [
                    'text' => null
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' =>  'pointer',
                        'showInLegend' => true,

                        'dataLabels' => [
                            'enabled' => false,
                            //'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                            /*'style' => [
                                'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'",
                                'fontSize' => '22px'
                            ]*/
                        ]
                    ]
                ],
                'series' => [[
                    'name' => 'Test',
                    'colorByPoint' => true,
                    'data' => $items,
                ]]

            ],
            //'async' => true,
        ];

        $imageData = Charts::getPie($pieData);

        return $imageData;
    }

    public function getSentimentPerTopicPR() {

        // Получаем топики
        $MyDatabase = new MyDatabase();
        $topics = $MyDatabase->topics();


        $reportData = new ReportData();


        $categories = [];
        $positiveData = [];
        $neutralData = [];
        $negativeData = [];

        foreach ($topics as $k => $topic) {
            // Получаем по каждому продукту кол-во позитивных, нейтральных, отричательных отзывов



            $positive_statements = $reportData->where('topic', (string)$topic->_id)->where('sentiment', '599c90f74e801f0006148023')->where('reportSchedule_id', ['$in' => $this->schedule_ids])->count();
            $positiveData[] = $positive_statements;

            $neutral_statements = $reportData->where('topic', (string)$topic->_id)->where('sentiment', '599c90fe4e801f0006148024')->where('reportSchedule_id', ['$in' => $this->schedule_ids])->count();
            $neutralData[] = $neutral_statements;

            $negative_statements = $reportData->where('topic', (string)$topic->_id)->where('sentiment', '599c91034e801f0006148025')->where('reportSchedule_id', ['$in' => $this->schedule_ids])->count();
            $negativeData[] = $negative_statements;

            $categories[] = $topic->name;


        }


        $data = [
            'infile' => [
                'chart' => [
                    'type' => 'bar'
                ],
                'title' => [
                    'text' => null,
                ],
                'xAxis' => [
                    'categories' => $categories
                ],


                'legend' => [
                    'reversed' => true,
                ],

                'plotOptions' => [
                    'series' => [
                        'stacking' => 'normal'
                    ],
                ],
                'series' => [
                    // Здесь в обратном порядке вывода должно быть

                    [
                        'name' => 'Neutral',
                        'data' => $neutralData
                    ],
                    [
                        'name' => 'Negative',
                        'data' => $negativeData
                    ],
                    [
                        'name' => 'Positive',
                        'data' => $positiveData
                    ],

                ]

            ],
            //'async' => true,
        ];


        $imageData = Charts::getStackedBar($data);

        return $imageData;


    }

    public function getSentimentPerTopicSocial() {

        $allowedTopics = [
            'IDOL 5',
            'IDOL 5S',
            'Alcatel A7',
            'Alcatel A7 XL',
            'Alcatel Mobile',
            'A7',
            'A7 XL',
            'Palm',
        ];

        //$dayStart = date("Y-m-d");

        //echo 'contributorGroup[\'countryCode\']: '.$this->contributorGroup['countryCode'];exit;
        $api = new SynthesioApi();
        $this->contributorGroup['countryCode'] = null;
        $sentimentArray = $api->getSentimentAnalysys($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);



        // Почему-то ключ 0 возваращется, странно..
        unset($sentimentArray[0]);

        //print_r($sentimentArray);exit;


        $categories = [];
        $positiveData = [];
        $neutralData = [];
        $negativeData = [];
        $unassignedData = [];



        foreach ($sentimentArray as $name => $sentiment) {

            if (!in_array($name, $allowedTopics)) {
                continue;
            }

            $categories[] = $name;
            $positiveData[] = $sentiment['positive'] ?? 0;
            $neutralData[] = $sentiment['neutral'] ?? 0;
            $negativeData[] = $sentiment['negative'] ?? 0;
            $unassignedData[] = $sentiment['unassigned'] ?? 0;
        }




        $data = [
            'infile' => [
                'chart' => [
                    'type' => 'bar'
                ],
                'title' => [
                    'text' => null,
                ],
                'xAxis' => [
                    'categories' => $categories
                ],


                'legend' => [
                    'reversed' => true,
                ],

                'plotOptions' => [
                    'series' => [
                        'stacking' => 'normal'
                    ],
                ],
                'series' => [

                    // Здесь в обратном порядке вывода должно быть
                    [
                        'name' => 'Unassigned',
                        'data' => $unassignedData
                    ],
                    [
                        'name' => 'Negative',
                        'data' => $negativeData
                    ],
                    [
                        'name' => 'Neutral',
                        'data' => $neutralData
                    ],
                    [
                        'name' => 'Positive',
                        'data' => $positiveData
                    ],

                ]

            ],
            //'async' => true,
        ];


        $imageData = Charts::getStackedBar($data);

        return $imageData;

    }

    public function getSocialCountPosts() {


        //$dayStart = date("Y-m-d");

        $api = new SynthesioApi();
        $data = $api->getMentionCounts($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);


        $items = [];

        $total = 0;

        // Считаем общее кол-во пунктов, чтобы правильно затем высчитать проценты
        foreach ($data as $name => $v) {
            if (!in_array($name, $this->allowedTopics)) {
                continue;
            }
            $total += (float)$v;
        }


        return (string)$total;
    }

    public function getSocialReach() {
        return '68,54 MN';
        //$dayStart = date("Y-m-d");
        $api = new SynthesioApi();
        $reach = $api->getSentimentReach($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);


        //$audience_reach = round($audience_reach / 1000000, 1).' MN';

        return 0;
        //print_r($reach);exit;




    }

    public function getSocialSentimentPositive() {
        //return 'test';
        //$dayStart = date("Y-m-d");

        //return [];
        $api = new SynthesioApi();
        $sentimentArray = $api->getSentimentAnalysys($this->from_date, $this->to_date, $this->contributorGroup['countryCode']);


        //print_r($sentimentArray);


        $sentiment_defined_total = 0;
        $sentiment_positive_count = 0;
        foreach ($sentimentArray as $name => $sentiment) {

            if (!in_array($name, $this->allowedTopics)) {
                continue;
            }

            $count = intval($sentiment['positive']) + intval($sentiment['neutral']) + intval($sentiment['negative']);
            $sentiment_defined_total += $count;
            $sentiment_positive_count += intval($sentiment['positive']);

        }


        // Выяисляем процент Positive statements
        if ($sentiment_defined_total == 0) {$sentiment_defined_total = 1;}
        $result['positive_statemts_percent'] = round($sentiment_positive_count*100/$sentiment_defined_total, 2).'%';



        return (string)$result['positive_statemts_percent'];


    }


    public function getBestQuotes() {


        $reportData = new ReportData();

        $data = $reportData->where('best_of_quote', ['$exists' => true, '$ne' => ''])
            //->where('logo', ['$exists' => true, '$ne' => ''])
            ->where('url', ['$exists' => true, '$ne' => ''])
            ->where('reportSchedule_id', ['$in' => $this->schedule_ids])
            ->orderByDesc('ave')->limit(150)->get();
        $dataArr = $data->toArray();

        // Тут можно отсортировать массив в случайном порядке
        shuffle($dataArr);


        //print_r($data);


        $selectedCountries = [];

        $selected = [];
        foreach ($dataArr as $v) {

            if (strlen('best_of_quote') < 5) {
                continue;
            }

            if (in_array($v['contributorGroupId'], $selectedCountries)) {
                continue;
            }


            $country = \App\Models\Group::where('_id', new \MongoDB\BSON\ObjectID($v['contributorGroupId']))->first();

            //$country = \App\Models\Group::where('_id', new \MongoDB\BSON\ObjectID($v['contributorGroupId']))->first();



            $selected[] = [
                '_id'                       => $v['_id'],
                'best_of_quote'             => $v['best_of_quote'],
                'url'                       => $v['url'],
                'logo'                      => $v['logo'] ?? '',
                'ave'                       => $v['ave'] ?? 0,
                'contributorGroupId'        => $v['contributorGroupId'],
                'country'                   => $country['name'],
                'media_name'                => $v['media_name'] ?? 'undefined',
            ];




            $selectedCountries[] = $v['contributorGroupId'];



            if (count($selected) == 9) {
                break;
            }


        }

        //print_r($selected);exit;



        //print_r($selected); exit;
        return $selected;


    }

}
