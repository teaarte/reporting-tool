<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\ScreenshotSaved;

class Screenshot extends \Moloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url'
    ];

    private $rules = [
        'name' => 'required|max:255',
    ];


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        //'saved ' => ScreenshotSaved::class,
        //'saved ' => 'App\Events\ScreenshotSaved',
        'updated ' => 'App\Events\ScreenshotSaved',
    ];




    protected function ScreenshotSaved() {

    }


}
