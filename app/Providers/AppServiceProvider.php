<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

use App\Models\Screenshot;
use App\Observers\ScreenshotObserver;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Screenshot::observe(UserObserver::class);
        Screenshot::observe('App\Observers\ScreenshotObserver;');

        if($this->app->environment() === 'production'){
            $this->app['request']->server->set('HTTPS', true);
            \URL::forceScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
