<?php

namespace App\Libs;

use App\Models\Synthesio as SynthesioModel;

class SynthesioParser
{
    private $api = null;

    public function __construct(SynthesioApi $api)
    {
        $this->api = $api;
    }
    
    public function parsePeriod($dateStart = null, $dateStop = null)
    {   
        if(!$dateStart) {
            $dateStart = date('Y-m-d', time() - (86400)) . ' 00:00:00';
        }

        if(!$dateStop) {
            $dateStop = date('Y-m-d H:i:s');
        }
        
        $dateStart = strtotime($dateStart);
        $dateStop = strtotime($dateStop);
        
        $count = 0;
        
        $reportIds = $this->api->getReportList();

        for($time = $dateStart; $time <= $dateStop; $time = $time+3600) {
            $hour = (int) date('H', $time);
            $dateStr = date('ymdH', $time);
            
            if(!SynthesioModel::where('date_str', '=', $dateStr)->count()) {
                
                echo "Parse " . date('d.m.y H:i:s', $time) . " ($dateStr)...\n";
                
                foreach($reportIds as $rId) {
                    foreach($this->api->getTags() as $tag) {
                        $reports = $this->api->getHourReportData($time, $rId, $tag);

                        $report = $reports['analytics'];

                        if($report && $report->data) {
                            foreach($report->data as $date => $data) {
                                if($data->data) {
                                    foreach($data->data as $reaction => $value) {
                                        $row = new SynthesioModel;
                                        
                                        $row->source_name = 'all';
                                        $row->reaction = $reaction;
                                        $row->count_value = (int)$value->value;
                                        $row->date = date('Y-m-d H:i:s', $time);
                                        $row->hour = $hour;
                                        //echo "$dateStr\n";
                                        $row->date_str = $dateStr;
                                        $row->tag = $tag;
                                        if($row->save()) {
                                            $count++;
                                        }
                                    }
                                }
                            }
                        }

                        $report = $reports['reach'];
                        
                        if($report && $report->data) {
                            foreach($report->data as $date => $data) {
                                if($data->data) {
                                    foreach($data->data as $reaction => $dataReaction) {
                                        foreach($dataReaction as $source => $rank) {
                                            $row = new SynthesioModel;
                                            
                                            $row->source_name = $source;
                                            $row->reaction = $reaction;
                                            $row->reach_value = (int) $rank;
                                            $row->date = date('Y-m-d H:i:s', $time);
                                            $row->hour = (int) $hour;
                                            //echo "$dateStr\n";
                                            
                                            $row->date_str = $dateStr;
                                            $row->tag = $tag;
                                            if($row->save()) {
                                                $count++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
            

        }
        
        return $count;
    }
}
