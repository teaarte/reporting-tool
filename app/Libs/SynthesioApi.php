<?php

namespace App\Libs;

use App\Models\Synthesio as SynthesioModel;
use App\Models\SynthesioTag;
use App\Models\SynthesioToken;
use \MongoDB\BSON\UTCDateTime;

class SynthesioApi
{
    protected $token = null;
    protected $tags = [];
    protected $reportId = null;
    protected $requestCache = []; //date => data
    
    public function __construct($reportId = null, $tags = null)
    {
        $this->token = $this->getToken();
        
        if($reportId) {
            $this->reportId = $reportId;
        } else {
            $this->reportId = $this->getFirstReportId();
        }
        
        if($tags) {
            $this->tags = $tags;
        } else {
            $this->setTags();
        }
    }
    
    /**
     * Возвращает количество упоминаний для всех тегов
    */
     
    public function getMentionCounts($dateStart = null, $dateStop = null, $country = null)
    {
        $dirtyTagsData = $this->getDirtyData('_analytics', $dateStart, $dateStop, $country);
        
        $tags = $this->getTags();
        
        $clearTagsData = [];
        if(!empty($dirtyTagsData->data)) {
            foreach($dirtyTagsData->data as $tag => $data) {
                if(isset($data->value)) {
                    $clearTagsData[array_search($tag, $tags)] = $data->value; 
                } else {
                    $clearTagsData[array_search($tag, $tags)] = 0;
                }
            }
        }
        
        return $clearTagsData;
    }
    
    /**
     * Возвращает цифры для построения Sentiment analysys by topic
     */
     
    public function getSentimentAnalysys($dateStart = null, $dateStop = null, $country = null)
    {
        $dirtyTagsData = $this->getDirtyData('_analytics', $dateStart, $dateStop, $country);
        
        $tags = $this->getTags();
        
        $clearTagsData = [];
        if(!empty($dirtyTagsData->data)) {
            foreach($dirtyTagsData->data as $tag => $data) {
                if(isset($data->data) && !empty($data->data)) {
                    foreach($data->data as $reaction => $valueObj) {
                        $clearTagsData[array_search($tag, $tags)][$reaction] = $valueObj->value;
                    }
                }
            }
        }
        
        return $clearTagsData;
    }
    
    /**
     * Возвращает цифры для построения Sentiment analysys by topic
     */
     
    public function getSentimentReach($dateStart = null, $dateStop = null, $country = null)
    {
        $dirtyTagsData = $this->getDirtyData('_reach', $dateStart, $dateStop, $country);
        
        $tags = $this->getTags();
        
        $clearTagsData = [];
        if(!empty($dirtyTagsData->data)) {
            foreach($dirtyTagsData->data as $tag => $data) {
                if(isset($data->data) && !empty($data->data)) {
                    foreach($data->data as $reaction => $platforms) {
                        $value = 0;
                        foreach($platforms as $platformName => $platformValue) {
                            $value += $platformValue;
                        }
                        $clearTagsData[array_search($tag, $tags)][$reaction] = $value;
                    }
                }
            }
        }
        
        return $clearTagsData;
    }

    /*
        Возвращает неочищенные данные для всех типов отчетов
    */
    
    public function getDirtyData($type, $dateStart = null, $dateStop = null, $country = null)
    {
        if(!$dateStart) {
            $dateStart = date('Y-m-d H') . ':00:00';
        }
        
        if(!$dateStop) {
            $dateStop = date('Y-m-d H', strtotime($dateStart)+86400) . ':00:00';
        }
        
        $hash = md5($type . $dateStart . $dateStop . serialize($country));
        
        if(isset($this->requestCache[$hash])) {
            return $this->requestCache[$hash];
        }
        
        $dirtyTagsData = [];

        $query = [
            'aggs' => [
                [
                    "field" => "tags",
                ],
                [
                    "field" => "sentiment"
                ],
            ],
            'filters' => [
                'period' => [
                    "begin" => date('Y-m-d', strtotime($dateStart)) . "T" . date('H:i:s', strtotime($dateStart)) . "Z",
                    "end" => date('Y-m-d', strtotime($dateStop)) . "T" . date('H:i:s', strtotime($dateStop)) . "Z"
                ]
            ]
        ];
        
        if (!empty($country)) {
            if(!is_array($country)) {
                $query['filters']['countries'] = [$country];
            } else {
                $query['filters']['countries'] = $country;
            }
        }
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://rest.synthesio.com/mention/v2/reports/{$this->reportId}/{$type}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Authorization: Bearer {$this->token}";
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $dirtyTagsData = json_decode(curl_exec($ch));
        
        curl_close($ch);
        
        $this->requestCache[$hash] = $dirtyTagsData;
        
        return $dirtyTagsData;
    }
    
    public function getFirstReportId()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://rest.synthesio.com/workspace/v1/dashboards?from=0&size=2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = array();
        $headers[] = "Authorization: Bearer {$this->token}";
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        
        curl_close($ch);
        
        $result = json_decode($result);

        $data = $result->data[0]->report_list_id;
        
        return current($data);
    }
    
    public function getTags()
    {
        return $this->tags;
    }
    
    public function setTags($date = null)
    {
        if(!$date) {
            $date = date('Y-m-d');
        }
        
        $tagsModel = SynthesioTag::where('date_str', date('Ymd', strtotime($date)));
        
        if($tagsModel->count() != 0) {
            $tags = $tagsModel->get();
            $return = [];
            foreach($tags as $tag) {
                $this->tags[$tag->name] = $tag->tag;
            }
            
            return $this->tags;
        } else {
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://rest.synthesio.com/api/v2/report/{$this->reportId}/topics");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $headers = array();
            $headers[] = "Authorization: Bearer {$this->token}";
            $headers[] = "Content-Type: application/json";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            
            curl_close($ch);
            
            $result = json_decode($result);
            
            $data = [];
            
            foreach($result->{$this->reportId} as $tag1) {
                foreach($tag1->children as $tag) {
                    $data[$tag->name] = $tag->tag;
                }
            }
            
            $this->tags = $data;
            
            foreach($this->tags as $name => $tag) {
                $tagsModel = new SynthesioTag;
                $tagsModel->name = $name;
                $tagsModel->tag = $tag;
                $tagsModel->date_str = date('Ymd');
                $tagsModel->save();
            }
            
            return $this->tags;
        }
    }
    
    public function getToken()
    {
        //return true;
        $token = SynthesioToken::where('token_date_str', date('YmdH'))->first();
        $token = null;

        if(isset($token->token) && !empty($token->token)) {
            return $token->token;
        }
        
        $data = [
            'grant_type' => 'password',
            'username' => env('SYNTHESIO_USERNAME'),
            'password' => env('SYNTHESIO_PASSWORD'),
            'client_id' => env('SYNTHESIO_CLIENT_ID'),
            'client_secret' => env('SYNTHESIO_CLIENT_SECRET'),
            'scope' => 'read',
        ];
        
        $dataString = http_build_query($data, '', '&'); 

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://rest.synthesio.com/security/v1/oauth/token");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        $result = json_decode($result);

        //print_r($result);exit;

        $token = $result->access_token;
        
        if(!$token) {
            abort("Synthesio token do not taken");
        }
        
        curl_close ($ch);
        
        if($oldToken = SynthesioToken::first()) {
            $oldToken->delete();
        }
        
        $tokenModel = new SynthesioToken;
        $tokenModel->token_date_str = date('YmdH');
        $tokenModel->token = $token;
        $tokenModel->save();
        
        return $token;
    }
}
