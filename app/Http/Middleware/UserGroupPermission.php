<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserGroupPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  int  $group
     * @return mixed
     */
    public function handle($request, Closure $next, $group = null)
    {
        if(Auth::user()->inRole('admin'))
            return $next($request);

        if(!Auth::user()->inGroup($group))
            return abort(404);

        return $next($request);
    }
}
