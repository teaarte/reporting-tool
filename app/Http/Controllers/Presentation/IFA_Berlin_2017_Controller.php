<?php

namespace App\Http\Controllers\Presentation;

use App\Http\Controllers\Controller;
use App\Models\Presentation\IFA_Berlin_2017_Model;
use App\Libs\SynthesioApi;



define('CREDENTIALS_PATH', resource_path('slides/slides.googleapis.com-php-quickstart.json'));
define('CLIENT_SECRET_PATH', resource_path('slides/client_secret.json'));
// если изменялись права, надо удалить файл <домашняя директория юзера>/.credentials/slides.googleapis.com-php-quickstart.json
define('SCOPES', implode(' ', array(
    \Google_Service_Slides::PRESENTATIONS,
    \Google_Service_Drive::DRIVE) // тут добавлять права для апи
));


class IFA_Berlin_2017_Controller extends Controller
{
    protected
        $model,
        // id презентации-шаблона в Google Slides
        $presentationTplId = '1zMIZlvYI5pMGSrvdGs5DCVAKP9M0js7hFWGj3xgPbwU',
        $presentationCopyId,
        $accessToken,
        $serviceSlides,
        $serviceDrive,
        $serviceRequests = [],

        $contributorGroup,
        $schedule_ids,
        $from_ts,
        $from_date,
        $to_ts,
        $to_date;


    public function make($copyName = null, $model = null){
        // имя создаваемой копии
        if ($copyName == null){
            $copyName = 'New Presentation';
        }


        if ($model == null){
            $model =
            $this->model = new IFA_Berlin_2017_Model();
        } else {
            $this->model = $model;
        }

        $this->model->setDate($this->from_ts, $this->to_ts);
        $this->model->setContributorGroup($this->contributorGroup);
        $this->model->setSchedules($this->schedule_ids);




        $data = $model->getData();
        $testData = 'text'; // тестовые данные

        $credentialsPath = $this->expandHomeDirectory(CREDENTIALS_PATH);
        if (file_exists($credentialsPath)) {
            $this->accessToken = json_decode(file_get_contents($credentialsPath), true);
        }

        $slidesIDs = [
            'slide1' => 'p4',
            'slide2' => 'p8',
            'slide3' => 'p10',
            'slide4' => 'p12',
            'slide5' => 'p14',
            'slide6' => 'p16',
            'slide7' => 'p18',
            'slide8' => 'p20',
            'slide9' => 'p23',
        ];

        // пример именования ключа "s2_Articles_published" второй слайд блок под иконкой газеты где написано Articles published далее названия по шаблону <номер слайда>_<ключевое слово>
        $elementIDs = [
            's2_Articles_published' => 'p8_i162',
            's2_Audience_Reach' => 'p8_i163',
            's2_AVE_generated' => 'p8_i165',
            's2_Positive_statements' => 'p8_i167',
            's2_SM_posts' => 'p8_i169',
            's2_SM_Audience_Reach' => 'p8_i171',
            's2_SM_Sentiment_overview' => 'p8_i173',
            's5_Best_Of_Quote_1' => 'p14_i215',
            's5_Link_1' => 'p14_i216',
            's5_Best_Of_Quote_2' => 'p14_i218',
            's5_Link_2' => 'p14_i219',
            's5_Best_Of_Quote_3' => 'p14_i221',
            's5_Link_3' => 'p14_i222',
        ];

        $client = $this->getClient();

        $this->serviceSlides = new \Google_Service_Slides($client);
        $this->serviceDrive = new \Google_Service_Drive($client);

        $this->presentationCopyId = $this->createCopy($copyName);

        $slides = $this->getPresentationCopy(); // изменить на копи

        $this->insertDataInNewPresentation($slides, $slidesIDs, $elementIDs, $testData, $data);

        //$downloadLink = "https://docs.google.com/presentation/d/".$this->presentationCopyId."/export/pptx";
        $downloadLink = "https://docs.google.com/presentation/d/".$this->presentationCopyId;

        return $downloadLink;
    }


    public function setSchedules($schedule_ids = []) {
        $this->schedule_ids = $schedule_ids;
    }

    public function setDate($from_ts, $to_ts) {
        $this->from_ts = $from_ts;
        $this->from_date = date('Y-m-d H:i:s', $from_ts);

        $this->to_ts = $to_ts;
        $this->to_date = date('Y-m-d H:i:s', $to_ts);
    }

    public function setContributorGroup($contributorGroup) {
        $this->contributorGroup = $contributorGroup;
    }


    protected function getClient() {
        $client = new \Google_Client();
        $client->setApplicationName('Reporting tool');
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');

        $credentialsPath = $this->expandHomeDirectory(CREDENTIALS_PATH);
        if (file_exists($credentialsPath)) {
            $this->accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            $this->accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            if(file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($this->accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($this->accessToken);

        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    public function createCopy($copyName) {
        $copy = new \Google_Service_Drive_DriveFile(array(
            'name' => $copyName
        ));
        $driveResponse = $this->serviceDrive->files->copy($this->presentationTplId, $copy);
        $presentationCopyId = $driveResponse->id;

        return $presentationCopyId;
    }

    public function expandHomeDirectory($path) {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
            $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }

    public function getPresentationCopy() {
        $presentation = $this->serviceSlides->presentations->get($this->presentationCopyId);
        $slides = $presentation->getSlides();
        return $slides;
    }

    public function insertDataInNewPresentation($slides, $slidesIDs, $elementIDs, $testData, $data) {

        /* Конструкция для получения списка индексов элементов слайда
         * if ($slide->getObjectId() == $slidesIDs['slide2']) {
         *       foreach ($slide->getPageElements() as $element){
         *          $elementID = $element->getObjectId();
         *           echo $elementID.'<br/>';
         *       }
         *   }
         */
        $whiteColor = 1.0;
        $blackColor = 0.0;
        $pt72 = 72;
        $pt50 = 50;



        $socialPostsCount = $this->model->getSocialCountPosts();
        $socialReachCount = $this->model->getSocialReach();
        $sentimentSocialPositive = $this->model->getSocialSentimentPositive();

        $this->editText2('{{a-pub}}', $data['articles_published']);
        $this->editText2('{{a-reach}}', $data['audience_reach']);
        $this->editText2('{{ave}}', $data['ave_generated']);
        $this->editText2('{{p-stat}}', $data['positive_statements']);
        $this->editText2('{{sm-p}}', $socialPostsCount);
        $this->editText2('{{sm-reach}}', $socialReachCount);
        $this->editText2('{{sm-sent}}', $sentimentSocialPositive);

        /*
        foreach ($slides as $i => $slide) {
            if ($slide->getObjectId() == $slidesIDs['slide2']) {
                foreach ($slide->getPageElements() as $element){
                    $elementID = $element->getObjectId();


                    if ($elementID = $elementIDs['s2_Articles_published']) $this->editText($elementID, strval($data['articles_published']), $whiteColor, $pt72);
                    if ($elementID = $elementIDs['s2_Audience_Reach']) $this->editText($elementID,  strval($data['audience_reach']), $whiteColor, $pt72);
                    if ($elementID = $elementIDs['s2_AVE_generated']) $this->editText($elementID,  strval($data['ave_generated']), $whiteColor, $pt72);
                    if ($elementID = $elementIDs['s2_Positive_statements']) $this->editText($elementID, strval($data['positive_statements']), $whiteColor, $pt72);
                    if ($elementID = $elementIDs['s2_SM_posts']) $this->editText($elementID, $socialPostsCount, $whiteColor, $pt72);
                    if ($elementID = $elementIDs['s2_SM_Audience_Reach']) $this->editText($elementID, $socialReachCount, $whiteColor, $pt72);
                    if ($elementID = $elementIDs['s2_SM_Sentiment_overview']) $this->editText($elementID, $sentimentSocialPositive, $whiteColor, $pt72);

                    break;
                }
            }
        }
*/




        /*
        foreach ($slides as $i => $slide) {
            if ($slide->getObjectId() == $slidesIDs['slide6']) {
                $elId = $slide->getObjectId();
                $imageName = 'slide6.png';

                //$imageData = \App\Models\Charts::getStackedBar([]);
                $imageData_TopicsPR = $this->model->getPieTopicsPR();
                $imageData_TopicsSocial = $this->model->getPieTopicsSocial();
                $imageData_TopicsPRSocial = $this->model->getPieTopicsPRSocial();
                $imageData_PerCountryPR = $this->model->getPiePerCountryPR();


                $this->addImageTpl("{{img-topics-pr}}", $imageData_TopicsPR);
                $this->addImageTpl("{{img-topics-pr-social}}", $imageData_TopicsPRSocial);
                $this->addImageTpl("{{img-per-country-pr}}", $imageData_PerCountryPR);


                //$this->addImage($elId, 1500000, 3500000, 'slid6_image1', 6500000, $imageData_TopicsPR);
                //$this->addImage($elId, 10000000,3500000, 'slid6_image2', 6500000, $imageData_TopicsPRSocial);
                //$this->addImage($elId, 17000000,3500000, 'slid6_image3', 6500000, $imageData_PerCountryPR);
                break;
            }
        }
*/

        $imageData_TopicsPR = $this->model->getPieTopicsPR();
        $imageData_TopicsSocial = $this->model->getPieTopicsSocial();
        $imageData_TopicsPRSocial = $this->model->getPieTopicsPRSocial();
        $imageData_PerCountryPR = $this->model->getPiePerCountryPR();


        $this->addImage("{{img-topics-pr}}", $imageData_TopicsPR);
        $this->addImage("{{img-topics-pr-social}}", $imageData_TopicsPRSocial);
        $this->addImage("{{img-per-country-pr}}", $imageData_PerCountryPR);



        $imageData_SentimentPR = $this->model->getSentimentPerTopicPR();
        $imageData_SentimentSocial = $this->model->getSentimentPerTopicSocial();


        $this->addImage("{{img-sentiment-pr}}", $imageData_SentimentPR);
        $this->addImage("{{img-sentiment-social}}", $imageData_SentimentSocial);

/*
        foreach ($slides as $i => $slide) {
            if ($slide->getObjectId() == $slidesIDs['slide7']) {
                $elId = $slide->getObjectId();

                $imageData_SentimentPR = $this->model->getSentimentPerTopicPR();
                $imageData_SentimentSocial = $this->model->getSentimentPerTopicSocial();


                $this->addImageTpl("{{img-sentiment-pr}}", $imageData_SentimentPR);
                $this->addImageTpl("{{img-sentiment-social}}", $imageData_SentimentSocial);

                //$this->addImage($elId, 1700000,3000000, 'slid7_image1', 8000000, $imageData_SentimentPR);
                //$this->addImage($elId, 11000000,3000000, 'slide7_image2', 8000000, $imageData_Sentimentocial);
                break;
            }
        }*/


        $imageData_Competirors = $this->model->getPieCompetitors();
        $this->addImage("{{img-competirors}}", $imageData_Competirors);


        /*
        foreach ($slides as $i => $slide) {
            if ($slide->getObjectId() == $slidesIDs['slide8']) {

                $imageData_Competirors = $this->model->getPieCompetitors();

                $elId = $slide->getObjectId();
                //$this->addImage($elId, 1800000,2000000, 'slid8_image1', 10000000, $imageData_Competirors);

                $this->addImageTpl("{{img-competirors}}", $imageData_Competirors);
                break;
            }
        }
*/


        $bestQuotes = $this->model->getBestQuotes();

        for ($i = 0; $i < count($bestQuotes); $i++) {
            if (!empty($bestQuotes[$i]['logo'])) {
                $this->addImageFromUrl("{{bestOfQuoteLogo{$i}}}", $bestQuotes[$i]['logo']);
            }
                $this->editText2("{{bestOfQuoteText{$i}}}", $bestQuotes[$i]['best_of_quote']);
                $this->editText2("{{bestOfQuoteLink{$i}}}", $bestQuotes[$i]['url']);
                $this->editText2("{{country{$i}}}", $bestQuotes[$i]['country']);


        }

        /*
        foreach ($slides as $i => $slide) {
            if ($slide->getObjectId() == $slidesIDs['slide5']) {

                $imageName = 'logo.png';

                $elId = $slide->getObjectId();

                foreach ($slide->getPageElements() as $element){
                    $elementID = $element->getObjectId();

                    if (!empty($bestQuotes[0])) {
                        if ($elementID = $elementIDs['s5_Best_Of_Quote_1']) $this->editText($elementID, $bestQuotes[0]['best_of_quote'], $blackColor, 30);
                        if ($elementID = $elementIDs['s5_Link_1']) $this->editText($elementID, 'Link', $blackColor, 30, $bestQuotes[0]['url']);
                    }

                    if (!empty($bestQuotes[1])) {
                        if ($elementID = $elementIDs['s5_Best_Of_Quote_2']) $this->editText($elementID, $bestQuotes[1]['best_of_quote'], $blackColor, 30);
                        if ($elementID = $elementIDs['s5_Link_2']) $this->editText($elementID, 'Link', $blackColor, 30, $bestQuotes[1]['url']);
                    }

                    if (!empty($bestQuotes[2])) {
                        if ($elementID = $elementIDs['s5_Best_Of_Quote_3']) $this->editText($elementID, $bestQuotes[2]['best_of_quote'], $blackColor, 30);
                        if ($elementID = $elementIDs['s5_Link_3']) $this->editText($elementID, 'Link', $blackColor, 30, $bestQuotes[2]['url']);
                    }

                    break;
                }
            break;
            }
        }
        */



        $this->doServiceRequest();
    }



    public function editText($elementID, $text, $textColor, $textSize, $link = null) {



        $this->serviceRequests[] = new \Google_Service_Slides_Request(array(
            'deleteText' => array(
                'objectId' => $elementID,
                'textRange' => array(
                    'type' => 'ALL'
                )
            )
        ));

        $this->serviceRequests[] = new \Google_Service_Slides_Request(array(
            'insertText' => array(
                'objectId' => $elementID,
                'insertionIndex' => 0,
                'text' => $text
            ),
        ));

        // формирования запроса изменения стиля текста
        $this->serviceRequests[] = new \Google_Service_Slides_Request(array(
            'updateTextStyle' => array(
                'objectId' => $elementID,
                "textRange" => array(
                    "type" => "ALL"
                ),
                'style' => array(
                    'bold' => true
                ),
                'fields' => 'bold'
            )
        ));

        if (!empty($link)) {
            $this->serviceRequests[] = new \Google_Service_Slides_Request(array(
                'updateTextStyle' => array(
                    'objectId' => $elementID,
                    "textRange" => array(
                        "type" => "ALL"
                    ),
                    'style' => array(
                        'link' => array(
                            'url' => $link  //сюда ссылку надо передавать
                    )
                    ),
                    'fields' => 'link'
                )
            ));
        }


        $this->serviceRequests[] = new \Google_Service_Slides_Request(array(
            'updateTextStyle' => array(
                'objectId' => $elementID,
                "textRange" => array(
                    "type" => "ALL"
                ),
                'style' => array(
                    'fontFamily' => 'Arial',
                    'fontSize' => array(
                        'magnitude' => $textSize,
                        'unit' => 'PT'
                    ),
                    'foregroundColor' => array(
                        'opaqueColor' => array(
                            'rgbColor' => array(
                                'blue' => $textColor,
                                'green' => $textColor,
                                'red' => $textColor
                            )
                        )
                    )
                ),
                'fields' => 'foregroundColor,fontFamily,fontSize'
            )
        ));

        /*
        // запрос для изменеия стиля в элементе
        $batchUpdateRequest = new \Google_Service_Slides_BatchUpdatePresentationRequest(array(
            'requests' => $requests
        ));
        $response = $this->serviceSlides->presentations->batchUpdate($this->presentationCopyId, $batchUpdateRequest);
*/
    }

    protected function saveImageAndGetUrl($imageData) {

        $file = new \Google_Service_Drive_DriveFile(array(
            'name' => 'My Image File',
            'mimeType' => 'image/png'
        ));
        $params = array(
            //'data' => file_get_contents($imageFilePath),
            'data' => $imageData,
            'uploadType' => 'media',
        );


        $upload = $this->serviceDrive->files->create($file, $params);
        $fileId = $upload->id;

        $token = $this->accessToken['access_token'];
        $endPoint = 'https://www.googleapis.com/drive/v3/files';
        $imageUrl = sprintf('%s/%s?alt=media&access_token=%s', $endPoint, $fileId, $token);

        return $imageUrl;

    }

/*
    public function addImage($elId, $positionX, $positionY, $imageID, $magnitude, $imageData) {


        //$imageFilePath = resource_path('slides/'.$imageName);
        $file = new \Google_Service_Drive_DriveFile(array(
            'name' => 'My Image File',
            'mimeType' => 'image/png'
        ));
        $params = array(
            //'data' => file_get_contents($imageFilePath),
            'data' => $imageData,
            'uploadType' => 'media',
        );


        $upload = $this->serviceDrive->files->create($file, $params);
        $fileId = $upload->id;

        $token = $this->accessToken['access_token'];
        $endPoint = 'https://www.googleapis.com/drive/v3/files';
        $imageUrl = sprintf('%s/%s?alt=media&access_token=%s', $endPoint, $fileId, $token);

        $imageId = $imageID;
        $emu4M = array('magnitude' => $magnitude, 'unit' => 'EMU');
        $requests = array();
        $requests[] = new \Google_Service_Slides_Request(array(
            'createImage' => array (
                'objectId' => $imageId,
                'url' => $imageUrl,
                'elementProperties' => array(
                    'pageObjectId' => $elId,
                    'size' => array(
                        'height' => $emu4M,
                        'width' => $emu4M
                    ),
                    'transform' => array(
                        'scaleX' => 1,
                        'scaleY' => 1,
                        'translateX' => $positionX,
                        'translateY' => $positionY,
                        'unit' => 'EMU'
                    )
                )
            )
        ));

        $batchUpdateRequest = new \Google_Service_Slides_BatchUpdatePresentationRequest(array(
            'requests' => $requests
        ));
        $response = $this->serviceSlides->presentations->batchUpdate($this->presentationCopyId, $batchUpdateRequest);
        $createImageResponse = $response->getReplies()[0]->getCreateImage();

        $this->serviceDrive->files->delete($fileId);
    }
*/

    public function editText2($code, $text) {

        $this->serviceRequests[] = new \Google_Service_Slides_Request(array(
            'replaceAllText' => array(
                'containsText' => array(
                    'text' => $code,
                    'matchCase' => true
                ),
                'replaceText' => (string)$text
            )
        ));

    }

    public function addImage($code, $imgData) {

        $imgUrl = $this->saveImageAndGetUrl($imgData);

        return $this->addImageFromUrl($code, $imgUrl);
    }

    public function addImageFromUrl($code, $imgUrl) {


        // Create the image merge (replaceAllShapesWithImage) requests.

        $this->serviceRequests[] = new \Google_Service_Slides_Request(array(
            'replaceAllShapesWithImage' => array(
                'imageUrl' => $imgUrl,
                'replaceMethod' => 'CENTER_INSIDE',
                'containsText' => array(
                    'text' => $code,
                    'matchCase' => true
                )
            )
        ));

    }

    protected function doServiceRequest() {

        //print_r($this->serviceRequests);exit;

        $batchUpdateRequest = new \Google_Service_Slides_BatchUpdatePresentationRequest(array(
            'requests' => $this->serviceRequests
        ));
        $response = $this->serviceSlides->presentations->batchUpdate($this->presentationCopyId, $batchUpdateRequest);

    }




}

























