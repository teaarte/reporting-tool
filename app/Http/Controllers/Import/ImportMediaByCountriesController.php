<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Models\Report\DatabaseItem;
use Maatwebsite\Excel\Facades\Excel;

class ImportMediaByCountriesController extends Controller
{

    public function importMediaByCountries() {

        exit;
        $countries = [
            'DE' => '599a893c5dbf5f00062d0604',
            'UK' => '599a89455dbf5f00075dcd94',
            'RU' => '599a8ca25dbf5f00062d060b',

            'FR' => '599a8cad5dbf5f00075dcd9a',
            'PL' => '599a8ce25dbf5f00075dcd9b',
            'SP' => '599a8cec5dbf5f00062d060c',
            'IT' => '599a8cfe5dbf5f00075dcd9c',
            'NL' => '599a8d135dbf5f00062d060d',
        ];






        //$this->dataPreparation($countries['RU'], resource_path('import/media-ru2.csv'));
        //$this->dataPreparation($countries['DE'], resource_path('import/media-de2.csv'));
        //$this->dataPreparation($countries['UK'], resource_path('import/media-uk2.csv'));

        //$this->dataPreparation($countries['FR'], resource_path('import/media-fr2.csv'));
        //$this->dataPreparation($countries['SP'], resource_path('import/media-sp2.csv'));
        //$this->dataPreparation($countries['IT'], resource_path('import/media-it2.csv'));
        //$this->dataPreparation($countries['NL'], resource_path('import/media-nl2.csv'));

        //$this->dataPreparation($countries['PL'], resource_path('import/media-pl2.csv'));





    }

    public function dataPreparation($contributorGroupId, $filename) {




        //Excel::setDelimiter(';')->load($filename,function ($reader) use ($filename) {
        Excel::load($filename, function ($reader) use ($contributorGroupId, $filename) {

            $dataModel = new DatabaseItem();

            $list_id = '599c91284e801f0006148026';
            $results = $reader->all();
            $sector_type = [
                'hitech' => '599d2cf44e801f0006148036',
                'lifestyle' => '599d2cfe4e801f00076dd21f',
                'generalist' => '599d2d084e801f0006148037',
                'others' => '599d2da34e801f00076dd220'
                ];

            $media_types = [
                'digital' => '599c91424e801f00076dd215',
                'print' => '599c914b4e801f0006148028',
                'tv' => '599c914e4e801f0006148029',
                'radio' => '599c91534e801f000614802a',
            ];



            foreach ($results as $result){

                $m_types = [];

                if (trim($result['digital_artic.']) == 1) array_push($m_types, $media_types['digital']);
                if (trim($result['print_artic.']) == 1) array_push($m_types, $media_types['print']);

                $result['list_id'] = $list_id;
                $result['media_type'] = $m_types;

                $result['circulation'] = preg_replace("/[^0-9]/", '', $result['circulation']);
                $result['unique_visitor'] = preg_replace("/[^0-9]/", '', $result['unique_visitor']);

                $result['unique_visitor'] = strval(trim($result['unique_visitor']));
                $result['circulation'] = strval(trim($result['circulation']));

                if (ctype_upper($result['media_name'])) {
                    $result['media_name'] = mb_strtolower($result['media_name']);
                    $result['media_name'] = ucfirst(trim($result['media_name']));
                }


                $result['contributorGroupId'] = $contributorGroupId;


                switch (mb_strtolower($result['sector'])) {
                    case 'hi-tech':
                        $result['sector'] = [$sector_type['hitech']];
                        break;
                    case 'High-tech':
                        $result['sector'] = [$sector_type['hitech']];
                        break;
                    case 'hitech':
                        $result['sector'] = [$sector_type['hitech']];
                        break;
                    case 'lifestyle':
                        $result['sector'] = [$sector_type['lifestyle']];
                        break;
                    case 'generalist':
                        $result['sector'] = [$sector_type['generalist']];
                        break;
                    case 'others':
                        $result['sector'] = [$sector_type['others']];
                        break;
                    default:
                        break;
                }

                $filtered = $result->except(['digital_artic.','print_artic.']);

                $sector = $filtered->sector;
                $unique_visitor = $filtered->unique_visitor;
                $circulation = $filtered->circulation;
                $media_type = $filtered->media_type;
                $media_name = $filtered->media_name;
                $contributorGroupId = $filtered->contributorGroupId;

                $dataModel->generateNewExcelImportRow($list_id, $contributorGroupId, $media_name, $sector, $unique_visitor, $circulation, $media_type);

            }
        });
    }
}
