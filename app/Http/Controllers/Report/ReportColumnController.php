<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Report\ReportColumn;
use App\Models\Report\MyDatabase;
use Illuminate\Http\Request;

class ReportColumnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report_column.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new ReportColumn();
        $types = array_combine($model->types, $model->types);
        $data = MyDatabase::pluck('name', '_id');







        return view('report_column.create', compact(['types', 'data']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new ReportColumn();
        $data = array();


        foreach ($request->all() as $key => $value){
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }


        if ($model->validate($data))
            if($model->create($data))
                return redirect()->route('report.column.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ReportColumn::findOrFail($id);
        $types = array_combine($model->types, $model->types);
        $data = MyDatabase::pluck('name', '_id');

        return view('report_column.edit', compact(['model', 'types','data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = ReportColumn::findOrFail($id);
        $data = array();

        foreach ($request->all() as $key => $value){
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }
        if($data['type'] !== 'combo')
            $data['data'] = null;

        if ($model->validate($data))
            if($model->update($data))
                return redirect()->route('report.column.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = ReportColumn::findOrFail($id);

        if($model->delete())
            return redirect()->route('report.column.index');
    }

    /**
     * Get data for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $model = new ReportColumn();
        $data = $model->getList();

        return $model->datatables($data);
    }
}
