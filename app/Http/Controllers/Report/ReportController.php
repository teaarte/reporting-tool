<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Report\Report;
use App\Models\Report\ReportColumn;
use App\Models\Report\MyDatabase;
use App\Models\Report\ReportData;
use App\Models\Report\ReportTemplate;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ReportController extends Controller
{

    public function index()
    {
        $start_date = Carbon::parse(config('report.start_date'));
        //$reports = Report::all()->orderBy('id', 'desc');
        $reports = Report::orderBy('_id', 'desc')->get();


        return view('report.index',['start_date' => $start_date, 'reports' => $reports]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = ReportTemplate::pluck('name', '_id');
        return view('report.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Report();
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];


        }

        if ($model->validate($data)) {

            if($model->createRow($data))
                return redirect()->route('report.index');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Report::findOrFail($id);

        $model->delete();

        return redirect()->route('report.index');


        if($model->delete())
            return redirect()->route('report.index');
        /*
                if($model->users->isEmpty()){
                    if($model->delete())
                        return redirect()->route('project.index');
                }
                else{
                    return redirect()->route('project.index')->with('error', trans('project.group-has-users'));
                }

                */

    }


}
