<?php

namespace App\Http\Controllers\Report;

use App\Models\Report\ReportFinal;
use App\Models\Report\ReportSchedule;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ReportFinalController extends Controller
{


    public function create()
    {
        return view('report-final.create');
    }


    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //return view('user.profile', ['user' => Project::findOrFail($id)]);
    }



    public function index()
    {
        return view('report-final.index');
    }


    public function getList()
    {
        $model = new ReportFinal();
        $data = $model->getList();

        return $model->datatables($data);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new ReportFinal();
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->create($data))
                return redirect()->route('report-final.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ReportFinal::findOrFail($id);

        return view('report-final.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = ReportFinal::findOrFail($id);
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->update($data))
                return redirect()->route('report-final.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = ReportFinal::findOrFail($id);

        $model->delete();

        return redirect()->route('report-final.index');


        if($model->delete())
            return redirect()->route('report-final.index');
/*
        if($model->users->isEmpty()){
            if($model->delete())
                return redirect()->route('project.index');
        }
        else{
            return redirect()->route('project.index')->with('error', trans('project.group-has-users'));
        }

        */
    }


    public function addNew(Request $request) {


        //$api = new \App\Libs\SynthesioApi();
        //$data = $api->getMentionCounts('2017-08-22', '2017-08-30');
        //$data = $api->getSentimentAnalysys('2017-08-22', '2017-08-30');

        //print_r($data);
//exit;


        //echo "wweqew "; exit;

        // Получение данных от schedule_id
        if (empty($request->input('schedule_id'))) {
            $schedule_id = $request->input('schedule_ids');
                $schedule_id = $schedule_id[0];
        }

        $reportSchedule = ReportSchedule::findOrFail($schedule_id);

        //$report = $reportSchedule->report;
        //print_r($report);exit;

        //print_r($reportSchedule);

        //echo 'Date:'. $reportSchedule->date;
//        exit;

        $name = $reportSchedule->report->name."_".$reportSchedule->name;
        $name = str_replace(' ', '_', $name);


        $from_ts = null;
        $to_ts = null;

        if (!empty($request->input('date_from')) && !empty($request->input('date_to'))) {

            $date = new \DateTime($request->input('date_from'));
            $date->setTime(0,0,0);
            $from_ts = $date->getTimestamp();


            $date = new \DateTime($request->input('date_to'));
            $date->setTime(23,59,59);
            $to_ts = $date->getTimestamp();


        } else {
            $date = new \DateTime($reportSchedule->date);
            $date->setTime(0,0,0);
            $from_ts = $date->getTimestamp();

            $date->setTime(23,59,59);
            $to_ts = $date->getTimestamp();

        }













        //$DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $column->data)->get();



        $contributorGroupsArr = [
            [
                'id'  => 0,
                'name' => 'global',
                'countryCode' => null,
            ]
        ];


        $contributorGroups = \App\Models\Group::getList();
        foreach ($contributorGroups as $row) {
            $contributorGroupsArr[] = [
                'id' => $row->id,
                'name' => $row->id,
                'countryCode' => $row->countryCode,
            ];

        }


        $schedule_ids = [];
        if (!empty($request->input('schedule_ids'))) {

            foreach ($request->input('schedule_ids') as $schedule_id) {

                $schedule_ids[] = $schedule_id;
            }
        } else {
            $schedule_ids[] = $request->input('schedule_id');
        }






        foreach ($contributorGroupsArr as $contributorGroup) {

            // Проверять наличие по schedule_id, contributorGroupId, from_ts,_ to_ts

            $result = ReportFinal::where('schedule_id', (string)$reportSchedule->_id)
                ->where('schedule_id', (string)$reportSchedule->_id)
                ->where('contributorGroupId', $contributorGroup['id'])
                ->where('from_ts', $from_ts)
                ->where('to_ts', $to_ts)
                ->first();

            //echo "----";
            //print_r($result);exit;



            $presentation = new \App\Http\Controllers\Presentation\IFA_Berlin_2017_Controller();
            $presentation->setDate($from_ts, $to_ts);
            $presentation->setContributorGroup($contributorGroup);
            $presentation->setSchedules($schedule_ids);



            $link = $presentation->make($name);






            if (empty($result->_id)) {
                $final = new ReportFinal();
                $final->name = $name;
                $final->schedule_id = (string)$reportSchedule->_id;
                $final->from_ts = $from_ts;
                $final->to_ts = $to_ts;
                $final->link = $link;
                $final->contributorGroupId = $contributorGroup['id'];
                $final->contributorGroupName = $contributorGroup['name'];
                $final->save();

            } else {
                $final = ReportFinal::find($result->_id);
                $final->link = $link;
                $final->save();

            }

            //$status = $model->addNew($name, $from_ts, $to_ts, 0, $link);

            break;
        }



        return 'link: '.$link." <br> \n";

    }



}