<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Report\ReportColumn;
use App\Models\Report\ReportTemplate;
use Illuminate\Http\Request;
use Helper;


class ReportTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report_template.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$fields = ReportColumn::pluck('title', '_id');

        $fields_tmp = ReportColumn::pluck('title', '_id');
        $fields = [];
        foreach ($fields_tmp as $k => $v) {
            $fields[] = [
                '_id' => $k,
                'title' => $v,
            ];
        }

        $data = [
            //'model' => $model,
            //'_id' => $model->_id,
            //'title' => $model->title,
            'fields' => $fields,
            'selectedFields' => []
        ];

        //return view('report_template.create',compact('fields'));
        return view('report_template.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $model = new ReportTemplate();
        $data = array();

        foreach ($request->all() as $key => $value){
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->create($data))
                return redirect()->route('report.template.index');
*/

        $model = new ReportTemplate();
        $model->name = $request->input('name');
        $model->fields = explode('|', $request->input('selectedFields_ser'));

        //echo Helper::shout('now i\'m using my helper class in a controller!!');

        $model->save();
        return redirect()->route('report.template.index');







    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ReportTemplate::findOrFail($id);
        //$fields = ReportColumn::pluck('title', '_id');

        //return view('report_template.edit',compact(['model','fields']));


        $fields_tmp = ReportColumn::pluck('title', '_id');
        $fields = [];
        foreach ($fields_tmp as $k => $v) {
            $fields[] = [
                '_id' => $k,
                'title' => $v,
            ];
        }

/*
        $fieldsStr = [];
        foreach ($model->fields as $f) {
            $fieldsStr[] = (string)$f;
        }*/

        $selectedFields_tmp = ReportColumn::whereIn('_id', $model->fields)->get();


        $selectedFields_tmp = Helper::getMongoArray($selectedFields_tmp, '_id');


        $selectedFields = [];
        foreach ($model->fields as $fieldId) {
            $selectedFields[] = $selectedFields_tmp[$fieldId];

            // Нужно удалить из списка те, которые уже выбраны, чтобы они не были дважды
            //unset($fields[$f->_id]);

            foreach ($fields as $key2 => $subArr2) {
                if ($subArr2['_id'] == $fieldId) {
                    unset($fields[$key2]);
                }

            }

        }


/*

        $selectedFields = [];
        foreach ($selectedFields_tmp as $f) {
            $selectedFields[$f->_id] = $f->title;

            // Нужно удалить из списка те, которые уже выбраны, чтобы они не были дважды
            unset($fields[$f->_id]);
        }
*/


        //print_r($selectedFields->toArray());exit;

        //print_r($fields);exit;


        //$fields = ['a'];
        //$selectedFields = $model->fields;

        $data = [
            'model' => $model,
            '_id' => $model->_id,
            'title' => $model->title,
            'fields' => $fields,
            'selectedFields' => $selectedFields
        ];
        return view('report_template.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        $model = ReportTemplate::findOrFail($id);


        /*
         * $data = array();
        foreach ($request->all() as $key => $value){
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }
*/


        $model->name = $request->input('name');
        $model->fields = explode('|', $request->input('selectedFields_ser'));

        //echo Helper::shout('now i\'m using my helper class in a controller!!');

        $model->save();
        return redirect()->route('report.template.index');

        if ($model->validate($data))
            if($model->save($data))
                return redirect()->route('report.template.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = ReportTemplate::findOrFail($id);

        if($model->delete())
            return redirect()->route('report.template.index');
    }

    /**
     * Get data for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $model = new ReportTemplate();
        $data = $model->getList();

        return $model->datatables($data);
    }
}
