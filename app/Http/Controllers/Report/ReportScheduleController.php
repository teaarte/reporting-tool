<?php

namespace App\Http\Controllers\Report;

use App\Models\Report\ReportSchedule;
use App\Models\Report\Report;
use App\Models\Report\ReportTemplate;
use App\Models\Report\ReportData;
use App\Models\Report\MyDatabase;
use App\Models\Report\DatabaseItem;
use App\Models\Report\ReportColumn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Session;

class ReportScheduleController extends Controller
{



    public function create()
    {
        return view('report_schedule.create');
    }


    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //return view('user.profile', ['user' => Project::findOrFail($id)]);
    }



    public function index($reportId)
    {


        $data = [
            'reportId' => $reportId,
        ];

        return view('report_schedule.index', $data);
    }


    public function getList(Request $request)
    {

        $reportId = intval($request['reportId']);

        //print_r($request['reportId']);exit;
        $model = new ReportSchedule();
        $data = $model->getList($reportId);

        return $model->datatables($data);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reportId = new \MongoDB\BSON\ObjectID($request['report_id']);
        $model = new ReportSchedule();
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }


        $data['report_id'] = $reportId;

        if ($model->validate($data))
            if($model->create($data)) {
                return redirect('report-schedule/report/'.$reportId);
            }

//                return redirect()->route('report-schedule/report/'.$reportId);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ReportSchedule::findOrFail($id);

        return view('report_plan.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = ReportSchedule::findOrFail($id);
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->update($data))
                return redirect()->route('report-schedule.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = ReportSchedule::findOrFail($id);

        $model->delete();

        return redirect()->route('report.index');


        if($model->delete())
            return redirect()->route('report.index');
        /*
                if($model->users->isEmpty()){
                    if($model->delete())
                        return redirect()->route('project.index');
                }
                else{
                    return redirect()->route('project.index')->with('error', trans('project.group-has-users'));
                }

                */
    }






    // -------------------


    public function grid($index)
    {

        $reportTitle = null;
        $date = null;
        $start_date = Carbon::parse(config('report.start_date'));

        //$type = ReportTemplate::findBySlug(ReportTemplate::MONTHLY_REPORT)->getKey();

        // Проверка на дату
        /*
        if(preg_match("/^0\d{5,6}$/",$index,$matches)) {
            $parsedDate = $this->_parseStringToDate($matches[0]);

            try {
                $date = Carbon::parse($parsedDate);
            } catch(\Exception $x) {
                abort(404,'Invalid date');
            }

            if($date > Carbon::now() || $date < $start_date){
                abort(404,'Invalid date');
            }

            $reportTitle = $date->format('F Y');
        }*/
        // Проверка на существование пользовательского отчета
        /*else {
            $reportCustom = Report::findOrFail($index);
            $reportTitle = $reportCustom->name.' ('.Carbon::parse($reportCustom->from)->format('d F Y').'-'.Carbon::parse($reportCustom->to)->format('d F Y').') ';
            $type = $reportCustom->type_id;
        }*/



        $index = new \MongoDB\BSON\ObjectID($index);
        $ReportSchedule = ReportSchedule::findOrFail($index);

        //$reportTitle = $ReportSchedule->name.' ('.Carbon::parse($ReportSchedule->from)->format('d F Y').'-'.Carbon::parse($ReportSchedule->to)->format('d F Y').') ';
        $reportTitle = $ReportSchedule->name;
        /*
        if ($ReportSchedule->from == $ReportSchedule->to) {
            $reportTitle .= ' ('.Carbon::parse($ReportSchedule->from)->format('d F Y').') ';
        } else {
            $reportTitle .= ' ('.Carbon::parse($ReportSchedule->from)->format('d F Y').'-'.Carbon::parse($ReportSchedule->to)->format('d F Y').') ';
        }*/

        $Report = Report::findOrFail($ReportSchedule->report_id);

        $ReportTemplate = ReportTemplate::find(new \MongoDB\BSON\ObjectID($Report->template_id));



        $ReportData = new ReportData();

        // Проверка на существование отчета по индексу
        //$report = $ReportData->findReportByIndex($index)->first();

        // Создание отчета с данными, если не существует
        //if(!$report)
//            $report = $ReportData->addWithData($index, $type);

        $report = $ReportData->addWithData($ReportSchedule);
        //echo $Report->template_id;
        //print_r($ReportTemplate);exit;


        $fieldsMultiselect = Helper::prepareFieldsForForm($ReportTemplate->fields)['multiselect'];

        //print_r($fieldsMultiselect);exit;


        return view('report_schedule.grid', ['reportTitle' => $reportTitle, 'reportSchedule' => $ReportSchedule, 'fieldsMultiselect' => $fieldsMultiselect]);
    }





    public function getData(Request $request)
    {
        //header("Content-type: text/xml");

        //print_r($request->all());
        //echo ':::'.$request->db_id;exit;





        $ReportSchedule = ReportSchedule::findOrFail($request->reportSchedule_id);
        $Report = Report::findOrFail($ReportSchedule->report_id);

        $ReportTemplate = ReportTemplate::find(new \MongoDB\BSON\ObjectID($Report->template_id));


        //$db_id = ReportColumn::find();
        //$MyDatabase = MyDatabase::findOrFail($request->db_id);

        //$columns = $model->generateColumns();

//        print_r($Database);exit;




        $columns = [];
        $dataFromId = [];
        foreach ($ReportTemplate->fields as $columnId) {
            //print_r($column);continue;
            $column = ReportColumn::find($columnId);

            /*if ($columnId == '598abb7031f37200064f86fb') {
                //$item['type'] = 'ave';
                print_r($column);
                exit;
            }*/


            //$item->index = (string)$item->_id;


            $item = [
//                'width' => 70,
                'type' => 'ed',
                'align' => 'left',
                'sort' => 'str',
                'value' => $column->title,
                'id' => $column->index,
                'currency' => 'eur',
            ];


            switch ($column->type) {

                case 'text':
                    $item['type'] = 'txt';
                    break;

                case 'number':
                    $item['type'] = 'edn';
                    //$item['setNumberFormat'] = 'edn';
                    //$item['validate'] = 'ValidInteger';
                    break;

                case 'multiselect':
                    $item['type'] = 'clist';
                    $dataFromId[] = $column->index;
                    break;

                case 'checkbox':
                    $item['type'] = 'ch';
                    break;

                case 'date':
                    $item['type'] = 'dhxCalendarA';
                    break;

                case 'upload':
                    $item['type'] = 'upload';
                    break;

                case 'ave':
                    $item['type'] = 'ave';
                    break;

                case 'combo':
                    // Значит у нас хранятся айдишники и надо получать значение по айдишнику
                    $dataFromId[] = $column->index;
                    //$item['type'] = 'clist';

                    $item['type'] = 'combo';

                    $item['filter'] = 'true';
                    //$item['auto'] = 'true';
                    $item['source'] = '/report/database/getItems/'.$column->index;
                    break;

                default:
                    $item['type'] = 'ed';
                    break;
            }




            // sourceId
            /*
            if ($column->data) {

                $list = MyDatabase::findOrFail($column->data);
                ///print_r($list->items->toArray());exit;
                //$item->data = $list->items->ofContributorGroup(Session::get('contributorGroupId'));
                //$column->data = $list->items;
                $list = $list->items;
                $i = 1;
                foreach ($list as $v) {


                                        $item['options'][] = [
                                            //'id'    => $v->_id,
                                            //'value' => $v->name,
                                            'value'    => "$i",
                                            'text' => $v->name,
                                        ];

                    $i++;

                    //$item['options'][] = $v->name;

                }





            }


            */
            $columns[] = $item;


        }


        //print_r($columns);


        $reportData = ReportData::byScheduleAndContributorGroup($ReportSchedule->id)->get();
        //$reportData = ReportData::all();

        //print_r($reportData->toArray());exit;


        //$DatabaseItem = DatabaseItem::all();
        //print_r($DatabaseItem->toArray());
        $reportData = Helper::findNamesToIdsTable($reportData, $dataFromId);





        //$reportData = Helper::findNamesToIdsTable($reportData->items2, $dataFromId);
        $orig_rows = $reportData;
        //$orig_rows = $this->_addMissingKeys($columns, $databaseData);

        //print_r($orig_rows);exit;


        $rows = [];
        foreach ($orig_rows as $row) {

            $item = [
                'id' => $row->_id,
            ];
            $item['data'] = [];
            foreach ($columns as $column) {

                switch ($column['type']) {
                    case 'ch':
                        $item['data'][] = intval($row->{$column['id']}) == 0;
                        break;

                    case 'ave':

                        if (!empty($row->{$column['id']}['def'])) {
                            $defaultCurrency = $row->{$column['id']}['def'];
                            $item['data'][] = $row->{$column['id']}[$defaultCurrency].' '.$defaultCurrency;

                            //print_r($item['data']);exit;

                        } else {
                            // Чтобы из-за старых значений не поломалась система
                            if (!empty($row->{$column['id']})) {
                                $item['data'][] = $row->{$column['id']}.' EUR';
                            } else {
                                $item['data'][] = '';
                            }

                        }


                        break;

                    default:

                        $item['data'][] = $row->{$column['id']} ?? '';
                        break;
                }

            }

            $rows[] = $item;


        }

            //print_r($rows);exit;





        $table = [
            'head' => $columns,
            'rows' => $rows,
        ];

        $xml = new \SimpleXMLElementExtended("<?xml version=\"1.0\"?><rows></rows>");
        $head = $xml->addChild("head");

        foreach ($columns as $column) {
            //$child = $head->addChild("column", Helper::prepareXmlString($column['value']));
            $child = $head->addChildWithCData("column", $column['value']);

            //$child = $head->addChild("column");
            //$child->addCData(Helper::prepareXmlString($column['value']));

            //print_r($column); exit;

            if (!empty($column['id'])) {
                $child->addAttribute('id', $column['id']);
            }

            if (!empty($column['width'])) {
                $child->addAttribute('width', $column['width']);
            }

            if (!empty($column['type'])) {
                $child->addAttribute('type', $column['type']);
            }

            if (!empty($column['align'])) {
                $child->addAttribute('align', $column['align']);
            }

            if (!empty($column['sort'])) {
                $child->addAttribute('sort', $column['sort']);
            }

            if (!empty($column['source'])) {
                $child->addAttribute('source', $column['source']);
            }

            if (!empty($column['filter'])) {
                $child->addAttribute('filter', $column['filter']);
            }

            if (!empty($column['auto'])) {
                $child->addAttribute('auto', $column['auto']);
            }

            if (!empty($column['cache'])) {
                $child->addAttribute('cache', $column['cache']);
            }

            if (!empty($column['sub'])) {
                $child->addAttribute('sub', $column['sub']);
            }

            if (!empty($column['validate'])) {
                $child->addAttribute('validate', $column['validate']);
            }


            switch ($columns['type'] ?? '') {

                case 'ed':

                    break;


                default:

                    break;
            }
        }


        foreach ($rows as $row) {

            //print_r($row);exit;
            $child = $xml->addChild("row");
            $child->addAttribute('id', $row['id']);


            foreach ($row['data'] as $v2) {


                if (is_array($v2)) {
                    //print_r($row);exit;
                }


                //$child->addChild("cell", Helper::prepareXmlString($v2));
                $child->addChildWithCData("cell", $v2);
                //$child->addChild("cell");
                //$child->addCData($v2);

            }


        }


        echo $xml->asXML();

        exit;

        $tableEncoded = json_encode($table, JSON_PRETTY_PRINT);
        echo $tableEncoded; exit;



    }







    public function getData2(Request $request)
    {

        $columns = array();
        $ReportSchedule = ReportSchedule::findOrFail($request->reportSchedule_id);

        $Report = Report::findOrFail($ReportSchedule->report_id);

        $ReportTemplate = ReportTemplate::find(new \MongoDB\BSON\ObjectID($Report->template_id));


        //print_r($ReportTemplate->columns);exit;

        // Проверка на владельца отчета
        /*
        if(!$report->hasGroupAccess())
            abort(404,'Group access error');
*/



        //ReportData::findOrFail($ReportSchedule->report_id);

        $dataFromId = [];
        foreach ($ReportTemplate->fields as $column){
            $item = ReportColumn::find($column);

            //print_r($item->to);

            //$item->index = (string)$item->_id;

            //print_r($item->data);exit;


            if($item->data){
                $list = MyDatabase::findOrFail($item->data);
                $item->data = $list->items;

                if ($item->type == 'combo') {
                    // Значит у нас хранятся айдишники и надо получать значение по айдишнику
                    $dataFromId[] = $item->index;
                }

                //print_r($item->data);exit;
                // Для того, чтобы комбобоксе таблицы отображались правильные названия
                $item->displayKey =  'name';
                //$item->label = 'Country';
                // не работает правильно, баг..
                $item->valueKey =  'id';



            }

            //$item->cellTip = 'function(o){                return o.value;            }';

            if ($item->type == 'combo') {
                //$item->cellTip = 'return o.value;';
                //$item->cellTip = 'return o.item.data.value2;';
            }




            //$item->cellTip = 'console.dir(o); return "123"';



            //print_r($item);exit;

            if ($item) {
                $columns[] = $item;
            }



            //print_r($columns); exit;

        }


        if (Session::get('contributorGroupId') == 0) {

            $obj = new \stdClass;
            $obj->index = 'contributorId';
            $obj->type = 'string';
            $obj->title = 'Contributor';


            array_unshift($columns, $obj);


        }


        // Получаем поля, в которых хранятся не значения, а айдишники

        //\App\Models\Report\ReportColumn::where('index', $request->data['key'])->first()
        //\App\Models\Report\ReportSchedule::where('_id', $request->data['key'])->first()





        // If not exists columns
        if (!$columns) {
            return json_encode(false);
        }


        $data['columns'] = $columns;



        $reportData = ReportData::byScheduleAndContributorGroup($ReportSchedule->id)->get();
        //$reportData = ReportData::all();
        print_r($reportData);exit;
        foreach ($reportData as $v) {
            echo $v->contributor." <br>\n";
        }

        $reportData = Helper::findNamesToIdsTable($reportData, $dataFromId);



        print_r($reportData);exit;




        $data['data'] = $this->_addMissingKeys($columns, $reportData);
        $data['status'] = $ReportSchedule->status;

        //$data['data'][0]['addit'] = ['a' => 1, 'b' => 2];


        //print_r($data);exit;
        return json_encode($data);
    }

    public function addRow(Request $request)
    {
        $dataModel = new ReportData();

        $ReportSchedule = ReportSchedule::findOrFail($request->reportSchedule_id);

        $Report = Report::findOrFail($ReportSchedule->report_id);





        // Проверка на владельца отчета
        //if(!$report->hasGroupAccess())
//            abort(404,'Group access error');

        if($dataModel->addRow($request->id, $Report->template_id, $ReportSchedule))
            return response()->json(true);

        return response()->json(false);
    }

    /*
    public function removeRow(Request $request)
    {
        $dataModel = new ReportData();

        //$report = Report::findOrFail($request->report_id);
        $ReportSchedule = ReportSchedule::findOrFail($request->reportSchedule_id);

        // Проверка на владельца отчета
        //if(!$report->hasGroupAccess())
//            abort(404,'Group access error');

        if($dataModel->removeRow($request->id, $ReportSchedule->getKey()))
            return response()->json(true);

        return response()->json(false);
    }
*/


    public function removeRow(Request $request)
    {
        $dataModel = new ReportData();

        $ReportSchedule = ReportSchedule::findOrFail($request->reportSchedule_id);

        //echo '$request->rId'.$request->data['rId'];exit;
        if($dataModel->removeRow($request->data['rId'], $ReportSchedule->getKey()))
            return response()->json(true);

        return response()->json(false);
    }


    public function insertData(Request $request)
    {
        $dataModel = new ReportData();

        //$report = Report::findOrFail($request->report_id);
        $ReportSchedule = ReportSchedule::findOrFail($request->reportSchedule_id);

        // Проверка на владельца отчета
        //if(!$report->hasGroupAccess())
//            abort(404,'Group access error');


        $insertData = $request->data;

        // Основываясь на data->key надо определить какой столбец заполняем. Если это комбобокс, то писать ИД значения.

        $reportColumn = \App\Models\Report\ReportColumn::where('index', $request->data['cId'])->first();
        /*
        if ($reportColumn['type'] == 'combo') {
            // Получаем айди записи по ее значению



            $DatabaseItem = \App\Models\Report\DatabaseItem::where('name', $request->data['value'])
                ->where('list_id', $reportColumn['data'])
                ->first();

            $insertData['rId'] = $request->data['key'];
            $insertData['value'] = (string)$DatabaseItem['_id'];
            //print_r($DatabaseItem);exit;
            //echo $request->data['key'];exit;

        }*/



        if ($reportColumn['type'] == 'multiselect') {
            // Получаем айди записей по ее значению

            if (!empty($request->data['value'])) {
                $valuesIds = [];
                $values = explode(',', $request->data['value']);
                foreach ($values as $v) {
                    $DatabaseItem = \App\Models\Report\DatabaseItem::where('name', $v)
                        ->where('list_id', $reportColumn['data'])
                        ->first();

                    $valuesIds[] = (string)$DatabaseItem['_id'];

                }

                $insertData['value'] = $valuesIds;

            } else {
                $insertData['value'] = '';
            }
        } elseif ($reportColumn['type'] == 'ave') {


            if (!empty($request->data['value'])) {
                list($money, $currency) = explode(" ", $request->data['value']);

                $insertData['value'] = [
                    'def'     => $currency,
                    $currency => floatval($money),
                ];

            } else {
                $insertData['value'] = '';
            }

            //print_r($insertData['value']);exit;


        }

        //print_r($insertData);exit;

        //echo 'Session::get(\'contributorGroupId\'): '.Session::get('contributorGroupId');exit;

/*
        $screenshot = new \App\Models\Screenshot();
        $screenshot->url = 'http://vingrad.com';
        $screenshot->save();
*/
        //Log::info('test');

//        Event::fire('App\Events\ScreenshotSaved', $screenshot);



        if ($reportColumn['type'] == 'string') {

            // Делаем скриншот, если указан урл
            // url, data, reportid



        }

        if ($reportColumn['type'] == 'number') {
            $insertData['value'] = floatval($insertData['value']);
        }








        $dataModel->add($insertData, $ReportSchedule->getKey());
    }

    public function changeStatus(Request $request, $id)
    {
        if($request->status !== Report::OPEN_STATUS && $request->status !== Report::CLOSE_STATUS)
            abort(404, 'Wrong data');

        $report = Report::findOrFail($id);
        if ($report->changeStatus($request->status))
            return redirect()->back();
    }

    private function _parseStringToDate($string)
    {
        $date = '01';

        if(strlen($string) == 6)
            $date = $date.'.'.substr($string, 0, 2);
        else
            $date = $date.'.'.substr($string, 1, 2);

        $date = $date.'.'.substr($string, -4, 4);

        return $date;
    }

    /*Добавление/удаление столбцов при изменении настроек типа отчета*/
    private function _addMissingKeys($columns, $data)
    {
        foreach ($columns as $column){
            foreach ($data as $item){
                if (!array_has($item, $column->index)){
                    $key = $column->index;
                    $item->$key = "";
                }
            }
        }
        return $data;
    }


    /**
     * Получаем клиентский айди, вставляем строку в базе и возвращаем айди из базы
     */
    public function generateNewRow(Request $request) {


        $dataModel = new ReportData();

        $objectId = $dataModel->generateNewRow($request->reportSchedule_id);
        if (!empty($objectId)) {
            return response()->json(['id' => $objectId, 's' => 1]);
        } else {
            return response()->json(['s' => 0]);
        }

    }


    public function getMiniStat(Request $request) {

        $contributorGroupId = Session::get('contributorGroupId');
        $reportSchedule_id = $request->input('reportSchedule_id');


        $result = [];

        if (!empty($contributorGroupId)) {
            //echo "----------".$reportSchedule_id;exit;
            $result['contributorGroupId'] = $contributorGroupId;
            $items = ReportData::where('reportSchedule_id', $reportSchedule_id)->where('contributorGroupId', $contributorGroupId)->get();
        } else {
            $items = ReportData::where('reportSchedule_id', $reportSchedule_id)->get();
        }




        $result['count'] = 0;
        $result['ave'] = 0;
        $positive_statemts = 0;
        $sentiment_defined_total = 0;

        $allMediaIds = [];




        //print_r($items->toArray());exit;

        foreach ($items as $item) {

            // Articles published
            $result['count']++;

            // Audience Reach
            // Получаем список всех медиа
            if (!empty($item['media_name'])) {
                $allMediaIds[] = new \MongoDB\BSON\ObjectID($item['media_name']);
            }



            // AVE generated

            if (isset($item['ave']['def'])) {
                $result['ave'] += intval($item['ave'][$item['ave']['def']]);
            } else {
                $result['ave'] += intval($item['ave']);
            }


            // Positive statements
            if ($item['sentiment'] == '599c90f74e801f0006148023') {
                $positive_statemts++;
            }

            if (!empty($item['sentiment'])) {
                $sentiment_defined_total++;
            }


        }

        // Audience Reach
        // По списку Медиа узнаем их reach

        $mediaData = \App\Models\Report\DatabaseItem::where('_id', ['$in' => $allMediaIds])->get();
        $result['reach'] = 0;
        foreach ($mediaData as $media) {
            if (!empty($media['circulation'])) {
                $result['reach'] += intval($media['circulation']);
            }
            if (!empty($media['unique_visitors'])) {
                $result['reach'] += intval($media['unique_visitors']);
            }

        }


        if ($result['reach'] < 1000000) {
            $result['reach'] = round($result['reach'] / 1000, 1).' k';
        } else {
            $result['reach'] = round($result['reach'] / 1000000, 1).' MN';
        }


        //print_r($mediaData);


        //$items = ReportData::where('reportSchedule_id', $reportSchedule_id)->get();





        // Выяисляем процент Positive statements
        if ($sentiment_defined_total == 0) {$sentiment_defined_total = 1;}
        $result['positive_statemts_percent'] = round($positive_statemts*100/$sentiment_defined_total, 2).'%';



        //print_r($result);

        header('Content-Type: application/json');
        echo json_encode(['result' => $result]);
        exit;





    }

}