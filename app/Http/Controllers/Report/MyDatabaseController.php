<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Report\MyDatabase;
use App\Models\Report\DatabaseItem;
use Illuminate\Http\Request;
use App\Models\Report\ReportColumn;
use Helper;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class MyDatabaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('my-database.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {




        $fields_tmp = ReportColumn::pluck('title', '_id');
        $fields = [];
        foreach ($fields_tmp as $k => $v) {
            $fields[] = [
                '_id'   => $k,
                'title' => $v,
            ];
        }

        $data = [
            //'model' => $model,
            //'_id' => $model->_id,
            //'title' => $model->title,
            'fields' => $fields,
            'selectedFields' => []
        ];



        return view('my-database.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $model = new ReportColumnList();
        $data = array();

        foreach ($request->all() as $key => $value){
            if($key == 'fields'){
                if($value){
                    foreach ($value as $k => $v){
                        if($v) {
                            $data[$key][$k] = htmlspecialchars( clean( $v, 'noHtml'), ENT_QUOTES );;
                            $data[$key][$k] = htmlspecialchars( clean( $v, 'noHtml'), ENT_QUOTES );;
                        }

                    }
                }
            }
            else{
                $data[$key] = htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
                $data[$key] = empty($data[$key]) ? null : $data[$key];
            }
        }

        if ($model->validate($data))
            if($model->create($data))
                return redirect()->route('report.database.index');
*/




        $model = new MyDatabase();
        $model->name = $request->input('name');
        $model->fields = explode('|', $request->input('selectedFields_ser'));

        //echo Helper::shout('now i\'m using my helper class in a controller!!');

        $model->save();
        return redirect()->route('report.database.index');







    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $model = MyDatabase::findOrFail($id);
        //$fields = ReportColumn::pluck('title', '_id');

        //return view('report_template.edit',compact(['model','fields']));


        $fields_tmp = ReportColumn::pluck('title', '_id');
        $fields = [];
        foreach ($fields_tmp as $k => $v) {
            $fields[] = [
                '_id' => $k,
                'title' => $v,
            ];
        }

        /*
                $fieldsStr = [];
                foreach ($model->fields as $f) {
                    $fieldsStr[] = (string)$f;
                }*/

        $selectedFields_tmp = ReportColumn::whereIn('_id', $model->fields)->get();


        $selectedFields_tmp = Helper::getMongoArray($selectedFields_tmp, '_id');


        $selectedFields = [];
        foreach ($model->fields as $fieldId) {
            $selectedFields[] = $selectedFields_tmp[$fieldId];

            // Нужно удалить из списка те, которые уже выбраны, чтобы они не были дважды
            //unset($fields[$f->_id]);

            foreach ($fields as $key2 => $subArr2) {
                if ($subArr2['_id'] == $fieldId) {
                    unset($fields[$key2]);
                }

            }

        }


        /*

                $selectedFields = [];
                foreach ($selectedFields_tmp as $f) {
                    $selectedFields[$f->_id] = $f->title;

                    // Нужно удалить из списка те, которые уже выбраны, чтобы они не были дважды
                    unset($fields[$f->_id]);
                }
        */


        //print_r($selectedFields->toArray());exit;

        //print_r($fields);exit;


        //$fields = ['a'];
        //$selectedFields = $model->fields;

        $data = [
            'model' => $model,
            '_id' => $model->_id,
            'title' => $model->title,
            'fields' => $fields,
            'selectedFields' => $selectedFields
        ];



        return view('my-database.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = MyDatabase::findOrFail($id);
        /*
        $data = array();

        foreach ($request->all() as $key => $value){
            if($key == 'columns'){
                if($value){
                    foreach ($value as $k => $v){
                        if($v)
                            $data[$key][$k] = htmlspecialchars( clean( $v, 'noHtml'), ENT_QUOTES );;
                    }
                }
            }
            else{
                $data[$key] = htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
                $data[$key] = empty($data[$key]) ? null : $data[$key];
            }
        }

        if ($model->validate($data))
            if($model->update($data))
                return redirect()->route('report.database.index');
        */

        $model->name = $request->input('name');
        $model->fields = explode('|', $request->input('selectedFields_ser'));

        //echo Helper::shout('now i\'m using my helper class in a controller!!');

        $model->save();
        return redirect()->route('report.database.index');





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$model = ReportColumnData::findOrFail($id);
//
        //if($model->delete())
           return redirect()->route('report.database.index');
    }

    /**
     * Get data for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {

        $model = new MyDatabase();
        $data = $model->getList();

        return $model->datatables($data);
    }

    public function fill($id)
    {
        $MyDatabase = MyDatabase::findOrFail($id);

        // Здесь надо определить какие поля будут Multi-selection select box, т.к. из значения надо передать в шаблон
        $fieldsMultiselect = Helper::prepareFieldsForForm($MyDatabase->fields)['multiselect'];







        return view('my-database.grid', ['model' => $MyDatabase, 'fieldsMultiselect' => $fieldsMultiselect]);
    }

    public function getData(Request $request)
    {

        //print_r($request->all());
        //echo ':::'.$request->db_id;exit;





        $MyDatabase = MyDatabase::findOrFail($request->db_id);
        //$columns = $model->generateColumns();

        //print_r(MyDatabase);exit;


        $columns = [];
        $dataFromId = [];
        foreach ($MyDatabase->fields as $columnId) {
            //print_r($column);continue;
            $column = ReportColumn::find($columnId);


            //$item->index = (string)$item->_id;
//print_r($column);exit;

            $item = [
//                'width' => 70,
                'type' => 'ed',
                'align' => 'left',
                'sort' => 'str',
                'value' => $column->title,
                'id' => $column->index,
            ];

            switch ($column->type) {
                case 'text':
                    $item['type'] = 'txt';
                    break;

                case 'number':
                    $item['type'] = 'edn';
                    //$item['setNumberFormat'] = 'edn';
                    break;



                case 'multiselect':
                    $item['type'] = 'clist';
                    $dataFromId[] = $column->index;
                    break;


                case 'checkbox':
                    $item['type'] = 'ch';
                    break;

                case 'date':
                    $item['type'] = 'dhxCalendarA';
                    break;

                case 'upload':
                    $item['type'] = 'upload';
                    break;

                case 'combo':
                    // Значит у нас хранятся айдишники и надо получать значение по айдишнику
                    $dataFromId[] = $column->index;
                    //$item['type'] = 'clist';

                    $item['type'] = 'combo';

                    $item['filter'] = 'true';
                    //$item['auto'] = 'true';
                    $item['source'] = '/report/database/getItems/'.$column->index;
                    break;

                default:
                    $item['type'] = 'ed';
                    break;
            }



            // sourceId
            if ($column->data) {

                $list = MyDatabase::findOrFail($column->data);
                ///print_r($list->items->toArray());exit;
                //$item->data = $list->items->ofContributorGroup(Session::get('contributorGroupId'));
                //$column->data = $list->items;
                $list = $list->items;
                $i = 1;
                foreach ($list as $v) {

/*
                    $item['options'][] = [
                        //'id'    => $v->_id,
                        //'value' => $v->name,
                        'value'    => "$i",
                        'text' => $v->name,
                    ];
*/
                    $i++;

                    //$item['options'][] = $v->name;

                }





            }

            $columns[] = $item;


        }




        //print_r($columns);

        $databaseData = Helper::findNamesToIdsTable($MyDatabase->items2, $dataFromId);


        //echo "!!!!!!!!!!";exit;



        //print_r($databaseData);exit;
        $orig_rows = $databaseData;
        //$orig_rows = $this->_addMissingKeys($columns, $databaseData);


        $rows = [];
        foreach ($orig_rows as $row) {

            $item = [
              'id' => $row->_id,
            ];
            $item['data'] = [];
            foreach ($columns as $column) {

                switch ($column['type']) {
                    case 'ch':
                        $item['data'][] = intval($row->{$column['id']}) == 0;
                        break;
                    case 'clist':

                        //print_r($row->{$column['id']});exit;
                        //// Нужно по МонгоИд получить текстовые значения и превратить их в строку
                        //print_r($row);exit;
                        $item['data'][] = $row->{$column['id']} ?? '';
                        break;

                    default:




                        $item['data'][] = $row->{$column['id']} ?? '';
                        break;
                }

            }





            $rows[] = $item;


        }

        //print_r($rows);





        $table = [
            'head' => $columns,
            'rows' => $rows,
        ];



        $xml = new \SimpleXMLElementExtended("<?xml version=\"1.0\" encoding=\"UTF-8\"?><rows></rows>");
        $head = $xml->addChild("head");

        foreach ($columns as $column) {
            //$child = $head->addChild("column", htmlspecialchars($column['value']));

            $child = $head->addChildWithCData("column", htmlspecialchars($column['value']));

            //$child = $head->addChild("column");
            //$child->addCData(Helper::prepareXmlString($column['value']));

            //print_r($column); exit;

            if (!empty($column['id'])) {
                $child->addAttribute('id', $column['id']);
            }

            if (!empty($column['width'])) {
                $child->addAttribute('width', $column['width']);
            }

            if (!empty($column['type'])) {
                $child->addAttribute('type', $column['type']);
            }

            if (!empty($column['align'])) {
                $child->addAttribute('align', $column['align']);
            }

            if (!empty($column['sort'])) {
                $child->addAttribute('sort', $column['sort']);
            }

            if (!empty($column['source'])) {
                $child->addAttribute('source', $column['source']);
            }

            if (!empty($column['filter'])) {
                $child->addAttribute('filter', $column['filter']);
            }

            if (!empty($column['auto'])) {
                $child->addAttribute('auto', $column['auto']);
            }

            if (!empty($column['cache'])) {
                $child->addAttribute('cache', $column['cache']);
            }

            if (!empty($column['sub'])) {
                $child->addAttribute('sub', $column['sub']);
            }


            switch ($columns['type'] ?? '') {

                case 'ed':

                    break;


                default:

                    break;
            }
        }


        foreach ($rows as $row) {

            //print_r($row);exit;
            $child = $xml->addChild("row");
            $child->addAttribute('id', $row['id']);


            foreach ($row['data'] as $v2) {


                if (is_array($v2)) {
                    //print_r($row);exit;
                }




                $child->addChildWithCData("cell", $v2);
                //$child->addChild("cell");
                //$child->addCData(Helper::prepareXmlString($v2));
            }


        }


        echo $xml->asXML();

        exit;




        echo Helper::array2xml($table);
        exit;


        $tableEncoded = json_encode($table, JSON_PRETTY_PRINT);
        echo $tableEncoded;exit;






        $a = <<<EOF
{
   head:[
      { width:70,  type:"dyn",   align:"right",  sort:"int", value:"Sales" },
      { width:150, type:"ed",    align:"left",   sort:"str", value:"Book Title" },
      { width:100, type:"ed",    align:"left",   sort:"str", value:"Author" },
      { width:80,  type:"price", align:"right",  sort:"str", value:"Price" },
      { width:80,  type:"ch",    align:"center", sort:"str", value:"In Store" },
      { width:80,  type:"co",    align:"left",   sort:"str", value:"Shipping",
        options:[
          { id:1, value:"Fast"},
          { id:2, value:"Slow"}
        ]}
   ],
   rows:[
       {  id:1001,
          data:[
               "100",
               "A Time to Kill",
               "John Grisham",
               "12.99",
               "1",
               "1"] },
       {  id:1002, 
          data:[
               "1000",
               "Blood and Smoke",
               "Stephen King",
               "0",
               "1",
               "1"] },
       {  id:1003, 
          data:[
               "-200",
               "The Rainmaker",
               "John Grisham",
               "7.99",
               "0",
               "2"] }
   ]
}
EOF;

        echo $a; exit;


    }



    public function insertData(Request $request)
    {
        $dataModel = new DatabaseItem();

        $list = MyDatabase::findOrFail($request->db_id);



        $insertData = $request->data;

        //print_r($insertData); exit;

        // Основываясь на data->key надо определить какой столбец заполняем. Если это комбобокс, то писать ИД значения.

        $reportColumn = \App\Models\Report\ReportColumn::where('index', $request->data['cId'])->first();


        /*
        if ($reportColumn['type'] == 'combo') {
            // Получаем айди записи по ее значению

            //echo "-------------";Exit;


            $DatabaseItem = \App\Models\Report\DatabaseItem::where('name', $request->data['value'])
                ->where('list_id', $reportColumn['data'])
                ->first();

            $insertData['rId'] = $request->data['key'];
            $insertData['value'] = (string)$DatabaseItem['_id'];
            //print_r($DatabaseItem);exit;
            //echo $request->data['key'];exit;

        }*/


        if ($reportColumn['type'] == 'multiselect') {
            // Получаем айди записей по ее значению

            if (!empty($request->data['value'])) {
                $valuesIds = [];
                $values = explode(',', $request->data['value']);
                foreach ($values as $v) {
                    $DatabaseItem = \App\Models\Report\DatabaseItem::where('name', $v)
                        ->where('list_id', $reportColumn['data'])
                        ->first();

                    $valuesIds[] = (string)$DatabaseItem['_id'];

                }

                $insertData['value'] = $valuesIds;

            } else {
                $insertData['value'] = '';
            }


        }


        //print_r($list);exit;

        //echo "_-".$list->getKey();

        $dataModel->add($insertData, $list->getKey());
    }

    public function removeRow(Request $request)
    {
        $dataModel = new DatabaseItem();

        $list = MyDatabase::findOrFail($request->db_id);

        if($dataModel->removeRow($request->data['rId'], $list->getKey()))
            return response()->json(true);

        return response()->json(false);
    }

    /*Добавление/удаление столбцов при изменении настроек типа отчета*/
    private function _addMissingKeys($columns, $data)
    {
        foreach ($columns as $column){
            foreach ($data as $item){
                if (!array_has($item, $column['index'])){
                    $key = $column['index'];
                    $item->$key = "";
                }
            }
        }
        return $data;
    }


    /**
     * Получаем клиентский айди, вставляем строку в базе и возвращаем айди из базы
     */
    public function generateNewRow(Request $request) {


        $dataModel = new DatabaseItem();

        $objectId = $dataModel->generateNewRow($request->db_id);
        if (!empty($objectId)) {
            return response()->json(['id' => $objectId, 's' => 1]);
        } else {
            return response()->json(['s' => 0]);
        }

    }

    public function getItems(Request $request) {


        //echo "data: ". $request->field;

        //$column = ReportColumn::find($request->field);
        $reportColumn = ReportColumn::where('index', $request->field)->first();
        $db = MyDatabase::where('_id', new \MongoDB\BSON\ObjectID($reportColumn->data))->first();

        //print_r($db->fields[0]);exit;
        //echo "data: ". $reportColumn->data;




        //$reportColumn->data = "599b456a9405601e64006194";

        //$DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', new \MongoDB\BSON\ObjectID($reportColumn->data))->get();
        if (empty($request->mask)) {
            //$DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->skip(6)->limit(5)->get();

            if ($request->field == 'media_name') {

                if (empty(Session::get('contributorGroupId'))) {
                    $DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->get();
                } else {
                    $DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->where('contributorGroupId', Session::get('contributorGroupId'))->get();
                }

            } else {
                $DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->get();
            }

        } else {
            if ($request->field == 'media_name') {
                if (empty(Session::get('contributorGroupId'))) {
                    $DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->get();
                } else {
                    $DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->where('contributorGroupId', Session::get('contributorGroupId'))->get();
                }

            } else {
                $DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->get();
            }

            //$DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $reportColumn->data)->where('name', 'LINE', "%{$request->mask}%")->get();
        }




        header("Content-type: text/xml");



        $xml = new \SimpleXMLElementExtended("<?xml version=\"1.0\"?><complete></complete>");

        foreach ($DatabaseItem as $item) {

            //$option = $xml->addChild("option", htmlspecialchars($item->name));

            $name = $item->name;

            if (!empty($item->media_type)) {
                $name = Helper::addMediaTypeAsSuffix($name, $item->media_type);
            }

            $option = $xml->addChildWithCData("option", $name);


            $option->addAttribute('value', $item->_id);


/*
            try {
                //$item->name
                $option = $xml->addChild("option", htmlspecialchars($item->name));
                $option->addAttribute('value', $item->_id);
            } catch (\Exception $e) {
                print_r($item->toArray());
                exit;
                print_r($e);
            }*/

        }


        echo $xml->asXML();
        exit;


    }


}
