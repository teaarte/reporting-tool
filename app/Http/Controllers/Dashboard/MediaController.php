<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }


    public function test()
    {


        $items = [];

        $items[] = [
            'name' => 'a',
            'y'     => 50,
        ];
        $items[] = [
            'name' => 'b',
            'y'     => 50,
        ];


        $pieData = [
            'infile' => [
                'chart' => [
                    'type' => 'pie'
                ],
                'series' => [[
                    'name' => 'Test',
                    'colorByPoint' => true,
                    'data' => $items,
                ]]

            ],
            'constr' => "Chart",
            'type' => "image/png",
            'asyncRendering' => false,



            //'async' => true,
        ];

        $result = \App\Models\Charts::getPie($pieData);
        //$result = \App\Models\Charts::getStackedBar([]);

        header('Content-Type: image/png');

        echo $result;exit;
    }




}
