<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Group();
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->create($data))
                return redirect()->route('group.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Group::findOrFail($id);

        return view('group.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Group::findOrFail($id);
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->update($data))
                return redirect()->route('group.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Group::findOrFail($id);

        if($model->users->isEmpty()){
            if($model->delete())
                return redirect()->route('group.index');
        }
        else{
            return redirect()->route('group.index')->with('error', trans('group.group-has-users'));
        }
    }

    /**
     * Get data for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $model = new Group();
        $data = $model->getList();

        return $model->datatables($data);
    }
}
