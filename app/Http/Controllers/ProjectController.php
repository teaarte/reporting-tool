<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ProjectController extends Controller
{


    public function create()
    {
        return view('project.create');
    }


    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //return view('user.profile', ['user' => Project::findOrFail($id)]);
    }



    public function index()
    {
        return view('project.index');
    }


    public function getList()
    {
        $model = new Project();
        $data = $model->getList();

        return $model->datatables($data);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Project();
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->create($data))
                return redirect()->route('project.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Project::findOrFail($id);

        return view('project.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Project::findOrFail($id);
        $data = array();

        foreach ($request->all() as $key => $value)
        {
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        if ($model->validate($data))
            if($model->update($data))
                return redirect()->route('project.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Project::findOrFail($id);

        $model->delete();

        return redirect()->route('project.index');


        if($model->delete())
            return redirect()->route('project.index');
/*
        if($model->users->isEmpty()){
            if($model->delete())
                return redirect()->route('project.index');
        }
        else{
            return redirect()->route('project.index')->with('error', trans('project.group-has-users'));
        }

        */
    }

}