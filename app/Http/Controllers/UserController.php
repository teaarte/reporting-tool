<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::all();

        return view('user.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new User();
        $data = array();

        foreach ($request->except('group') as $key => $value)
        {
            $data[$key] = htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        $data['group'] = $request->input('group');

        if ($model->validate($data))
            if($model->createRow($data))
                return redirect()->route('user.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = User::findOrFail($id);
        $groups = Group::all();

        return view('user.edit',compact(['model','groups']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = User::findOrFail($id);
        $data = array();

        foreach ($request->except('group') as $key => $value)
        {
            $data[$key] = is_array($value) ? $request->$key : htmlspecialchars( clean( $request->$key, 'noHtml'), ENT_QUOTES );
            $data[$key] = empty($data[$key]) ? null : $data[$key];
        }

        $data['group'] = $request->input('group');

        if ($model->validate($data, $model->getKey()))
            if($model->updateRow($data))
                return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = User::findOrFail($id);
        $userRole = Sentinel::findRoleBySlug('user');
        $userRole->users()->detach($model);

        if($model->delete())
            return redirect()->route('user.index');
    }

    /**
     * Get data for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $model = new User();
        $data = $model->getList();

        return $model->datatables($data);
    }


    public function changeContributorGroup(Request $request)
    {
        $model = User::findOrFail(Auth::id());
        $group = $request->contributorGroupId;
        if($model->setActiveContributorGroup($group))
            return redirect()->back();
    }

    public function changeProject(Request $request)
    {
        $model = User::findOrFail(Auth::id());
        $project = $request->project;
        if($model->setActiveProject($project))
            return redirect()->back();
    }

}
