<?php

namespace App\Helpers;

use App\Models\Group;
use App\Models\Report\MyDatabase;
use App\Models\Report\DatabaseItem;
use App\Models\Report\ReportColumn;

class Helper
{


    public static function shout(string $string)
    {
        return strtoupper($string);
    }

    public static function prepareXmlString($raw)
    {
        return $raw;

        $raw = urlencode(utf8_encode($raw));
        $raw = str_replace('++','',$raw);
        $raw = urldecode($raw);

        $raw = htmlspecialchars($raw);

        //$raw = preg_replace("/>\s+</", "><", $raw);

        preg_replace('/[\x00-\x1F\x7F]/', '', $raw);


        return $raw;

    }


    public static function array2xml($data, $root = null){
        $xml = new \SimpleXMLElement($root ? '<' . $root . '/>' : '<root/>');
        array_walk_recursive($data, function($value, $key)use($xml){
            $xml->addChild($key, $value);
        });
        return $xml->asXML();
    }


    public static function prepareFieldsForForm($fields) {

        $fieldsMultiselect = [];
        $columnIndex = 0;
        foreach ($fields as $columnId) {
            $column = ReportColumn::find($columnId);

            if ($column->type == 'multiselect') {


                $DatabaseItem = \App\Models\Report\DatabaseItem::where('list_id', $column->data)->get();

                $field = ['values' => []];
                foreach ($DatabaseItem as $item) {

                    $field['values'][] = $item->name;

                }


                $field['columnIndex'] = $columnIndex;

                $fieldsMultiselect[] = $field;

            }


            $columnIndex++;

        }


        return ['multiselect' => $fieldsMultiselect];
    }

    public static function findContributors($data, $dataFromId) {

        foreach($data as $k => $row) {


            //if (isset($row->contributorGroupId)) {

            //}

            //Group::findOrFail($row->contributorGroupId);




        }

    }

    public static function findNamesToIdsTable($data, $dataFromId) {

        $cache = [];


        foreach ($data as $k => $row) {

            $data[$k]['value2'] = 'abc';

            //print_r(array_keys((array)$v));exit;

            foreach ($dataFromId as $fieldKey) {
                if (isset($row->$fieldKey)) {

                    // $row[$fieldKey] - тут содержится айди, и надо получить значение

                    // в блоке try, т.к. может быть проблема с mongoid
                    try {


                        if (is_array($row[$fieldKey])) {


                            $str = [];

                            foreach ($row[$fieldKey] as $id) {


                                if (!isset($cache[(string)$id])) {
                                    $DatabaseItem = \App\Models\Report\DatabaseItem::where('_id', new \MongoDB\BSON\ObjectID($id))
                                        ->first();

                                    $cache[(string)$id] = $DatabaseItem['name'];
                                    $str[] = $DatabaseItem['name'];

                                }

                                $str[] = $cache[(string)$id];


                            }




                            // Выводим значение из БД
                            $data[$k][$fieldKey] = implode(',', $str);

                            //echo "--+-".$data[$k][$fieldKey];print_r($str); exit;

                            //print_r($data[$k]);exit;



                        } else {

                            if (!isset($cache[(string)$row[$fieldKey]])) {
                                $DatabaseItem = \App\Models\Report\DatabaseItem::where('_id', new \MongoDB\BSON\ObjectID($row[$fieldKey]))
                                    ->first();
                                //print_r($DatabaseItem);exit;
                                if ($DatabaseItem['list_id'] == '599c91284e801f0006148026') {
                                    // Доблавем тип медиа к названию
                                    $DatabaseItem['name'] = self::addMediaTypeAsSuffix($DatabaseItem['name'], $DatabaseItem['media_type']);

                                }


                                // Выводим значение из БД
                                $cache[(string)$row[$fieldKey]] = $DatabaseItem['name'];
                            }


                            $data[$k][$fieldKey] = $cache[(string)$row[$fieldKey]];


                            //print_r($data[$k][$fieldKey]);exit;

                        }




                    } catch (\Exception $e) {

                    }
                }
            }
//print_r($data[$k]->toArray());

        }

        //exit;
        return $data;
    }

    public static function getMongoFieldArray(&$cur, $f = '_id', $toString = false, $toArray = false) {
        if (empty($f)) {$f = '_id';}

        $mas = [];

        if ($toArray) {
            $cur = iterator_to_array($cur, false);
        }


        foreach ($cur as $m) {

            if ($toString) {
                $mas[] = (string)$m[$f];
            } else {
                $mas[] = $m[$f];
            }

        }

        return $mas;
    }

    /*
     *
     * @param bool|string $retAllIdInKey - если не false, то ключ в котором будм сохранены все _id
     * $cur нельзя принимать по ссылке, т.к. это часто не переменная, а курсор
    */
    public static function getMongoArray($cur, $field_as_key = '', $field_to_string = '', $retAllIdInKey = false) {

        // вызываем нативную PHP-функцию, где можно
        if ($field_as_key === '' && $field_to_string === '' && $retAllIdInKey === false) {
            return iterator_to_array($cur, false);
        } elseif ($field_as_key === '_id' && $field_to_string === '' && $retAllIdInKey === false) {
            //return iterator_to_array($cur, true);

            $arr = iterator_to_array($cur, false);
            $res = [];
            foreach ($arr as $k => $v) {
                $res[(string)$v[$field_as_key]] = $v;
            }
            return $res;
        }


        $mas = [];

        foreach ($cur as $m) {

            if (!empty($field_to_string)) {
                if (isset($m[ $field_to_string ])) {
                    $m[ $field_to_string ] = (string)$m[ $field_to_string ];
                }
            }

            if (empty($field_as_key)) {

                $mas[] = $m;

            } else {

                $mas[ (string)$m[$field_as_key] ] = $m;

            }

            // Если нам отдельно нужно все айдшиники сохранить в каком-то ключе
            if ( $retAllIdInKey != false ) {
                if ( $field_as_key == $field_to_string ) {
                    $mas[ $retAllIdInKey ][] = (string)$m[ $field_as_key ];
                } else {
                    $mas[ $retAllIdInKey ][] = $m[ $field_as_key ];
                }

            }

        }

        return $mas;
    }

// Конвертирует монгоИд значения в массиве в строки, переработана
    public static function makeValuesStrings(array $arr) {
        return array_map(function($var) {return strval($var);}, $arr);
    }

// Конвертирует значения в массиве в Int, 21.12.2015 (EPA)
    public static function makeValuesIntegers(array $arr) {
        return array_map(function($var) {return intval($var);}, $arr);
    }

// Конвертирует массив строк в монгоИд массив
    public static function makeValuesMongoIds(array $arr) {
        return array_map(function($var) {return new \MongoDB\BSON\ObjectID($var);}, $arr);
    }



    public static function addMediaTypeAsSuffix($name, $media_type) {
        $media_type = (array)$media_type;
        $media_type_string_array = [];

        if (in_array('599c91424e801f00076dd215', $media_type)) {
            $media_type_string_array[] = 'D';
        }
        if (in_array('599c914b4e801f0006148028', $media_type)) {
            $media_type_string_array[] = 'P';
        }
        if (in_array('599c914e4e801f0006148029', $media_type)) {
            $media_type_string_array[] = 'T';
        }
        if (in_array('599c91534e801f000614802a', $media_type)) {
            $media_type_string_array[] = 'R';
        }

        if (count($media_type_string_array) > 0) {
            $name.= " (".implode(',', $media_type_string_array).")";
        }

        return $name;

    }



}
