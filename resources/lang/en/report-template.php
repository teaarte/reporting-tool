<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Report Type Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'report_template' => 'Report templates',
    'add_type' => 'Add new template',
    'template-list' => 'Templates',
    'templates' => 'Report templates',
    'name' => 'Name',
    'columns' => 'Columns',
    'fields' => 'Fields',
    'edit' => 'Edit type',
    'create' => 'Add new template',
    'name-placeholder' => 'Enter type name',
    'columns-placeholder' => 'Select columns',
    'actions' => 'Actions',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'add-column-warning' => 'Please add a report column before continue'

];
