<?php

return [


    'add' => 'Add new area',
    'projects' => 'Marketing areas',
    'name' => 'Name',
    'actions' => 'Actions',
    'create' => 'Create',
    'cancel' => 'Cancel',
    'area' => 'Marketing area',
    'name-placeholder' => '',

];
