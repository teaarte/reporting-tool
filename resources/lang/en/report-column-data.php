<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Report Column Data Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'database' => 'Database',
    'columns_data' => 'Database',
    'columns_data_lists' => 'Database',
    'add_data' => 'Add new entry',
    'name' => 'Name',
    'name-placeholder' => 'Enter list name',
    'data' => 'Fields for additional info',
    'actions' => 'Actions',
    'cancel' => 'Cancel',
    'create' => 'Add new list',
    'save' => 'Save',

];
