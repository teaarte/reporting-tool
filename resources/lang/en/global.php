<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'errors' => 'Errors',
    'reporting' => 'Reporting',
    'warning' => 'Warning',
    'columns_not_exists' => 'Columns are not exists ',
    'select-project' => 'Marketing area',
    'select-contributor-group' => 'Contributor',
    'select' => 'Select',
    'change' => 'Change'
];
