<?php

return [


    'add' => 'Add new area',
    'projects' => 'Report final',
    'name' => 'Name',
    'actions' => 'Actions',
    'create' => 'Create',
    'cancel' => 'Cancel',
    'area' => 'Marketing area',
    'name-placeholder' => '',

];
