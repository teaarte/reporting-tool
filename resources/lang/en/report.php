<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Report Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'reports' => 'Reports',
    'report_date' => 'Report for :reportTitle',
    'monthly' => 'Monthly',
    'monthly_reports' => 'Monthly reports',
    'custom_reports' => 'Custom reports',
    'create_report' => 'Create report',
    'cancel' => 'Cancel',
    'name' => 'Name',
    'name-placeholder' => 'Enter report name',
    'from' => 'From',
    'from-placeholder' => 'Choose from date',
    'to' => 'To',
    'to-placeholder' => 'Choose to date',
    'type' => 'Type',
    'type-placeholder' => 'Select report type',
    'save' => 'Save',
    'fill' => 'Fill',
    'report-settings' => 'Report settings',
    'close' => 'Close',
    'open' => 'Open',
    'report-database' => 'Report database',
    'template' => 'Template',
    'template-placeholder' => 'Select template',
    'period-placeholder' => 'Select period',
    'period' => 'Period',

];
