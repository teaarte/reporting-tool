<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table actions Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'edit' => 'Edit',
    'delete' => 'Delete',
    'fill' => 'Fill',
    'confirm-delete' => 'Are you sure?',

];
