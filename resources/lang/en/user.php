<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'users' => 'Reporters',
    'users-list' => 'Users list',
    'add-user' => 'Add user',
    'id' => 'ID',
    'email' => 'Email',
    'actions' => 'Actions',
    'create' => 'Add new user',
    'edit' => 'Edit user',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'email-placeholder' => 'Enter user email',
    'password' => 'Password',
    'password-placeholder' => 'Enter user password',
    'groups' => 'Groups',
    'add-group-warning' => 'Please add a group before continue'

];
