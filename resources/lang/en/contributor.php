<?php

return [


    'users' => 'Contributors',
    'groups' => 'Groups',
    'add' => 'Add new',
    'create' => 'Add new contributor',
    'edit' => 'Edit contributor',
    'group' => 'Contributor group',
    'create-group' => 'Add new contributor group',
    'add-group' => 'Add new group',



];
