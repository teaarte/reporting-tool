<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Report Column Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'fields' => 'Fields',
    'report_columns' => 'Fields',
    'add_column' => 'Add new field',
    'columns-list' => 'Fields list',
    'id' => 'ID',
    'title' => 'Title',
    'index' => 'Key',
    'index-placeholder' => 'Enter field key',
    'edit' => 'Edit field',
    'create' => 'Add new field',
    'title-placeholder' => 'Enter field title',
    'type' => 'Type',
    'type-placeholder' => 'Select field type',
    'actions' => 'Actions',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'data' => 'Data',
    'data-placeholder' => 'Select field data',

];
