<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Group Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'group' => 'Groups of reporters',
    'group-list' => 'User groups',
    'add-group' => 'Add user group',
    'id' => 'ID',
    'name' => 'Name',
    'actions' => 'Actions',
    'create' => 'Add new user group',
    'edit' => 'Edit group',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'name-placeholder' => 'Enter user group name',
    'group-has-users' => 'Group has users!'

];
