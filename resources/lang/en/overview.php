<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Overview Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'yes' => 'Yes',
    'no' => 'No',
    'group' => 'User group',
    'report-complete' => 'Report Complete',
    'remark' => 'Remark',
    'reminder' => 'Reminder'
];
