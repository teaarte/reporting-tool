
<div class="form-sortable" style="display: flex;">
    <div class="all-fields">

        <span>All:</span>

        <ul id="allFields">
            @foreach ($fields as $field)
                <li data-id="{{ $field['_id'] }}">{{ $field['title'] }}</li>
            @endforeach
        </ul>
    </div>

    <div class="selected-fields">

        <span>Selected:</span>


        <ul id="selectedFields">
            @foreach ($selectedFields as $field)
                <li data-id="{{ $field['_id'] }}">{{ $field['title'] }}</li>
            @endforeach
        </ul>
    </div>
</div>

{{ Form::hidden('selectedFields_ser', '222', array('id' => 'selectedFields_ser')) }}




<script>
    $( document ).ready(function() {

        var el = document.getElementById('allFields');
        var sortable = Sortable.create(el, {group: "fields"});


        var el = document.getElementById('selectedFields');
        var sortable = Sortable.create(el, {
            group: "fields",
            onSort: function (e) {

                var items = e.to.children;
                var result = [];
                for (var i = 0; i < items.length; i++) {
                    result.push($(items[i]).data('id'));
                }
                console.log(result);
                $('#selectedFields_ser').val(result.join('|'));

                /* Do ajax call here */
            }

        });
    });

</script>