@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('project.group') }}
@endsection

@section('contentheader_title')
    {{ trans('project.group') }}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('errors'))
                    <div class="callout callout-danger">
                        <h4>{{trans('global.errors')}}</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ trans($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('group.edit')}}</h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('user.index')}}" class="btn btn-sm btn-primary">{{trans('user.cancel')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        {!!Form::model($model,['route' => ['project.update',  $model->getKey()], 'method' => 'put'])!!}
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('name', trans('group.name')) !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('project.name-placeholder')]) !!}
                            </div>
                        </div>
                        <div class="box-footer">
                            {!! Form::submit(trans('group.save'),['class' => 'btn btn-primary btn-flat btn-md']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
