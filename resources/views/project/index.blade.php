@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('projects.projects') }}
@endsection

@section('contentheader_title')
    {{ trans('projects.projects') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/project.js')}}"></script>
@endsection

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('project.create')}}" class="btn btn-sm btn-primary">{{trans('projects.add')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" id="projects-table">
                            <thead>
                            <tr>
                                <th>{{trans('projects.name')}}</th>
                                <th class="table-actions">{{trans('projects.actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
