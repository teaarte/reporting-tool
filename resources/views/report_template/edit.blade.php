@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-template.report_template') }}
@endsection

@section('contentheader_title')
    {{ trans('report-template.report_template') }}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            @if(empty($fields))
                <div class="col-md-12">
                    <div class="box box-solid box-danger">
                        <div class="box-header">
                            <h3 class="box-title">{{trans('global.warning')}}</h3>
                        </div>
                        <div class="box-body">
                            {{trans('report-template.add-column-warning')}}
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                @if(Session::has('errors'))
                    <div class="callout callout-danger">
                        <h4>{{trans('global.errors')}}</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ trans($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('report-template.create')}}</h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('report.template.index')}}" class="btn btn-sm btn-primary">{{trans('report-template.cancel')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        {!!Form::model($model,['route' => ['report.template.update',  $model->getKey()], 'method' => 'put'])!!}
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('name', trans('report-template.name')) !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('report-template.name-placeholder')]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('fields', trans('report-template.fields')) !!}

                                {{ Form::sortable('fields', $fields, $selectedFields) }}

                            </div>
                        </div>
                        <div class="box-footer">
                            {!! Form::submit(trans('report-template.save'),['class' => 'btn btn-primary btn-flat btn-md']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



