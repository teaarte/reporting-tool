@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-template.report_template') }}
@endsection

@section('contentheader_title')
    {{ trans('report-template.report_template') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/report-template.js')}}"></script>
@endsection

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('report.template.create')}}" class="btn btn-sm btn-primary">{{trans('report-template.add_type')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" id="report-templates-table">
                            <thead>
                            <tr>
                                <th>{{trans('report-template.name')}}</th>
                                <th class="table-actions">{{trans('report-column.actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
