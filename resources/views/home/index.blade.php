@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Reports by countries
@endsection

@section('contentheader_title')
    Reports by countries
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
            @for($now = \Carbon\Carbon::now(); $now >= $start_date; $now->subMonth())
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$now->format('F Y')}}</h3>
                    </div>
                    <div class="box-body">
                        @if(Auth::user()->inRole('admin'))
                            <script>window.location.href = "/report";</script>
                        @else
                            <script>window.location.href = "/report";</script>
                        @endif
                    </div>
                </div>
            @endfor
            </div>
        </div>
    </div>
@endsection
