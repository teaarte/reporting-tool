@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('contributor.group') }}
@endsection

@section('contentheader_title')
    {{ trans('contributor.group') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/group.js')}}"></script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            @if (session('error'))
                <div class="col-md-12">
                    <div class="box box-solid box-danger">
                        <div class="box-header">
                            <h3 class="box-title">{{trans('global.warning')}}</h3>
                        </div>
                        <div class="box-body">
                            {{ session('error') }}
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('group.create')}}" class="btn btn-sm btn-primary">{{trans('contributor.add-group')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" id="groups-table">
                            <thead>
                            <tr>
                                <th>{{trans('group.name')}}</th>
                                <th class="table-actions">{{trans('group.actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
