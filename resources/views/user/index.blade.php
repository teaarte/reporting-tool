@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('contributor.users') }}
@endsection

@section('contentheader_title')
    {{ trans('contributor.users') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/user.js')}}"></script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('user.create')}}" class="btn btn-sm btn-primary">{{trans('contributor.add')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" id="users-table">
                            <thead>
                            <tr>
                                <th>{{trans('user.email')}}</th>
                                <th>{{trans('user.password')}}</th>
                                <th>{{trans('user.groups')}}</th>
                                <th class="table-actions">{{trans('user.actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
