@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('contributor.users') }}
@endsection

@section('contentheader_title')
    {{ trans('contributor.users') }}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            @if($groups->isEmpty())
                <div class="col-md-12">
                    <div class="box box-solid box-danger">
                        <div class="box-header">
                            <h3 class="box-title">{{trans('global.warning')}}</h3>
                        </div>
                        <div class="box-body">
                            {{trans('user.add-group-warning')}}
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                @if(Session::has('errors'))
                    <div class="callout callout-danger">
                        <h4>{{trans('global.errors')}}</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ trans($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('contributor.create')}}</h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('user.index')}}" class="btn btn-sm btn-primary">{{trans('user.cancel')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'user.store']) !!}
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('email', trans('user.email')) !!}
                                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('user.email-placeholder')]) !!}
                            </div>
                            <div class="form-group">
                                @foreach($groups as $item)
                                    {!! Form::checkbox('group[]', $item->getKey()) !!} {{$item->name}}
                                @endforeach
                            </div>
                        </div>
                        <div class="box-footer">
                            {!! Form::submit(trans('user.save'),['class' => 'btn btn-primary btn-flat btn-md']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
