@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-column.report_columns') }}
@endsection

@section('contentheader_title')
    {{ trans('report-column.report_columns') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/report-column.js')}}"></script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('report.column.create')}}" class="btn btn-sm btn-primary">{{trans('report-column.add_column')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" id="report-columns-table">
                            <thead>
                            <tr>
                                <th>{{trans('report-column.index')}}</th>
                                <th>{{trans('report-column.title')}}</th>
                                <th>{{trans('report-column.type')}}</th>
                                <th class="table-actions">{{trans('report-column.actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
