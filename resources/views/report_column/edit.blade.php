@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-column.report_columns') }}
@endsection

@section('contentheader_title')
    {{ trans('report-column.report_columns') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/report-column.js')}}"></script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('errors'))
                    <div class="callout callout-danger">
                        <h4>{{trans('global.errors')}}</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ trans($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('report-column.edit')}}</h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('report.column.index')}}" class="btn btn-sm btn-primary">{{trans('report-column.cancel')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        {!!Form::model($model,['route' => ['report.column.update',  $model->getKey()], 'method' => 'put'])!!}
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('index', trans('report-column.index')) !!}
                                {!! Form::text('index', null, ['class' => 'form-control', 'placeholder' => trans('report-column.index-placeholder')]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('title', trans('report-column.title')) !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => trans('report-column.title-placeholder')]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('type', trans('report-column.type')) !!}
                                {!! Form::select('type', $types, null, ['class' => 'form-control', 'placeholder' => trans('report-column.type-placeholder')]) !!}
                            </div>
                            <div class="form-group report-column-data">
                                {!! Form::label('data', trans('report-column.data')) !!}
                                {!! Form::select('data', $data, null, ['class' => 'form-control', 'placeholder' => trans('report-column.data-placeholder')]) !!}
                            </div>
                        </div>
                        <div class="box-footer">
                            {!! Form::submit(trans('report-column.save'),['class' => 'btn btn-primary btn-flat btn-md']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
