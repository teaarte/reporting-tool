<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/chartjs/Chart.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/select2/select2.js')}}" type="text/javascript"></script>

<script src="/dhtmlxSuite/codebase/dhtmlx.js"></script>
<!--
<script src="/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
<script src="/dhtmlxGrid/sources/\dhtmlxCommon/codebase/dhtmlxcommon.js"></script>
<script src="/dhtmlxGrid/sources/dhtmlxCombo/codebase/dhtmlxcombo.js"></script>
-->

<script src="{{ asset('/js/custom.js') }}" type="text/javascript"></script>
@yield('js')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
