<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Overview</li>
            <!-- Optionally, you can add icons to the links -->


            @if(Auth::user()->hasAccess(['dashboard.view']))
                <li><a href="{{ route('dashboard.media') }}"><i class='fa fa-link'></i> <span>Dashboard</span></a></li>
            @endif

            @if(Auth::user()->hasAccess(['project.view']))
                <li><a href="{{route('project.index')}}"><i class='fa fa-link'></i> <span>Marketing areas</span></a></li>
            @endif

            @if(Auth::user()->hasAccess(['reports.countries']))
                <li><a href="{{ url('home') }}"><i class='fa fa-link'></i> <span>Reports by countries</span></a></li>
            @endif

            <li><a href="{{route('report.index')}}"><i class='fa fa-calendar-check-o'></i> <span>{{ trans('report.reports') }}</span></a></li>

            @if(Auth::user()->inRole('user'))
                <li><a href="/report/database/fill/599c91284e801f0006148026"><i class='fa fa-link'></i> <span>Media</span></a></li>
                <!-- <li><a href="/report/database/fill/59a141ca981b4f00063c3d62"><i class='fa fa-link'></i> <span>Media dynamic data</span></a></li> -->
            @endif


            @if(Auth::user()->inRole('admin'))


                @if(Auth::user()->hasAccess(['reporter.view']))
                    <li class="header">Contributors</li>
                    <li><a href="{{route('user.index')}}"><i class='fa fa-user'></i> <span>{{ trans('contributor.users') }}</span></a></li>
                    <li><a href="{{route('group.index')}}"><i class='fa fa-group'></i> <span>{{ trans('contributor.groups') }}</span></a></li>
                @endif

                <li class="header">Settings</li>
                @if(Auth::user()->hasAccess(['settings.structure']))
                    <li @if(Request::is('report/column') || Request::is('report/column/*')) class="active" @endif><a href="{{route('report.column.index')}}"><i class='fa fa-list'></i> <span>{{ trans('report-column.fields') }}</span></a></li>
                    <li @if(Request::is('report/type') || Request::is('report/type/*')) class="active" @endif><a href="{{route('report.template.index')}}"><i class='fa fa-list'></i> <span>{{ trans('report-template.templates') }}</span></a></li>
                @endif
                <li @if(Request::is('report/database') || Request::is('report/database/*')) class="active" @endif><a href="{{route('report.database.index')}}"><i class='fa fa-list'></i> <span>{{ trans('report-column-data.database') }}</span></a></li>

            @endif

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
