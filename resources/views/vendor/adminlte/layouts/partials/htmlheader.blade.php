<head>
    <meta charset="UTF-8">
    <title> {{trans('global.reporting')}} - @yield('htmlheader_title', 'Your title here') </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script charset="utf-8" src="https://ucarecdn.com/libs/widget/3.1.2/uploadcare.full.min.js"></script>
    <script>
        UPLOADCARE_PUBLIC_KEY = '04d8432bb78039d1c83d';
        UPLOADCARE_MANUAL_START = true
    </script>
    <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('plugins/datatables/jquery.dataTables.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('plugins/select2/select2.css')}}" rel="stylesheet" type="text/css">

    <!--
    <link href="/dhtmlxGrid/codebase/fonts/font_roboto/roboto.css" rel="stylesheet">
    <link href="/dhtmlxGrid/codebase/dhtmlxgrid.css" rel="stylesheet">
-->

    <link href="/dhtmlxSuite/codebase/dhtmlx.css" rel="stylesheet">

    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css" />
    @yield('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <script>
        //See https://laracasts.com/discuss/channels/vue/use-trans-in-vuejs
        window.trans = @php
            // copy all translations from /resources/lang/CURRENT_LOCALE/* to global JS variable
            $lang_files = File::files(resource_path() . '/lang/' . App::getLocale());
            $trans = [];
            foreach ($lang_files as $f) {
                $filename = pathinfo($f)['filename'];
                $trans[$filename] = trans($filename);
            }
            $trans['adminlte_lang_message'] = trans('adminlte_lang::message');
            echo json_encode($trans);
        @endphp

@php
    if (Session::get('contributorGroupId') == '599a89455dbf5f00075dcd94') {
        echo "var default_currency = 'GBP';";
    } else {
        echo "var default_currency = 'EUR';";
    }
@endphp

</script>
</head>
