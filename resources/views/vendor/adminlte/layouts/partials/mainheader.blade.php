<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">{{trans('global.reporting')}}</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{{trans('global.reporting')}}</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">

            @if(Auth::user()->inRole('admin'))
                <div id="project-select">
                    {!! Form::open(['route' => 'user.change-project']) !!}
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" id="sizing-addon1">{{trans('global.select-project')}}</span>
                        <select name="project" class="form-control">
                            <option @if(Session::get('project') == 0) selected @endif value="0">All</option>
                            @foreach(App\Models\Project::getList() as $item)
                                <option @if(Session::get('project') == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <div class="input-group-btn">
                            {!! Form::submit(trans('global.select'),['class' => 'btn btn-info']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            @endif

            <div id="contributor-group-select">
                {!! Form::open(['route' => 'user.change-group']) !!}
                <div class="input-group input-group-sm">

                    <span class="input-group-addon" id="sizing-addon1">{{trans('global.select-contributor-group')}}</span>
                    <select name="contributorGroupId" class="form-control">
                        @if(\Illuminate\Support\Facades\Auth::user()->inRole('user'))
                            @foreach(\Illuminate\Support\Facades\Auth::user()->contributorGroups as $item)
                                <option @if(Session::get('contributorGroupId') == $item->group->getKey()) selected @endif value="{{$item->group->getKey()}}">{{$item->group->name}}</option>
                            @endforeach
                        @else
                            <option @if(Session::get('contributorGroupId') == 0) selected @endif value="0">All</option>
                            @foreach(App\Models\Group::getList() as $item)
                                <option @if(Session::get('contributorGroupId') == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        @endif
                    </select>

                    <div class="input-group-btn">
                        {!! Form::submit(trans('global.select'),['class' => 'btn btn-info']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <ul class="nav navbar-nav">


                <!-- Notifications Menu -->

                <!-- Tasks Menu -->

                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu" id="user_menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                                <p>
                                    {{ Auth::user()->name }}
                                    <small>Account</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">

                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
                @endif

            </ul>
        </div>
    </nav>
</header>
