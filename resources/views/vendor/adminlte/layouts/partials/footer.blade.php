<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Powered by Cagoi Networks AG
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 TCL Communication. All rights reserved.</strong>
</footer>
