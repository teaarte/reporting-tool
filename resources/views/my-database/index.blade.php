@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-column-data.columns_data') }}
@endsection

@section('contentheader_title')
    {{ trans('report-column-data.columns_data') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/my-database.js')}}"></script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('report.database.create')}}" class="btn btn-sm btn-primary">{{trans('report-column-data.add_data')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" id="my-database-table">
                            <thead>
                            <tr>
                                <th>{{trans('report-column-data.name')}}</th>
                                <th class="table-actions" width="146">{{trans('report-column-data.actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
