@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-column-data.columns_data') }}
@endsection

@section('contentheader_title')
    {{ trans('report-column-data.columns_data') }}
@endsection

@section('js')
    <script src="{{asset('js/MyDatabaseEdit.js')}}"></script>
    <script src="{{asset('js/dhtmlx.js')}}"></script>
@endsection

@section('main-content')




    <div class="container-fluid spark-screen">
        <div class="row">
            {!! Form::hidden('db_id', $model->getKey()) !!}

            @foreach ($fieldsMultiselect as $field)

                {!! Form::hidden('columnIndex_'.$field['columnIndex'], json_encode($field['values']), ['class' => 'column-multiselect', 'data-column-index' => $field['columnIndex']])  !!}

            @endforeach

            <div class="col-md-12">
                <div class="form-group pull-right">

                </div>
            </div>
            <div class="col-md-12">
                <button onclick="addRow();">Add row</button>
                <button onclick="deleteRow();">Delete row</button>
                <div id="grid" style="width:100%; min-height: 500px;">

                </div>
            </div>
        </div>
    </div>
@endsection
