@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-column-data.columns_data') }}
@endsection

@section('contentheader_title')
    {{ trans('report-column-data.columns_data') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/my-database.js')}}"></script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('errors'))
                    <div class="callout callout-danger">
                        <h4>{{trans('global.errors')}}</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ trans($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('report-column-data.create')}}</h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('report.database.index')}}" class="btn btn-sm btn-primary">{{trans('report-column-data.cancel')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        {!!Form::model($model,['route' => ['report.database.update',  $model->getKey()], 'method' => 'put'])!!}
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('name', trans('report-column-data.name')) !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('report-column-data.name-placeholder')]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('fields', trans('report-template.fields')) !!}

                                {{ Form::sortable('fields', $fields, $selectedFields) }}

                            </div>
                        </div>
                        <div class="box-footer">
                            {!! Form::submit(trans('report-column-data.save'),['class' => 'btn btn-primary btn-flat btn-md']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection