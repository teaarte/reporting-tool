@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report.report_date',['reportTitle' => $reportTitle]) }}
@endsection

@section('contentheader_title')
    {{ trans('report.report_date',['reportTitle' => $reportTitle]) }}
@endsection

@section('js')
<script src="{{asset('js/reportEdit.js')}}"></script>
<script src="{{asset('js/dhtmlx.js')}}"></script>
<script>
    getMiniStat('{{ $reportSchedule->_id }}');
    setInterval(function() {getMiniStat('{{ $reportSchedule->_id }}')}, 5000);
</script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            @foreach ($fieldsMultiselect as $field)

                {!! Form::hidden('columnIndex_'.$field['columnIndex'], json_encode($field['values']), ['class' => 'column-multiselect', 'data-column-index' => $field['columnIndex']])  !!}

            @endforeach
            {!! Form::hidden('reportSchedule_id', $reportSchedule->getKey()) !!}
            <div class="col-md-12">
                {!! Form::open(['route' => ['report-schedule.change-status',  $reportSchedule->getKey()]]) !!}
                    <div class="form-group pull-right">
                        @if($reportSchedule->status === 'open')
                            {!! Form::hidden('status', 'close') !!}
                            {!! Form::submit(trans('report.close'),['class' => 'btn btn-danger btn-flat btn-sm']) !!}
                        @elseif($reportSchedule->status === 'close')
                            {!! Form::hidden('status', 'open') !!}
                            {!! Form::submit(trans('report.open'),['class' => 'btn btn-success btn-flat btn-sm']) !!}
                        @endif
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="col-md-12">
                <button onclick="addRow();">Add row</button>
                <button onclick="deleteRow();">Delete row</button>
                <div id="grid" style="width:100%; min-height: 500px;">

                </div>
            </div>
        </div>
    </div>


    <div class="mini_stat">

        <div class="block">
            <div class="number" id="articles_published">-</div>
            <div class="descr">Articles published</div>
        </div>

        <div class="block">
            <div class="number" id="audience_reach">-</div>
            <div class="descr">Audience Reach</div>
        </div>

        <div class="block">
            <div class="number" id="ave_generated">-</div>
            <div class="descr">AVE generated</div>
        </div>

        <div class="block">
            <div class="number" id="positive_statements">-</div>
            <div class="descr">Positive statements</div>
        </div>
    </div>


    @push('js')
        console.log('----------------');
        alert(1);
        getMiniStat();
    @endpush


@endsection
