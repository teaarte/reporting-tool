@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report-plan.schedule') }}
@endsection

@section('contentheader_title')
    {{ trans('report-plan.schedule') }}
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/report-schedule.js')}}"></script>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('report-schedule.projects')}}</h3>
                        <div class="box-tools pull-right">
                            <a href="{{route('report-schedule.create', ['report_id' => $reportId])}}" class="btn btn-sm btn-primary">{{trans('report-schedule.add')}}</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" id="report-schedules-table" data-report-id="{{$reportId}}">
                            <thead>
                            <tr>
                                <th>{{trans('report-schedule.name')}}</th>
                                <th class="table-actions">{{trans('report-schedule.actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
