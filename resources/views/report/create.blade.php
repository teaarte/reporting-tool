@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('report.reports') }}
@endsection

@section('contentheader_title')
    {{ trans('report.reports') }}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
           <div class="col-md-12">
               @if(Session::has('errors'))
                   <div class="callout callout-danger">
                       <h4>{{trans('global.errors')}}</h4>
                       <ul>
                           @foreach ($errors->all() as $error)
                               <li>{{ trans($error) }}</li>
                           @endforeach
                       </ul>
                   </div>
               @endif
               <div class="box">
                   <div class="box-header with-border">
                       <h3 class="box-title">{{trans('report.create_report')}}</h3>
                       <div class="box-tools pull-right">
                           <a href="{{route('report.index')}}" class="btn btn-sm btn-primary">{{trans('report.cancel')}}</a>
                       </div>
                   </div>
                   <div class="box-body">
                       {!! Form::open(['route' => 'report.store']) !!}
                       <div class="box-body">
                           <div class="form-group">
                               {!! Form::label('name', trans('report.name')) !!}
                               {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('report.name-placeholder')]) !!}
                           </div>
                           <div class="form-group">
                               {!! Form::label('from', trans('report.from')) !!}
                               <div class="input-group date">
                                   <div class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </div>
                                   {!! Form::text('from_date', null, ['class' => 'form-control  pull-right datepicker','placeholder' => trans('report.from-placeholder')]) !!}
                               </div>
                           </div>
                           <div class="form-group">
                               {!! Form::label('to', trans('report.to')) !!}
                               <div class="input-group date">
                                   <div class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </div>
                                   {!! Form::text('to_date', null, ['class' => 'form-control  pull-right datepicker','placeholder' => trans('report.to-placeholder')]) !!}
                               </div>
                           </div>
                           <div class="form-group">
                               {!! Form::label('template_id', trans('report.template')) !!}
                               {!! Form::select('template_id', $types, null, ['class' => 'form-control', 'placeholder' => trans('report.template-placeholder')]) !!}
                           </div>
                           <div class="form-group">
                               {!! Form::label('type', trans('report.period')) !!}
                               {!! Form::select('period', ['daily' => 'Daily', 'weekly' => 'Weekly', 'monthly' => 'Monthly', 'quarterly' => 'Quarterly'], null, ['class' => 'form-control', 'placeholder' => trans('report.period-placeholder')]) !!}
                           </div>
                       </div>
                       <div class="box-footer">
                           {!! Form::submit(trans('report.save'),['class' => 'btn btn-primary btn-flat btn-md']) !!}
                       </div>
                       {!! Form::close() !!}
                   </div>
               </div>
           </div>
        </div>
    </div>
@endsection
