@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('report.reports') }}
@endsection

@section('contentheader_title')
{{ trans('report.reports') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen reports">

    @if(Auth::user()->inRole('admin'))
    <a href="{{route('report.create')}}" class="btn btn-sm btn-primary inline">{{trans('report.create_report')}}</a>
    @endif

    <div class="row">

        @foreach($reports as $report)
        <h3 title="Template: {{$report->template['name']}}">{{$report->name}} - {{$report->period}}
            @if(Auth::user()->inRole('admin'))
            <a href="{{URL::to('report/destroy', $report->id)}}" class="delete" title="Remove"><i class="fa fa-times"></i></a>
            @endif
        </h3>
        <div class="pre-scrollable">
            @foreach($report->reportSchedule as $schedule)
                <div class="col-md-2">
                    <div class="small-box bg-green">
                        @if(Auth::user()->inRole('admin'))
                        <a href="{{URL::to('report-schedule/destroy', $schedule->id)}}" class="delete" title="Remove"><i class="fa fa-times"></i></a>
                        @endif
                        <div class="inner">
                            <h4 class="">{{ $schedule->date }}</h4>
                            <br>
                        </div>
                        <a href="{{route('report-schedule/grid', $schedule->id)}}" class="small-box-footer">Fill <i class="fa fa-arrow-circle-right"></i></a>

                    </div>
                </div>
            @endforeach
        </div>
        @endforeach

{{--
<h3>{{trans('report.custom_reports')}} <a href="{{route('report.custom.create')}}" class="btn btn-sm btn-primary inline">{{trans('report.create_custom_report')}}</a></h3>
<div class="pre-scrollable">
    @foreach($customs as $custom)
        <div class="col-md-2">
            <div class="small-box bg-blue">
                <div class="inner">
                    <h4 class="">{{\Carbon\Carbon::parse($custom->from)->format('d M Y')}} - {{\Carbon\Carbon::parse($custom->to)->format('d M Y')}}</h4>
                    <p>{{$custom->name}}</p>
                </div>
                <a href="{{route('report.grid', $custom->getKey())}}" class="small-box-footer">{{trans('report.fill')}} <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    @endforeach
</div>
--}}
</div>
</div>
@endsection
