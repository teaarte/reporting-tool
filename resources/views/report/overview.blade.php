<table class="table table-bordered">
    <tbody>
        <tr>
            <th width="33%">{{trans('overview.group')}}</th>
            <th width="33%">{{trans('overview.report-complete')}}</th>
            <th width="5%">{{trans('overview.reminder')}}</th>
            <th width="33%">{{trans('overview.remark')}}</th>
        </tr>

        @foreach($groups as $group)
            <tr>
                <td>{{$group->name}}</td>
                <td>
                    @if(!$items->where('group_id', $group->getKey())->isEmpty())
                        @if($items->where('group_id', $group->getKey())->first()->status == 'open')
                            {{trans('overview.no')}}
                        @elseif($items->where('group_id', $group->getKey())->first()->status == 'close')
                            {{trans('overview.yes')}}
                        @endif
                    @else
                        {{trans('overview.no')}}
                    @endif
                </td>
                <td>
                    <button type="button" class="btn btn-default btn-sm">Send reminder</button>
                </td>
                <td>
                    {!! Form::text('remark', null, ['class' => 'form-control']) !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>