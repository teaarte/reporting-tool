<div class="inline table-actions">
    @if(isset($fillable) && $fillable)
        <a href="{{route($route.'.fill', $item->getKey())}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-list"></i> {{trans('actions.fill')}}</a>
    @endif
    @if($editable)
        <a href="{{route($route.'.edit', $item->getKey())}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> {{trans('actions.edit')}}</a>
    @endif
    @if($deletable)
        {!! Form::open(['route' => [$route.'.destroy', $item->getKey()], 'method' => 'delete', 'onsubmit' => 'return confirm("'.trans('actions.confirm-delete').'")']) !!}
            <button type="submit" class="btn btn-xs btn-primary">
                    <i class="glyphicon glyphicon-trash"></i> {{trans('actions.delete')}}
            </button>
        {!! Form::close() !!}
    @endif
</div>