<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create groups
        for ($i=1; $i <= 5; $i++){
            $model = new \App\Models\Group();
            $model->name = 'Test group '.$i;
            $model->save();
        }

        // Create users
        $userRole = \Cartalyst\Sentinel\Laravel\Facades\Sentinel::findRoleBySlug('user');
        $groups = \App\Models\Group::all();

        for ($i=1; $i <= 5; $i++){
            $model = new \App\Models\User();
            $model->email = 'user'.$i.'@mail.com';
            $model->generated_password = '123456';
            $model->password = bcrypt('123456');
            $model->save();

            // Attach role for user
            $userRole->users()->attach($model);

            // Attach groups for user
            $groupUsers = new \App\Models\GroupUsers();
            $groupUsers->user_id = $model->getKey();
            $groupUsers->group_id = $groups->random()->getKey();
            $groupUsers->save();
        }

        // Create columns
        for ($i=1; $i <= 5; $i++){
            $model = new \App\Models\Report\ReportColumn();
            $model->index = 'column'.$i;
            $model->title = 'Column '.$i;
            $model->type = 'string';
            $model->save();
        }

        // Attach columns for default type
        $columns = \App\Models\Report\ReportColumn::all();
        $type = \App\Models\Report\ReportTemplate::findBySlugOrFail('monthly-report');
        $data = array();

        foreach ($columns as $column){
            $data[] = $column->getKey();
        }

        $type->columns = $data;
        $type->save();
    }
}
