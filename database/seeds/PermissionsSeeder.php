<?php

use Illuminate\Database\Seeder;
use \Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        $role = Sentinel::findRoleBySlug('padmin');

        $role->updatePermission('dashboard.view', true, true)->save();
        $role->updatePermission('dashboard.component.create', true, true)->save();
        $role->updatePermission('dashboard.component.edit', true, true)->save();
        $role->updatePermission('dashboard.component.delete', true, true)->save();


        $role->updatePermission('reporter.view', true, true)->save();
        $role->updatePermission('settings.structure', true, true)->save();
        $role->updatePermission('project.view', true, true)->save();
        $role->updatePermission('reports.countries', true, true)->save();



        $role = Sentinel::findRoleBySlug('admin');

        $role->updatePermission('dashboard.view', true, true)->save();
        $role->updatePermission('dashboard.component.create', true, true)->save();
        $role->updatePermission('dashboard.component.edit', true, true)->save();
        $role->updatePermission('dashboard.component.delete', true, true)->save();


        $role->updatePermission('reporter.view', true, true)->save();
        $role->updatePermission('settings.structure', true, true)->save();
        $role->updatePermission('project.view', true, true)->save();
        $role->updatePermission('reports.countries', true, true)->save();

    }
}
