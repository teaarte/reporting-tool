<?php

use Illuminate\Database\Seeder;
use \Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class RolesAndAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->mainAdmin();

        $this->projectAdmin();


        $this->user();

    }


    private function mainAdmin() {

        // Create project admin role
        $role = [
            'name' => 'Administrator',
            'slug' => 'admin'
        ];
        Sentinel::getRoleRepository()->createModel()->fill($role)->save();
        $role = Sentinel::findRoleBySlug('admin');

        // Create user
        $userData = [
            'email'    => env('ADMIN_EMAIL', 'admin@mail.com'),
            'password' => env('ADMIN_PWD', '123456')
        ];
        $user =  Sentinel::registerAndActivate($userData);
        $role->users()->attach($user);

    }


    private function projectAdmin() {

        // Create project admin role
        $role = [
            'name' => 'Project administrator',
            'slug' => 'padmin'
        ];
        Sentinel::getRoleRepository()->createModel()->fill($role)->save();
        $role = Sentinel::findRoleBySlug('padmin');

        // Create user
        $userData = [
            'email'    => 'padmin@mail.com',
            'password' => '123456'
        ];
        $user =  Sentinel::registerAndActivate($userData);
        $role->users()->attach($user);


    }

    private function user() {

        // Set 'admin' role to Admin user
        $userRole = [
            'name' => 'User',
            'slug' => 'user'
        ];
        Sentinel::getRoleRepository()->createModel()->fill($userRole)->save();

    }



}
