<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndAdminSeeder::class);
        $this->call(ReportMonthlyTypeSeeder::class);
        $this->call(TestDataSeeder::class);
        $this->call(PermissionsSeeder::class);
    }
}
