<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['middleware' => 'auth'], function () {

    // Projects

    Route::get('project/index', 'ProjectController@index')->name('project.index');
    //Route::get('project/create', 'ProjectController@create')->name('project.create');
    Route::get('project/list', 'ProjectController@getList')->name('project.list');

    Route::resource('project', 'ProjectController', ['except' => ['show']]);


    // Users
    //Route::group(['middleware' => 'group:1'], function()
    //{
        Route::resource('user', 'UserController', ['except' => ['show']]);
    //});


    Route::get('user/get-list', 'UserController@getList')->name('user.get-list');
    Route::post('user/change-group', 'UserController@changeContributorGroup')->name('user.change-group');
    Route::post('user/change-project', 'UserController@changeProject')->name('user.change-project');

    // Groups
    Route::resource('group', 'GroupController', ['except' => ['show']]);
    Route::get('group/get-list', 'GroupController@getList')->name('group.get-list');



    Route::get('report-schedule/createMultiple', 'Report\ReportScheduleController@createMultiple');
//    Route::get('report-schedule', 'ReportScheduleController@index')->name('report_schedule.index');
    Route::get('report-schedule/report/{id}', 'ReportScheduleController@index')->name('report_schedule.index');
    Route::get('report-schedule/list', 'ReportScheduleController@getList')->name('report_schedule.list');
    Route::get('report-schedule/getMiniStat', 'Report\ReportScheduleController@getMiniStat');

    Route::resource('report-schedule', 'ReportScheduleController', ['except' => ['show']]);


    Route::namespace('Dashboard')->prefix('dashboard')->group(function () {

        Route::get('media', 'MediaController@index')->name('dashboard.media');
        Route::get('press', 'PressController@index')->name('dashboard.press');
        Route::get('test', 'MediaController@test');
        Route::resource('dashboard', 'DashboardController');

    });


    Route::namespace('Report')->prefix('report-schedule')->group(function () {

        //Reports
        Route::get('grid/{index}', 'ReportScheduleController@grid')->name('report-schedule/grid');
        Route::any('get-data', 'ReportScheduleController@getData')->name('report-schedule.get-data');
        Route::post('add-row', 'ReportScheduleController@addRow')->name('report-schedule.add-row');
        Route::post('remove-row', 'ReportScheduleController@removeRow')->name('report-schedule.remove-row');
        Route::post('insert-data', 'ReportScheduleController@insertData')->name('report-schedule.insert-data');
        Route::post('change-status/{id}', 'ReportScheduleController@changeStatus')->name('report-schedule.change-status');
        Route::post('generateNewRow', 'ReportScheduleController@generateNewRow');
        Route::any('getData/{reportSchedule_id}', 'ReportScheduleController@getData');

        Route::get('destroy/{id}', 'ReportScheduleController@destroy')->name('report-schedule.destroy');

    });


    Route::namespace('Report')->prefix('report')->group(function () {

        Route::get('/', 'ReportController@index')->name('report.index');
        Route::get('destroy/{id}', 'ReportController@destroy')->name('report.destroy');

        // Report
        Route::resource('report', 'ReportController', ['except' => ['index','show', 'edit','update']]);

        // Report columns
        Route::resource('column', 'ReportColumnController', ['as' => 'report','except' => ['show']]);
        Route::get('column/get-list', 'ReportColumnController@getList')->name('report.column.get-list');

        // Report columns lists
        Route::resource('database', 'MyDatabaseController', ['as' => 'report','except' => ['show']]);
        Route::get('database/get-list', 'MyDatabaseController@getList')->name('report.database.get-list');
        Route::get('database/fill/{id}', 'MyDatabaseController@fill')->name('report.database.fill');
        Route::post('database/get-data', 'MyDatabaseController@getData')->name('report.database.get-data');
        Route::post('database/insert-data', 'MyDatabaseController@insertData')->name('report.database.insert-data');
        Route::post('database/remove-row', 'MyDatabaseController@removeRow')->name('report.database.remove-row');
        Route::post('database/generateNewRow', 'MyDatabaseController@generateNewRow');

        Route::any('database/getItems/{field}', 'MyDatabaseController@getItems');


        Route::any('database/getData/{db_id}', 'MyDatabaseController@getData')->name('report.database.get-data');


        // Report templates
        Route::resource('template', 'ReportTemplateController', ['as' => 'report','except' => ['show']]);
        Route::get('template/get-list', 'ReportTemplateController@getList')->name('report.template.get-list');
    });

    Route::namespace('Report')->prefix('reportFinal')->group(function () {

        Route::get('/list', 'ReportFinalController@getList');
        Route::get('/addNew', 'ReportFinalController@addNew');
        Route::resource('/', 'ReportFinalController');

    });

    Route::namespace('Import')->prefix('import')->group(function () {


        Route::get('importMediaByContries', 'ImportMediaByCountriesController@importMediaByCountries');
    });

    Route::namespace('presentation')->prefix('presentation')->group(function () {

        Route::get('generatePresentation', 'IFA_Berlin_2017_Controller@init');
    });
});
